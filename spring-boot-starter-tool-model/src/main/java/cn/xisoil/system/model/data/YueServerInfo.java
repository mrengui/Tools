package cn.xisoil.system.model.data;

import lombok.Data;

@Data
public class YueServerInfo {

    private String serverName;//服务器名称

    private String equipment;//操作系统

    private String serverIp;//服务器IP

    private String architecture;//系统架构

    private String javaServerName;//java名称

    private String javaVersion;//java版本

    private String startTime;//启动时间

    private String runTime;//运行时常

    private String javaDir;//java路径

    private String projectDir;//java路径

    private String runParameters;//运行参数


}
