package cn.xisoil.log.data;

/**
 * @Description TODO
 * @Author Vien
 * @CreateTime 2023-02-2023/2/25 11:25:27
 **/
public enum LogEnum {

    DEL("删除%s"),
    PAGE("分页获取%s"),
    EXPORT("导出%s"),
    EDIT("修改%s"),
    ADD("增加%s"),
    GET("获取%s详情");

    private final String value;

    public String getValue() {
        return value;
    }

    LogEnum(String value){
        this.value=value;
    }
    public String toString(String name) {
        return String.format(this.value,name);
    }
}
