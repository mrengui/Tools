package cn.xisoil.auth.data.user;

import cn.xisoil.curd.model.enums.ObjectColumnType;
import cn.xisoil.curd.model.interfaces.CurdModel;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity(name = "User")
@Table(name = "User")
@CurdModelObject(value = "用户管理")
@EntityListeners(AuditingEntityListener.class)
public class YueLoginUser {

    public YueLoginUser(){

    }
    public YueLoginUser(String name){
        this.nickname=name;
        this.account=name;
    }

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    @CurdModel(value = "账号",search = true)
    private String account;

    @CurdModel(value = "加密密码",required = true)
    private String password="";

    @CurdModel(value = "昵称")
    private String nickname;

    @CurdModel(value = "排序",type = ObjectColumnType.NUMBER,comparable = true)
    private Integer comparable;

    @CreatedDate
    private Date createTime;

    private BigDecimal price=BigDecimal.ZERO;

    @CurdModel(value = "所属角色",type = ObjectColumnType.LIST,url = "/role/list",search = true)
    private String roleId;

    @CurdModel(value = "头像",type = ObjectColumnType.IMAGE,show = false)
    private String headImage;

    private String title;

    public String getTitle() {
        return nickname;
    }
}
