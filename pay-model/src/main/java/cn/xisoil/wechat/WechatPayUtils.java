package cn.xisoil.wechat;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import cn.xisoil.utils.HttpRequestIpUtil;
import cn.xisoil.vo.CallBackObj;
import cn.xisoil.vo.YuePayInfo;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Component
public class WechatPayUtils {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private WechatPayConfig wechatPayConfig;
    @Autowired
    private HttpRequestIpUtil httpRequestIpUtil;

    int width = 200;
    int height = 200;
    String format = "png";


    public void writeQrToStream(YuePayInfo yuePayInfo) throws Exception{
        Map<EncodeHintType, Object> config = new HashMap<>();
        config.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        config.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        config.put(EncodeHintType.MARGIN, 0);

        HttpPost httpPost = new HttpPost("https://api.mch.weixin.qq.com/v3/pay/transactions/native");
        httpPost.addHeader("Accept", "application/json");
        httpPost.addHeader("Content-type","application/json; charset=utf-8");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        ServletOutputStream out = response.getOutputStream();
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode rootNode = objectMapper.createObjectNode();

        rootNode.put("mchid",wechatPayConfig.getMchId())
                .put("appid", wechatPayConfig.getAppId())
                .put("description", yuePayInfo.getDescription())
                .put("notify_url", yuePayInfo.getNotify_url())
                .put("out_trade_no", yuePayInfo.getOrderNum());
        rootNode.putObject("amount")
                .put("total",yuePayInfo.getTotal()).put("currency","CNY");

        objectMapper.writeValue(bos, rootNode);
        httpPost.setEntity(new StringEntity(bos.toString(StandardCharsets.UTF_8), "UTF-8"));
        CloseableHttpResponse closeableHttpResponse = wechatPayConfig.getHttpClient().execute(httpPost);
        String bodyAsString = EntityUtils.toString(closeableHttpResponse.getEntity());
        JSONObject jsonObject=JSONObject.parseObject(bodyAsString);
        if (jsonObject.getString("code")!=null){
           logger.error("微信支付==>{}",jsonObject.getString("message"));
        }
        BitMatrix bitMatrix = (new MultiFormatWriter()).encode(jsonObject.getString("code_url"), BarcodeFormat.QR_CODE, width, height, config);
        MatrixToImageWriter.writeToStream(bitMatrix, format, out);
    }


    @SneakyThrows
    public String wapPay(YuePayInfo yuePayInfo){

        HttpPost httpPost = new HttpPost("https://api.mch.weixin.qq.com/v3/pay/transactions/h5");
        httpPost.addHeader("Accept", "application/json");
        httpPost.addHeader("Content-type","application/json; charset=utf-8");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode rootNode = objectMapper.createObjectNode();

        rootNode.put("mchid",wechatPayConfig.getMchId())
                .put("appid", wechatPayConfig.getAppId())
                .put("description", yuePayInfo.getDescription())
                .put("notify_url", yuePayInfo.getNotify_url())
                .put("out_trade_no", yuePayInfo.getOrderNum());
        rootNode.putObject("amount")
                .put("total",yuePayInfo.getTotal()).put("currency","CNY");

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        rootNode.putObject("scene_info")
                .put("payer_client_ip", httpRequestIpUtil.getIpAddress(request))
                .putObject("h5_info").put("type","Wap");

        objectMapper.writeValue(bos, rootNode);
        httpPost.setEntity(new StringEntity(bos.toString(StandardCharsets.UTF_8), "UTF-8"));
        CloseableHttpResponse closeableHttpResponse = wechatPayConfig.getHttpClient().execute(httpPost);
        String bodyAsString = EntityUtils.toString(closeableHttpResponse.getEntity());
        JSONObject jsonObject=JSONObject.parseObject(bodyAsString);
        if (jsonObject.getString("code")!=null){
           logger.error("微信支付==>{}",jsonObject.getString("message"));
        }
        return jsonObject.getString("h5_url");

    }


    @SneakyThrows
    public String writeQrToString(YuePayInfo yuePayInfo){
        Map<EncodeHintType, Object> config = new HashMap<>();
        config.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        config.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        config.put(EncodeHintType.MARGIN, 0);

        HttpPost httpPost = new HttpPost("https://api.mch.weixin.qq.com/v3/pay/transactions/native");
        httpPost.addHeader("Accept", "application/json");
        httpPost.addHeader("Content-type","application/json; charset=utf-8");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode rootNode = objectMapper.createObjectNode();

        rootNode.put("mchid",wechatPayConfig.getMchId())
                .put("appid", wechatPayConfig.getAppId())
                .put("description", yuePayInfo.getDescription())
                .put("notify_url", yuePayInfo.getNotify_url())
                .put("out_trade_no", yuePayInfo.getOrderNum());
        rootNode.putObject("amount")
                .put("total",yuePayInfo.getTotal()).put("currency","CNY");
        objectMapper.writeValue(bos, rootNode);
        httpPost.setEntity(new StringEntity(bos.toString(StandardCharsets.UTF_8), "UTF-8"));
        CloseableHttpResponse closeableHttpResponse = wechatPayConfig.getHttpClient().execute(httpPost);
        String bodyAsString = EntityUtils.toString(closeableHttpResponse.getEntity());
        JSONObject jsonObject=JSONObject.parseObject(bodyAsString);
        if (jsonObject.getString("code")!=null){
            logger.error("微信支付==>{}",jsonObject.getString("message"));
        }
        BitMatrix bitMatrix = (new MultiFormatWriter()).encode(jsonObject.getString("code_url"), BarcodeFormat.QR_CODE, width, height, config);
        BufferedImage bufferedImage=MatrixToImageWriter.toBufferedImage(bitMatrix);
        return BufferedImageToBase64(bufferedImage);
    }


    @SneakyThrows
    public String callBackContent(CallBackObj callBackObj){
        AesUtil aesUtil = new AesUtil(wechatPayConfig.getAppV3Secret().getBytes());
        String s = aesUtil.decryptToString(callBackObj.getResource()
                        .getAssociated_data().getBytes(),
                callBackObj.getResource().getNonce().getBytes(), callBackObj.getResource().getCiphertext());
        return s;
    }


    /**
     * BufferedImage 编码转换为 base64
     * @param bufferedImage
     * @return
     */
    private static String BufferedImageToBase64(BufferedImage bufferedImage) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();//io流
        try {
            ImageIO.write(bufferedImage, "png", baos);//写入流中
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] bytes = baos.toByteArray();//转换成字节
        Base64 encoder = new Base64();
        String png_base64 = encoder.encodeToString(bytes).trim();//转换成base64串
        png_base64 = png_base64.replaceAll("\n", "").replaceAll("\r", "");//删除 \r\n
        System.out.println("值为：" + "data:image/jpg;base64," + png_base64);
        return "data:image/jpg;base64," + png_base64;
    }

    /**
     * base64 编码转换为 BufferedImage
     * @param base64
     * @return
     */
    private  static BufferedImage base64ToBufferedImage(String base64) {
        Base64 decoder = new Base64();
        try {
            byte[] bytes1 = decoder.decode(base64);
            ByteArrayInputStream bais = new ByteArrayInputStream(bytes1);
            return ImageIO.read(bais);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
