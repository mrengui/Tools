package cn.xisoil.system.model.dao;

import cn.xisoil.curd.dao.YueRepository;
import cn.xisoil.system.model.data.YueBasicData;

import java.util.Optional;

public interface YueBasicDataRepository extends YueRepository<YueBasicData,String> {

    Optional<YueBasicData>findTopByNameIsNotNull();


    Boolean existsByIsLogIsTrue();

}
