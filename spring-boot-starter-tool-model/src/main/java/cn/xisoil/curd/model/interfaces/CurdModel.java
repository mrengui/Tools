package cn.xisoil.curd.model.interfaces;

import cn.xisoil.curd.model.enums.ObjectColumnType;

import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CurdModel {

    //表头信息
    String value();

    //从列表取值时，列表值来源地址
    String url() default "";

    // 输入提示
    String placeholder() default "";

    //字段类型
    ObjectColumnType type() default ObjectColumnType.STRING;

    String group() default "";

    //是否可编辑
    boolean editor() default true;

    //是否可搜索
    boolean search() default false;

    //是否列表展示
    boolean show() default true;

    //是否可排序
     boolean comparable() default false;

    //是否必填
    boolean required() default false;

}
