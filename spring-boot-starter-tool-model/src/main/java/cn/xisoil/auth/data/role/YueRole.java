package cn.xisoil.auth.data.role;


import cn.xisoil.common.to.StringConverters;
import cn.xisoil.curd.model.enums.ObjectColumnType;
import cn.xisoil.curd.model.interfaces.CurdModel;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity(name = "YueRole")
@Table(name = "yue_role")
@EntityListeners(AuditingEntityListener.class)
@CurdModelObject(value = "角色管理")
public class YueRole {


    public YueRole(){

    }

    public YueRole(String title){
        this.title=title;
    }

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    @CurdModel("角色名称")
    private String title;

    @CreatedDate
    @CurdModel(value = "添加时间",type = ObjectColumnType.TIME,editor = false)
    private Date createTime;

    @CurdModel(value = "角色权限",type = ObjectColumnType.LISTMULTISELECT,url = "/permission/list")
    @Convert(converter = StringConverters.class)
    @Column(columnDefinition = "text(0)")
    private List<String> permissionIds=new ArrayList<>();

}
