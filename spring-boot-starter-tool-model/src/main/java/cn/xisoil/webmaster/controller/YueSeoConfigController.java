package cn.xisoil.webmaster.controller;


import cn.xisoil.common.result.YueResult;
import cn.xisoil.datacheck.PermissionCheckAutomation;
import cn.xisoil.webmaster.data.YueSeoConfig;
import cn.xisoil.webmaster.dto.YueSeoUrlRequest;
import cn.xisoil.webmaster.service.YueSeoConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/manage/seo")
public class YueSeoConfigController implements PermissionCheckAutomation {
    @Override
    public String getUrl() {
        return "/seo/index";
    }

    @Override
    public String getName() {
        return "SEO推送";
    }

    @Override
    public String getParent() {
        return "站长工具";
    }

    @Autowired
    private YueSeoConfigService yueSeoConfigService;

    @GetMapping
    public YueResult<YueSeoConfig>getSeoConfig(){
        return yueSeoConfigService.getSeoConfig();
    }

    @PutMapping
    public YueResult<String>setSeoConfig(@Valid @RequestBody YueSeoConfig yueSeoConfig, BindingResult result){
        if (result.hasErrors()){
            for (ObjectError error : result.getAllErrors()) {
                return YueResult.<String>builder().fail().code(500).message(error.getDefaultMessage()).build();
            }
        }
        return yueSeoConfigService.setSeoConfig(yueSeoConfig);
    }

    @PostMapping("/baidu")
    public YueResult<String>pushBaidu(@Valid @RequestBody YueSeoUrlRequest yueSeoConfig, BindingResult result){
        if (result.hasErrors()){
            for (ObjectError error : result.getAllErrors()) {
                return YueResult.<String>builder().fail().code(500).message(error.getDefaultMessage()).build();
            }
        }
        return yueSeoConfigService.pushBaidu(yueSeoConfig.getUrls());
    }

}
