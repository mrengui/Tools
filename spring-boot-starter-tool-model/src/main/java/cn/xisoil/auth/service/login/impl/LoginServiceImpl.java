package cn.xisoil.auth.service.login.impl;


import cn.xisoil.auth.dao.user.UserRepository;
import cn.xisoil.auth.data.user.YueLoginUser;
import cn.xisoil.auth.dto.login.EditUserRequest;
import cn.xisoil.auth.dto.login.LoginDto;
import cn.xisoil.auth.dto.login.LoginRequest;
import cn.xisoil.auth.service.login.LoginPermissionUtils;
import cn.xisoil.auth.service.login.LoginService;
import cn.xisoil.auth.config.token.JwtUtils;
import cn.xisoil.common.enums.HTTPCODE;
import cn.xisoil.common.exception.NormalException;
import cn.xisoil.common.exception.ResponseException;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.system.model.dao.YueBasicDataRepository;
import cn.xisoil.system.model.data.YueBasicData;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private LoginPermissionUtils loginPermissionUtils;
    @Autowired
    private YueBasicDataRepository yueBasicDataRepository;

    @Override
    @Transactional
    public YueResult<LoginDto> userLogin(LoginRequest request) {
        YueLoginUser user=userRepository.findOneByAccount(request.getAccount()).orElseThrow(()->new ResponseException(403,"账号不存在"));
        userRepository.save(user);
        if (!new BCryptPasswordEncoder().matches(request.getPassword(), user.getPassword())){
            throw new ResponseException(403,"账号或密码错误");
        }
        String token=jwtUtils.jwt(user);
        LoginDto loginDto=new LoginDto();
        loginDto.setToken(token);
        loginDto.setUser(user);
        loginDto.setPermissions(loginPermissionUtils.structureByRoleId(user.getRoleId()));
        return YueResult.<LoginDto>builder()
                .result(HTTPCODE.SUCCESS)
                .message("登录成功")
                .data(loginDto)
                .build();
    }

    @Override
    public YueResult<YueLoginUser> getUserInfo() {
        YueLoginUser user= jwtUtils.getLoginUser();
        return YueResult.<YueLoginUser>builder().success().message("获取成功").data(userRepository.findById(user.getId()).get()).build();
    }

    @Override
    public YueResult<String> edit(EditUserRequest request) {
        YueLoginUser user=jwtUtils.getLoginUser();
        if (!user.getPassword().equals(request.getPassword())){
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String encode = passwordEncoder.encode(request.getPassword()); //进行动态加盐加密
            request.setPassword(encode);
        }
        BeanUtils.copyProperties(request,user);
        userRepository.save(user);
        return YueResult.<String>builder().success().message("修改成功").build();
    }


    @Override
    public YueResult<String> register(LoginRequest request) {
        if (userRepository.existsAllByAccount(request.getAccount())){
            throw new NormalException("该账号已注册");
        }
        YueLoginUser user=new YueLoginUser();
        BeanUtils.copyProperties(request,user);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encode = passwordEncoder.encode(request.getPassword()); //进行动态加盐加密
        user.setPassword(encode);
        user.setNickname(request.getAccount());
        YueBasicData yueBasicData=yueBasicDataRepository.findTopByNameIsNotNull().orElseThrow(()->new ResponseException("系统错误"));
        if (!yueBasicData.getIsRegister()) {
            throw new NormalException("未开放注册");
        }
        user.setRoleId(yueBasicData.getRoleId());
        userRepository.save(user);
        return YueResult.<String>builder().success().message("修改成功").build();
    }

}
