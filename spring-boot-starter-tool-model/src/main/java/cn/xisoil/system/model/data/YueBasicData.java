package cn.xisoil.system.model.data;

import cn.xisoil.curd.model.enums.ObjectColumnType;
import cn.xisoil.curd.model.interfaces.CurdModel;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;

@Data
@Entity(name = "YueBasicData")
@Table(name = "basic_data")
@EntityListeners(AuditingEntityListener.class)
@CurdModelObject(value = "系统配置", edit = false, add = false)
public class YueBasicData {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    @CurdModel(value = "网站名称",required = true)
    @NotBlank(message = "网站名称不能为空")
    private String name;

    @CurdModel(value = "网站简称")
    private String shortName;

    @CurdModel(value = "开启注册",type = ObjectColumnType.BOOLEAN)
    private Boolean isRegister;

    @CurdModel(value = "开启日志",type = ObjectColumnType.BOOLEAN)
    private Boolean isLog=false;

    @CurdModel(value = "默认角色",type = ObjectColumnType.LIST,url = "/role/list")
    private String roleId;

    @CurdModel(value = "网站logo",type = ObjectColumnType.IMAGE)
    private String logo;

    @CurdModel(value = "网站ICO",type = ObjectColumnType.IMAGE)
    private String ico;

    @CurdModel(value = "微信回调")
    private String wxNotifyUrl;

    @CurdModel(value = "通知地址")
    private String socketIo;

    @CurdModel(value = "邮箱服务器")
    private String emailSmtp;
    @CurdModel(value = "邮箱账号")
    private String emailAccount;
    @CurdModel(value = "邮箱密码")
    private String emailPassword;

    @CurdModel(value = "服务条款",type = ObjectColumnType.RICHTEXT)
    @Lob
    private String teamOfService;

}
