package cn.xisoil.task.controller;


import cn.xisoil.common.result.YueResult;
import cn.xisoil.datacheck.PermissionCheckAutomation;
import cn.xisoil.task.dto.TaskEditRequest;
import cn.xisoil.task.utils.TaskServiceConstructor;
import cn.xisoil.task.utils.TaskTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/manage/task")
public class YueTaskController  implements PermissionCheckAutomation {


    @Override
    public String getUrl() {
        return "/task";
    }

    @Override
    public String getName() {
        return "任务列表";
    }

    @Override
    public String getParent() {
        return "计划任务";
    }

    @Autowired
    private TaskTools taskTools;

    @PostMapping("/list")
    public YueResult<List<TaskServiceConstructor>>getTask(){
        List<TaskServiceConstructor>taskServices=taskTools.tasks();
        return YueResult.<List<TaskServiceConstructor>>builder().success().data(taskServices).build();
    }

    @PatchMapping("/cancel/{id}")
    public YueResult<String>cancel(@PathVariable String id){
        taskTools.close(id);
        return YueResult.<String>builder().success().message("关闭成功").build();
    }

    @PutMapping("")
    public YueResult<String>edit(@RequestBody TaskEditRequest request){
        taskTools.refreshCron(request.getId(),request.getCron());
        return YueResult.<String>builder().success().message("更新成功").build();
    }

    @PostMapping("/run/{id}")
    public YueResult<String>run(@PathVariable String id){
        taskTools.run(id);
        return YueResult.<String>builder().success().message("执行成功").build();
    }

    @PostMapping("/start/{id}")
    public YueResult<String>start(@PathVariable String id){
        taskTools.start(id);
        return YueResult.<String>builder().success().message("执行成功").build();
    }

}
