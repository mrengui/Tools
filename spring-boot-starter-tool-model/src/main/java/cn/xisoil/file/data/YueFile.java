package cn.xisoil.file.data;

import cn.xisoil.curd.model.enums.ObjectColumnType;
import cn.xisoil.curd.model.interfaces.CurdModel;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import jakarta.persistence.*;
import java.util.Date;

@Data
@Entity(name = "YueFile")
@Table(name = "yue_file")
@EntityListeners(AuditingEntityListener.class)
@CurdModelObject(value = "文件管理",add = false,edit = false)
public class YueFile {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    @CreatedDate
    @CurdModel(value = "上传时间")
    private Date createTime;

    @Enumerated(value = EnumType.STRING)
    @CurdModel(value = "存储方式",type = ObjectColumnType.LIST,url = "/file/config/type")
    private UPLOADTYPE uploadtype=UPLOADTYPE.LOCAL;

    public UPLOADTYPE getUploadtype() {
        return uploadtype==null?UPLOADTYPE.LOCAL:uploadtype;
    }

    @CurdModel(value = "文件名")
    private String filename;

    private String objectName;

    @CurdModel(value = "文件地址",type = ObjectColumnType.FILE)
    private String url;

    @CurdModel(value = "文件类型",type = ObjectColumnType.LIST,url = "/file/type",search = true)
    @Enumerated(EnumType.STRING)
    private YueFileType type;

}
