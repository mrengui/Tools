package cn.xisoil.auth.service.user.impl;

import cn.xisoil.auth.dao.user.UserRepository;
import cn.xisoil.auth.data.user.YueLoginUser;
import cn.xisoil.auth.service.user.UserService;
import cn.xisoil.auth.config.token.JwtUtils;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.common.to.SearchPageRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import jakarta.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private UserRepository userRepository;

    @Override
    public YueResult<Page<YueLoginUser>> page(SearchPageRequest searchPageRequest) {
        YueLoginUser user=jwtUtils.getLoginUser();
        Pageable pageable = PageRequest.of(searchPageRequest.getPageNum(), searchPageRequest.getPageSize());
        Specification<YueLoginUser> sectionSpecification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList();

            if (StringUtils.isNotBlank(searchPageRequest.getKeyword())){
                predicates.add(criteriaBuilder.like(root.get("account"),"%"+searchPageRequest.getKeyword()+"%"));
            }
            if (searchPageRequest.getOrderBy() != null) {
                criteriaQuery.orderBy(searchPageRequest.getOrderBy().getSortType().equals(Sort.Direction.DESC) ? criteriaBuilder.desc(root.get(searchPageRequest.getOrderBy().getColumn())) : criteriaBuilder.asc(root.get(searchPageRequest.getOrderBy().getColumn())));
            }
            return criteriaBuilder.and((Predicate[])predicates.toArray(new Predicate[0]));
        };
        Page<YueLoginUser> page = userRepository.findAll(sectionSpecification, pageable);
        return YueResult.<Page<YueLoginUser>>builder().message("获取成功").success().data(page).build();
    }

    @Override
    public YueResult<String> edit(YueLoginUser request) {
        YueLoginUser user=userRepository.findById(request.getId()).orElse(new YueLoginUser());
        if (!user.getPassword().equals(request.getPassword())){
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String encode = passwordEncoder.encode(request.getPassword()); //进行动态加盐加密
            request.setPassword(encode);
        }
        BeanUtils.copyProperties(request,user);
        userRepository.save(user);
        return YueResult.<String>builder().success().message("修改成功").build();
    }

    @Override
    public YueResult<String> add(YueLoginUser request) {
        YueLoginUser user=new YueLoginUser();
        BeanUtils.copyProperties(request,user);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encode = passwordEncoder.encode(request.getPassword()); //进行动态加盐加密
        user.setPassword(encode);
        userRepository.save(user);
        return YueResult.<String>builder().success().message("修改成功").build();
    }

    public static void main(String[] args) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encode = passwordEncoder.encode("123456"); //进行动态加盐加密
        System.out.println(encode);
    }

}
