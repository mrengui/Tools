package cn.xisoil.system.model.service.impl;

import cn.xisoil.common.enums.HTTPCODE;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.system.model.dao.YueBasicDataRepository;
import cn.xisoil.system.model.data.YueBasicData;
import cn.xisoil.system.model.service.YueBasicDataService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class YueBasicDataServiceImpl implements YueBasicDataService {

    @Autowired
    private YueBasicDataRepository yueBasicDataRepository;

    @Override
    public YueResult<YueBasicData> get() {
        return YueResult.<YueBasicData>builder().result(HTTPCODE.SUCCESS).data(yueBasicDataRepository.findTopByNameIsNotNull().orElse(new YueBasicData())).build();
    }

    @Override
    public YueResult<String> edit(YueBasicData edit) {
        YueBasicData basicData=yueBasicDataRepository.findTopByNameIsNotNull().orElse(new YueBasicData());
        BeanUtils.copyProperties(edit,basicData);
        yueBasicDataRepository.save(basicData);
        return YueResult.<String>builder().result(HTTPCODE.SUCCESS).build();
    }
}
