package cn.xisoil.system.model.data;

import lombok.Data;

@Data
public class YueDiskInfo {

    private String dirName;

    private String sysTypeName;

    private String typeName;

    private String total;

    private String free;

    private String used;

    private String usage;

}
