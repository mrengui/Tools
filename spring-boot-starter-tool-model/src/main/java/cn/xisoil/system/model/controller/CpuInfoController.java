package cn.xisoil.system.model.controller;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.datacheck.PermissionCheckAutomation;
import cn.xisoil.properties.YueProperties;
import cn.xisoil.system.model.data.YueDiskInfo;
import cn.xisoil.system.model.data.YueServerInfo;
import cn.xisoil.system.model.data.YueCpuInfo;
import cn.xisoil.system.model.data.YueMemoryInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.GlobalMemory;
import oshi.software.os.FileSystem;
import oshi.software.os.OSFileStore;
import oshi.util.Util;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryUsage;
import java.lang.management.RuntimeMXBean;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/manage/system")
public class CpuInfoController implements PermissionCheckAutomation {

    @Autowired
    private YueProperties yueProperties;

    @GetMapping("/cpu")
    public YueResult<YueCpuInfo> getCpu() {
        SystemInfo systemInfo = new SystemInfo();
        CentralProcessor processor = systemInfo.getHardware().getProcessor();
        long[] prevTicks = processor.getSystemCpuLoadTicks();
        Util.sleep(500);
        long[] ticks = processor.getSystemCpuLoadTicks();
        long nice = ticks[CentralProcessor.TickType.NICE.getIndex()] - prevTicks[CentralProcessor.TickType.NICE.getIndex()];
        long irq = ticks[CentralProcessor.TickType.IRQ.getIndex()] - prevTicks[CentralProcessor.TickType.IRQ.getIndex()];
        long softirq = ticks[CentralProcessor.TickType.SOFTIRQ.getIndex()] - prevTicks[CentralProcessor.TickType.SOFTIRQ.getIndex()];
        long steal = ticks[CentralProcessor.TickType.STEAL.getIndex()] - prevTicks[CentralProcessor.TickType.STEAL.getIndex()];
        long cSys = ticks[CentralProcessor.TickType.SYSTEM.getIndex()] - prevTicks[CentralProcessor.TickType.SYSTEM.getIndex()];
        long user = ticks[CentralProcessor.TickType.USER.getIndex()] - prevTicks[CentralProcessor.TickType.USER.getIndex()];
        long iowait = ticks[CentralProcessor.TickType.IOWAIT.getIndex()] - prevTicks[CentralProcessor.TickType.IOWAIT.getIndex()];
        long idle = ticks[CentralProcessor.TickType.IDLE.getIndex()] - prevTicks[CentralProcessor.TickType.IDLE.getIndex()];
        long totalCpu = user + nice + cSys + idle + iowait + irq + softirq + steal;

        YueCpuInfo yueCpuInfo = new YueCpuInfo();
        yueCpuInfo.setCpuCount(processor.getLogicalProcessorCount());// CPU的总量
        yueCpuInfo.setUtilization(new DecimalFormat("#.##%").format(cSys * 1.0 / totalCpu));// CPU的系统利用率
        yueCpuInfo.setUserUtilization(new DecimalFormat("#.##%").format(user * 1.0 / totalCpu));// CPU的用户利用率
        yueCpuInfo.setIdleness(new DecimalFormat("#.##%").format(idle * 1.0 / totalCpu));// CPU的空闲率
        return YueResult.<YueCpuInfo>builder().success().data(yueCpuInfo).build();

    }

    @GetMapping("/memory")
    public YueResult<YueMemoryInfo> getMemory() {
        SystemInfo systemInfo = new SystemInfo();
        GlobalMemory memory = systemInfo.getHardware().getMemory();
        long totalByte = memory.getTotal();
        long acaliableByte = memory.getAvailable();
        YueMemoryInfo yueMemoryInfo = new YueMemoryInfo();
        yueMemoryInfo.setMemorySize(formatByte(totalByte));
        yueMemoryInfo.setMemoryUsage(new DecimalFormat("#.##%").format((totalByte-acaliableByte- memory.getVirtualMemory().getSwapUsed())*1.0/totalByte));
        yueMemoryInfo.setMemoryFree(formatByte(acaliableByte));
        yueMemoryInfo.setMemoryUse(formatByte(totalByte-acaliableByte));


        MemoryUsage mxb = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
        yueMemoryInfo.setJvmMemorySize(formatByte(mxb.getMax()));
        yueMemoryInfo.setJvmMemoryFree(formatByte(mxb.getMax()-mxb.getCommitted()-mxb.getUsed()));
        yueMemoryInfo.setJvmMemoryUse(formatByte(mxb.getCommitted()+mxb.getUsed()));
        yueMemoryInfo.setJvmMemoryUsage(new DecimalFormat("#.##%").format((mxb.getCommitted()+mxb.getUsed())*1.0/mxb.getMax()));

        return YueResult.<YueMemoryInfo>builder().success().data(yueMemoryInfo).build();

    }

    @GetMapping("/server")
    public YueResult<YueServerInfo> getServer() throws UnknownHostException {
        YueServerInfo yueServerInfo = new YueServerInfo();
        Properties props = System.getProperties();

        InetAddress addr = InetAddress.getLocalHost();
        yueServerInfo.setServerIp(addr.getHostAddress());

        Map<String, String> map = System.getenv();
        yueServerInfo.setServerName(map.get("COMPUTERNAME"));// 获取计算机名

        yueServerInfo.setEquipment(props.getProperty("os.name"));
        yueServerInfo.setArchitecture(props.getProperty("os.arch"));

        yueServerInfo.setJavaServerName(props.getProperty("java.vm.name"));
        yueServerInfo.setJavaVersion(props.getProperty("java.version"));
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        yueServerInfo.setStartTime(simpleDateFormat.format(yueProperties.getStartTime()));
        yueServerInfo.setRunTime(yueProperties.getRunTime());
        yueServerInfo.setJavaDir(props.getProperty("java.home"));
        yueServerInfo.setProjectDir(props.getProperty("user.dir"));

        RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
        yueServerInfo.setRunParameters(bean.getInputArguments().toString());

        return YueResult.<YueServerInfo>builder().success().data(yueServerInfo).build();

    }

    @GetMapping("/disk")
    public YueResult<List<YueDiskInfo>> getDisk() throws UnknownHostException {
       List<YueDiskInfo>yueDiskInfos=new ArrayList<>();

        SystemInfo si = new SystemInfo();
        FileSystem fileSystem = si.getOperatingSystem().getFileSystem();
        List<OSFileStore> fsArray =fileSystem.getFileStores();
        for (OSFileStore fs : fsArray) {
            long free = fs.getUsableSpace();
            long total = fs.getTotalSpace();
            long used = total - free;
            YueDiskInfo sysFile = new YueDiskInfo();
            sysFile.setDirName(fs.getMount());
            sysFile.setSysTypeName(fs.getType());
            sysFile.setTypeName(fs.getName());
            sysFile.setTotal(formatByte(total));
            sysFile.setFree(formatByte(free));
            sysFile.setUsed(formatByte(used));
            sysFile.setUsage((new DecimalFormat("#.##%").format(used * 1.0 / total)));
            yueDiskInfos.add(sysFile);
        }
        return YueResult.<List<YueDiskInfo>>builder().success().data(yueDiskInfos).build();
    }


    public static String formatByte(long byteNumber){
        double FORMAT = 1024.0;
        double kbNumber = byteNumber/FORMAT;
        if(kbNumber<FORMAT){
            return new DecimalFormat("#.##KB").format(kbNumber);
        }
        double mbNumber = kbNumber/FORMAT;
        if(mbNumber<FORMAT){
            return new DecimalFormat("#.##MB").format(mbNumber);
        }
        double gbNumber = mbNumber/FORMAT;
        if(gbNumber<FORMAT){
            return new DecimalFormat("#.##GB").format(gbNumber);
        }
        double tbNumber = gbNumber/FORMAT;
        return new DecimalFormat("#.##TB").format(tbNumber);
    }


    @Override
    public String getUrl() {
        return "/system/server";
    }

    @Override
    public String getName() {
        return "服务监控";
    }

    @Override
    public String getParent() {
        return "系统管理";
    }
}
