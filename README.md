

<h1 align="center">Spring-boot-starter-tool-model</h1>

<p align="center">
    <!-- Java -->
    <img src="https://img.shields.io/badge/Java-ED8B00?logo=spring&logoColor=white" alt="Java Language, Spring Framework" />
    <!-- LICENSE -->
    <a target="_blank" href="https://gitee.com/mrengui/spring-boot-starter-tool-model/blob/vien-tools/LICENSE">
        <img src="https://img.shields.io/badge/LICENSE-Mulan-PSL--2" alt="MulanPSL2" />
    </a>
    <!-- GITEE-->
    <a target="_blank" href="https://gitee.com/mrengui/spring-boot-starter-tool-model">
        <img src="https://gitee.com/mrengui/spring-boot-starter-tool-model/badge/star.svg?theme=dark" />
    </a>
    <br />
    <!-- GITEE-->
    <a target="_blank" href="https://gitee.com/mrengui/spring-boot-starter-tool-model">
        <img src="https://gitee.com/mrengui/spring-boot-starter-tool-model/badge/fork.svg?theme=dark" />
    </a>
    <!-- AUTHOR-->
    <a target="_blank" href="https://gitee.com/mrengui_admin">
        <img src="https://img.shields.io/badge/Author-VIEN-orange.svg" />
    </a>
    <!-- AUTHOR-->
    <a target="_blank" href="https://repo.maven.apache.org/maven2/cn/xisoil/spring-boot-starter-tool-model/">
        <img src="https://img.shields.io/badge/version-v1.1.3-brightgreen.svg" />
    </a>
</p>

<p align="center">
    <a href="https://xisoil.cn">产品官网</a> | 
    <a href="https://gitee.com/mrengui/spring-boot-starter-tool-model">下载地址</a> | 
    <a href="https://gitee.com/mrengui/spring-boot-starter-tool-model/blob/vien-tools/READEME.md">文档资源</a> | 
    <a href="https://experience.xisoil.cn">体验环境</a> 
</p>
<hr>

## 项目介绍
<p>Spring-boot-starter-tool-moedl 是基于jdk 17，springframework 3.0.1的自动化实现数据增删改查的依赖。 可以使用极少的代码，实现数据对象的增删改查以及后台管理界面生成，让后端java开发者从繁琐的curd中解放双手，省去繁琐的前后端联调操作
</p>

## 如何使用
### 导入依赖
<p>项目中已经包含了一个spring-boot项目的大部分必须依赖，因此只需要在项目中导入依赖代码即可</p>

```
        <dependency>
            <groupId>cn.xisoil</groupId>
            <artifactId>spring-boot-starter-tool-model</artifactId>
            <version>1.1.3-RELEASE</version>
        </dependency>
```
<p>如果项目需要lombok,您依旧需要导入lombok</p>

```agsl
       <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.24</version>
        </dependency>
```

### 开启扫描
<p>基于springboot，您需要在启动文件上标注以下注解,@EnableModel为本项目启动注解方法</p>

```
@SpringBootApplication(scanBasePackages={"xx.xxx.*"})
@EnableModel
@EnableJpaAuditing
@EnableTransactionManagement
@EnableAsync
@EnableModel//开启注解
```

### 一些基本配置
<p>在springboot的配置基础上，spring-boot-starter-tool-model新增了某些配置，它具有某些默认值，也可以忽略</p>

```agsl
yue:
    equipment: windows//可选值，linux/windows,意义不大，某些情况下的端口占用会依靠它来解决
    socketio://用于支付回调的socket信息，其他配置参考netty
        host: 0.0.0.0
        port: 9009
    auto:
        check: false//是否自动检查数据，项目首次启动需要开启此处，项目会做一些数据的初始化操作
        user://开启检查时会初始化管理员账户
            username: xxx
            password: xxx
        package:
            controller: xx.xx.xx//控制层代码生成位置
            persistent: xx.xx.xx//持久层代码生成位置

        
```
<p>在完成以上配置后即可成功启动项目，访问地址：http://ip:port/admin，这样就完成了基本的项目配置，后台数据已经完成了初始化并且包含了一些基础的栏目</p>


## 思想：约定大于配置
<p>在自动生成的后台中接口调用中，我们已经做了某些约定。</p>
<ul>
    <li>项目使用JPA作为持久层，分页查询基于pageable完成</li>
    <li>后端API一级目录为/manage</li>
    <br>
    <li>接口规范(content-type:application/json)
        <ul>
            <li>分页查询：post方法，路由/xxx/page</li>
            <li>不分页查询：get方法，路由/xxx/list</li>
            <li>添加数据：post方法，路由/xxx</li>
            <li>修改数据：put方法，路由/xxx</li>
            <li>获取数据：get方法，路由/xxx/{id}</li>
            <li>删除数据：post方法，路由/xxx/delete</li>
        </ul>
        因此，你可以按照以上接口规范编写相关curd，后台可以获取到正确的数据
    </li>
    <br>
    <li>你可以使用项目封装的抽象controller方法与repository快速的实现CURD。
    <p>当我创建了一个实体类，只需要两个步骤就可以实现一个CURD</p>
    <ul>
        <li>持久层继承YueRepository
    
```
public interface BasicInfoRepository extends YueRepository<BasicInfo,String> {

}

```
</li>
<li>控制层继承SingleModelController或ModelCurdControllerMapping

```
@RestController
@RequestMapping("/manage/basicInfo")
public class BasicController extends SingleModelController<BasicInfo, BasicInfoRepository> {
}
```
</li>
    </ul>
    以上，一个完整的CURD就被实现了。
    </li>
</ul>


## 高阶操作：更好的实现CURD
以下是实体类注解
<ul>
<li>@CurdModelObject,标记实体类，描述类的相关信息</li>
<li>@CurdModel，标记类字段，描述字段信息</li>
<li>@CurdTrusteeShipObject，@CurdModelObject拓展使用，全自动生成CURD</li>
</ul>
为方便理解，下面展示了一个较为完整的类

```
@Data
@Entity(name = "Basic")
@Table(name = "BasicInfo")
//trusteeship不为空时会自动生成持久层与控制层，只需要一个实体类，就能自动实现增删改查
@CurdModelObject(name = "基本信息",trusteeship = @CurdTrusteeShipObject(auto = false))
@EntityListeners(AuditingEntityListener.class)
public class BasicInfo {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    //描述title
    @CurdModel(value = "网站主题")
    @NotBlank(message = "网站主题不能为空")
    private String title;

    //可以指定type
    @CurdModel(value = "ico图标",type = YueColumnsType.IMAGE)
    private String ico;

    @CurdModel(value = "logo1",type = YueColumnsType.IMAGE)
    private String logo;

    @CurdModel(value = "logo2",type = YueColumnsType.IMAGE)
    private String blogo;

    @CurdModel(value = "公司版权")
    private String company;

    @CurdModel(value = "版权所有")
    private String copyright;

    @CurdModel(value = "通知地址")
    private String email;

    @CurdModel(value = "seo-标题",type = YueColumnsType.TEXTAREA)
    private String tdkTitle;

    @CurdModel(value = "seo-描述",type = YueColumnsType.TEXTAREA)
    private String tdkRemark;

    @CurdModel(value = "seo-关键词",type = YueColumnsType.TEXTAREA)
    private String tdkKeywords;

    //富文本
    @CurdModel(value = "底部信息",type = YueColumnsType.RICHTEXT)
    @Lob
    private String footerInfo;


}
```

## 自带的数据统计


```
@VisitIntercept(clazz = xxxx.class,key = "sectionname")
```

@VisitIntercept标记的方法会进行栏目访问统计。

####  参数：

- key: 优先级最高，不为空时记录key值作为栏目名称


- clazz： YueAnalysisInterface的实现类，该方法包含了两个方法，getName(String) 与getName()，参数需要与args同时使用，使用该参数会通过调用getName方法获取sectionName,用户可自行编写逻辑获取sectionName


- args: 入参序号，当用户访问地址xxx?id=xxx时，request入参中第一个字段为id，则args=1,如果key值为空，则通过key值获取对应参数，调用YueAnalysisInterface的实现类的getSectionName方法获取sectionName

## 计划任务
使用案例,通过TaskService此方法实现的定时任务可以直接通过后台管理

```agsl
@Component
public class TaskUtils extends TaskService{


    public TaskUtils() {
        super("* * * * * ?");
    }


    @Override
    public String getTaskName() {
        return "计划任务-输出时间";
    }

    @Override
    public void run() {
        System.out.println(new Date().getTime());
    }
}
```

## 文件上传
我们新了一个模块专门用来实现文件上传，因此，你可以单独使用此模块实现文件上传，不过，spring-boot-starter-tool-model中已经包含了upload-model，如果你使用了tool-model，则无需重新引入

```agsl
       //  文件上传组件 
        <dependency>
            <groupId>cn.xisoil</groupId>
            <artifactId>spring-boot-starter-upload-model</artifactId>
            <version>1.0.1-RELEASE</version>
        </dependency>

```

### 使用方法
创建服务
```agsl

    //创建一个minio客户端
    MinioClient minioClient=UploadConfigClient
                .of(endpoint, bucketName, ak, sk)
                .minio()
    //创建一个OBS客户端
    ObsClient minioClient=UploadConfigClient
             .of(endpoint, bucketName, ak, sk)
             .obs()
    //创建一个OSS客户端
    OssClient minioClient=UploadConfigClient
             .of(endpoint, bucketName, ak, sk)
             .oss()
    
```
操作文件
```agsl
    //以minio举例
    //上传
    minioClient.upload(FileInfo)
    //FileInfo 包含了文件信息，其中name字段必填，剩下字段任选其一
    
    //删除
    minioClient.delete(filename);
    
```
