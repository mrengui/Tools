package cn.xisoil.common.exception;

public class NormalException extends RuntimeException  {

    public NormalException() {
        super();
    }
    public NormalException(String message) {
        super(message);
    }

}
