package cn.xisoil.analysis.controller;


import cn.xisoil.analysis.dto.AnalysisRequest;
import cn.xisoil.analysis.dto.StatisticalVo;
import cn.xisoil.analysis.sercvice.YueAnalysisService;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.log.interfaces.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/manage/analysis")
public class AnalysisController {

    @Autowired
    private YueAnalysisService yueAnalysisService;

    @GetMapping("/equipment")
    @Log("获取设备访问信息")
    public YueResult<AnalysisRequest>equipment(){
        return yueAnalysisService.equipment();
    }

    @GetMapping("/section")
    @Log("获取栏目访问信息")
    public YueResult<AnalysisRequest>getSectionBar(){
        return yueAnalysisService.getSectionBar();
    }

    @GetMapping("/browser")
    @Log("获取浏览器访问信息")
    public YueResult<AnalysisRequest>getBrowser(){
        return yueAnalysisService.getBrowser();
    }

    @GetMapping("/date")
    @Log("获取时间访问信息")
    public YueResult<StatisticalVo>getDate(){
        return yueAnalysisService.getPVAndUV();
    }

}
