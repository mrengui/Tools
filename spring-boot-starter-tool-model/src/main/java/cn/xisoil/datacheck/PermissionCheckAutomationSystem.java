package cn.xisoil.datacheck;

import cn.xisoil.auth.dao.permission.PermissionRepository;
import cn.xisoil.auth.dao.role.YueRoleRepository;
import cn.xisoil.auth.dao.user.UserRepository;
import cn.xisoil.auth.data.permission.Permission;
import cn.xisoil.auth.data.role.YueRole;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import jakarta.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class PermissionCheckAutomationSystem {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private Map<String, PermissionCheckAutomation> permissionCheckAutomationMap = new ConcurrentHashMap<>();

    void checkPermission() {
        logger.info("数据检查==>权限检查开始");
        permissionCheckAutomationMap.forEach((k, v) -> {
            if (v.auto()){
                logger.info("权限检查==>{}", v.getName());
                Permission permission = permissionRepository.findTopByName(v.getName())
                        .orElse(new Permission(v.getName(),v.getUrl()));
                if (StringUtils.isNotBlank(v.getParent())) {
                    Permission parent = permissionRepository.findTopByName(v.getParent())
                            .orElse(new Permission(v.getParent(), "/" + v.getParent(), "apps-o"));
                    parent = permissionRepository.save(parent);
                    permission.setParentId(parent.getId());
                } else {
                    permission.setIco("apps-o");
                }
                permissionRepository.save(permission);
                logger.info("权限检查==>{} 成功",v.getName());
            }
        });
    }


    @Autowired
    private YueRoleRepository yueRoleRepository;

    String checkRole() {
        List<String> permissionIds = permissionRepository.findAllId();

        YueRole role = yueRoleRepository.findTopByIdNotNull().orElse(new YueRole("初始化角色"));
        role.setPermissionIds(permissionIds);
        role = yueRoleRepository.save(role);
        logger.info("角色检查==>成功");
        return role.getId();
    }

}
