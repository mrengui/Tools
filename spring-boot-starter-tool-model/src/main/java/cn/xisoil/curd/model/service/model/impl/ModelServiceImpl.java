package cn.xisoil.curd.model.service.model.impl;

import cn.xisoil.curd.model.data.ModelClass;
import cn.xisoil.curd.model.data.ModelColumn;
import cn.xisoil.curd.model.enums.ObjectColumnType;
import cn.xisoil.curd.model.interfaces.CurdModel;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import cn.xisoil.curd.model.service.model.ModelService;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static cn.xisoil.curd.model.enums.ObjectColumnType.*;


@Service
public class ModelServiceImpl implements ModelService {

    private static final List<ObjectColumnType> KEYWORD_SEARCH = Arrays.asList(STRING, NUMBER, TEXTAREA);
    private static final List<ObjectColumnType> LIST_SEARCH = Arrays.asList(LIST, CHECKBOX, RADIO, LISTMULTISELECT);


    @Override
    public List<ModelColumn> getModelColumns(Class<?> clazz) {
        List<Field[]> list = new LinkedList<>();
        List<ModelColumn> modelColumns = new ArrayList<>();
        list.add(clazz.getSuperclass().getDeclaredFields());
        list.add(clazz.getDeclaredFields());
        list.forEach(fields -> {
            for (Field field : fields) {
                boolean annotationPresent = field.isAnnotationPresent(CurdModel.class);
                if (annotationPresent) {
                    CurdModel curdModel = field.getAnnotation(CurdModel.class);
                    modelColumns.add(ModelColumn.builder()
                            .column(field.getName())
                            .name(curdModel.value())
                            .type(curdModel.type())
                            .edit(curdModel.editor())
                            .search(curdModel.search())
                            .show(curdModel.show())
                            .url(curdModel.url())
                            .comparable(curdModel.comparable())
                            .required(curdModel.required())
                            .placeholder(curdModel.placeholder())
                            .group(curdModel.group())
                            .build());
                }
            }
        });
        return modelColumns;
    }

    @Override
    public ModelClass getModelClass(Class<?> clazz) {
        CurdModelObject modelClass = clazz.getAnnotation(CurdModelObject.class);
        ModelClass yueModelClassData = new ModelClass();
        if (modelClass != null) {
            yueModelClassData.setAdd(modelClass.add());
            yueModelClassData.setName(modelClass.value());
            yueModelClassData.setEdit(modelClass.edit());
            yueModelClassData.setDelete(modelClass.delete());
        }
        return yueModelClassData;
    }

    @Override
    public List<String> getSearchColumn(Class<?> clazz) {
        List<Field[]> list = new LinkedList<>();
        List<String> columns = new ArrayList<>();
        list.add(clazz.getSuperclass().getDeclaredFields());
        list.add(clazz.getDeclaredFields());
        list.forEach(fields -> {
            for (Field field : fields) {
                boolean annotationPresent = field.isAnnotationPresent(CurdModel.class);
                CurdModel curdModel = field.getAnnotation(CurdModel.class);
                if (annotationPresent &&  curdModel.search() && KEYWORD_SEARCH.contains(curdModel.type())) {
                    columns.add(field.getName());
                }
            }
        });
        return columns;
    }

    @Override
    public List<ModelColumn> getListSearchColumn(Class<?> clazz) {
        List<Field[]> list = new LinkedList<>();
        List<ModelColumn> modelColumns = new ArrayList<>();
        list.add(clazz.getSuperclass().getDeclaredFields());
        list.add(clazz.getDeclaredFields());
        list.forEach(fields -> {
            for (Field field : fields) {
                boolean annotationPresent = field.isAnnotationPresent(CurdModel.class);
                CurdModel curdModel = field.getAnnotation(CurdModel.class);
                if (annotationPresent &&  curdModel.search() &&  LIST_SEARCH.contains(curdModel.type())) {
                    modelColumns.add(ModelColumn.builder()
                            .column(field.getName())
                            .type(curdModel.type())
                            .build());
                }
            }
        });
        return modelColumns;
    }

    @Override
    public String getName(Class<?> clazz) {
        CurdModelObject modelClass = AnnotationUtils.getAnnotation(clazz, CurdModelObject.class);
        if (modelClass != null) {
            return modelClass.value();
        } else {
            return "";
        }
    }
}
