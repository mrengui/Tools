package cn.xisoil.file.dao;

import cn.xisoil.curd.dao.YueRepository;
import cn.xisoil.file.data.YueFile;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FileRepository extends YueRepository<YueFile,String> {


    @Query(value = "select file from YueFile file where file.id in ?1")
    List<YueFile> findUrlByIdIn(List<String> ids);

}
