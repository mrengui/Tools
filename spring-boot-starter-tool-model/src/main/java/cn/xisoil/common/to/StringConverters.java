package cn.xisoil.common.to;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Converter
@Component
public class StringConverters implements AttributeConverter<List<String>,String> {
    @Override
    public String convertToDatabaseColumn(List<String> attribute) {
        return attribute != null ? String.join(",", attribute) : null;
    }

    @Override
    public List<String> convertToEntityAttribute(String dbData) {
        if (StringUtils.isNotBlank(dbData))
            return new ArrayList<>(Arrays.asList(dbData.split(",")));
        else return new ArrayList<>();
    }



}
