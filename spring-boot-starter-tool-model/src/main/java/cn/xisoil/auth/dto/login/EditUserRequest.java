package cn.xisoil.auth.dto.login;

import lombok.Data;

@Data
public class EditUserRequest {


    private String account;

    private String password;

    private String nickname;

    private String headImage;

}
