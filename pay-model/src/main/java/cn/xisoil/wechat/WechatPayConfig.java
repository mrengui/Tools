package cn.xisoil.wechat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.wechat.pay.contrib.apache.httpclient.WechatPayHttpClientBuilder;
import com.wechat.pay.contrib.apache.httpclient.auth.PrivateKeySigner;
import com.wechat.pay.contrib.apache.httpclient.auth.Verifier;
import com.wechat.pay.contrib.apache.httpclient.auth.WechatPay2Credentials;
import com.wechat.pay.contrib.apache.httpclient.auth.WechatPay2Validator;
import com.wechat.pay.contrib.apache.httpclient.cert.CertificatesManager;
import com.wechat.pay.contrib.apache.httpclient.exception.HttpCodeException;
import com.wechat.pay.contrib.apache.httpclient.util.PemUtil;
import lombok.Data;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;

@Configuration
@Data
@ConfigurationProperties(prefix = "yue.pay.wechat", ignoreUnknownFields = true,ignoreInvalidFields = true)
public class WechatPayConfig implements InitializingBean {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private String appId;

    private String appSecret;

    private String appV3Secret;

    private String mchId;

    private String  privateKeyPath="";

    private String serialNo;

    private PrivateKey privateKey;

    private  CloseableHttpClient httpClient;


    @Override
    public void afterPropertiesSet() throws Exception {
        if (privateKeyPath.equals("")){
            logger.warn("微信支付===>证书地址错误{请填写正确的apiclient_key.pem地址}");
        }
        else {
            privateKey= PemUtil.loadPrivateKey(new ClassPathResource(privateKeyPath).getInputStream()) ;
            initWechatHttp();
            logger.info("微信支付已准备");
        }
    }


    public void initWechatHttp() throws Exception {
        // 获取证书管理器实例
        CertificatesManager certificatesManager = CertificatesManager.getInstance();
        certificatesManager.putMerchant(mchId, new WechatPay2Credentials(mchId,
                new PrivateKeySigner(serialNo, privateKey)), appV3Secret.getBytes(StandardCharsets.UTF_8));
        // 从证书管理器中获取verifier
        Verifier verifier = certificatesManager.getVerifier(mchId);
        WechatPayHttpClientBuilder builder = WechatPayHttpClientBuilder.create()
                .withMerchant(mchId, serialNo, privateKey)
                .withValidator(new WechatPay2Validator(verifier));
        // ... 接下来，你仍然可以通过builder设置各种参数，来配置你的HttpClient
        httpClient = builder.build();

    }


}
