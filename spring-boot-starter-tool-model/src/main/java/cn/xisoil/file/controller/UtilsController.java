package cn.xisoil.file.controller;

import cn.xisoil.common.result.ResultFile;
import cn.xisoil.file.service.upload.UploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;

@RequestMapping("")
@RestController
public class UtilsController{

    @Autowired
    private UploadFileService uploadFileService;

    @PostMapping("/manage/utils/upload")
    public ResultFile<String> upload(HttpServletRequest request){
        return uploadFileService.upload(request);
    }

}
