package cn.xisoil.system.portkill;

import cn.xisoil.properties.YueProperties;
import cn.xisoil.system.model.enums.EQUIPMENT;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


@Component
public class PortKillConfig {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private YueProperties yueProperties;

    @PostConstruct
    public void  init(){
        if (EQUIPMENT.windows.equals(yueProperties.getEquipment())){
            String res=exec(new String[]{"netstat","-ano"});
            if (StringUtils.isNotBlank(res)){
                logger.warn("端口占用！！！！抹杀！！！！");
                String pid= res.substring(res.lastIndexOf("LISTENING")+9);
                exec(new String[]{"taskkill", "-PID", pid ,"-F"});
                logger.info("{}已抹杀",pid);
            }
        }
        else {
            String res=exec(new String[]{"lsof","-i:"+ yueProperties.getPort()});
            if (StringUtils.isNotBlank(res)){
                logger.warn("端口占用！！！！抹杀！！！！");
                List<String> list=new ArrayList<String>(List.of(res.split("\\D+")));
                list.remove("");
                String pid=list.get(0);
                exec(new String[]{"kill", "-9", pid});
                logger.info("{}已抹杀",pid);
            }
        }
    }

    public  String exec(String[]para) {
        try {
            Process proc = Runtime.getRuntime().exec(para);
            BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            StringBuilder rs = new StringBuilder();
            String line;
            while ((line = in.readLine()) != null ) {
                if (line.contains(yueProperties.getPort()) && ( !yueProperties.getEquipment().equals(EQUIPMENT.windows) || line.contains(" LISTENING") ) ){
                    rs.append(line.replace(" ",""));
                    break;
                }
            }
            in.close();
            proc.waitFor();
            return rs.toString();
        } catch (Exception e){
            logger.warn("啊这，系统环境错误，无法抹杀！！！！");
        }
        return "";
    }


}
