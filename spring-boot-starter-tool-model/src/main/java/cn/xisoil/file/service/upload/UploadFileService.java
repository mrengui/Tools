package cn.xisoil.file.service.upload;

import cn.xisoil.common.result.ResultFile;

import jakarta.servlet.http.HttpServletRequest;

public interface UploadFileService {

    ResultFile<String> upload(HttpServletRequest request);

}
