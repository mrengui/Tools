package cn.xisoil.auth.interfaces;



import java.lang.annotation.*;

@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AutoPermissionIntercept {

    String message() default "权限不足，禁止访问!";

    String[] permission() default {};

    String[] role() default {};

    boolean login() default true;


}
