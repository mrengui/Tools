package cn.xisoil.service.impl;


import cn.xisoil.config.BeanConfig;
import cn.xisoil.service.SyncEsRepository;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import jakarta.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public class SyncEsRepositoryImpl<T1,ID extends Serializable> extends SimpleJpaRepository<T1, ID> implements SyncEsRepository<T1,ID> {


    public SyncEsRepositoryImpl(JpaEntityInformation<T1, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
    }


    private Class<T1> t1Class=this.getDomainClass();


    public SyncEsRepositoryImpl(Class<T1> domain1Class, EntityManager em) {
        super(domain1Class, em);
        this.t1Class=domain1Class;
    }

    @Override
    public T1 syncSave(T1 t) {
        T1 o=super.save(t);
        try{
            BeanConfig.elasticsearchRestTemplate.save(o,getIndexCoordinatesFor(t1Class));
        }
        catch (Exception ignored){}
        return o;
    }

    @Override
    public void syncDelete(T1 t) {
        super.delete(t);
        try {
            BeanConfig.elasticsearchRestTemplate.delete(t,getIndexCoordinatesFor(t1Class));
        }
        catch (Exception e){}
    }

    @Override
    public void syncDeleteById(ID id) {
        super.deleteById(id);
        try {
            BeanConfig.elasticsearchRestTemplate.delete(id.toString(), t1Class);
        }
        catch (Exception e){}
    }

    @Override
    public void syncDeleteByIdIn(List<ID> ids) {
        ids.forEach(id->{
            syncDeleteById((ID) id);
        });
    }

    @Override
    public SearchHits<T1> esAll() {
        NativeSearchQueryBuilder builder=new NativeSearchQueryBuilder();
        NativeSearchQuery query=builder
                .build();
        SearchHits<T1> search = BeanConfig.elasticsearchRestTemplate.search(query, t1Class,getIndexCoordinatesFor(t1Class));
        return search;
    }

    @Override
    public T1 esById(ID id) {
        return BeanConfig.elasticsearchRestTemplate.get(id.toString(),t1Class,getIndexCoordinatesFor(t1Class));
    }

    @Override
    public void syncDeleteAll(List<T1> ts) {
        super.deleteAll(ts);
        ts.forEach(t->{
            BeanConfig.elasticsearchRestTemplate.delete(t,getIndexCoordinatesFor(t1Class));
        });
    }

    @Override
    public void deleteAllByIdIn(List<String> ids) {
        ids.forEach(id->{
           super.deleteById((ID) id);
        });
    }

    @Override
    public Optional<T1> findTopByIdNotNull() {
        return Optional.empty();
    }

    public<K> K newObj(Class<K> kClass){
        K newObj;
        try {
            newObj = kClass.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return newObj;
    }

    public <T> IndexCoordinates getIndexCoordinatesFor(Class<T> clazz) {
        IndexCoordinates indexCoordinates =  BeanConfig.elasticsearchRestTemplate.getIndexCoordinatesFor(clazz);
        return IndexCoordinates.of(indexCoordinates.getIndexName().toLowerCase(Locale.ROOT));
    }

}
