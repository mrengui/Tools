package cn.xisoil.analysis.sercvice;


import cn.xisoil.analysis.dao.YueAnalysisRepository;
import cn.xisoil.analysis.data.YueAnalysis;
import cn.xisoil.analysis.interfaces.VisitIntercept;
import cn.xisoil.analysis.utils.AnalysisUtils;
import cn.xisoil.utils.HttpRequestIpUtil;
import cn.xisoil.utils.YueContextUtils;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import jakarta.servlet.http.HttpServletRequest;

@Component
public class YueAnalysisUtils {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private YueContextUtils yueContextUtils;
    @Autowired
    private HttpRequestIpUtil httpRequestIpUtil;
    @Autowired
    private YueAnalysisRepository yueAnalysisRepository;
    @Autowired
    private AnalysisUtils analysisUtils;

    @Async
    public void analysis(JoinPoint point, VisitIntercept visitIntercept){
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        YueAnalysisInterface yueAnalysisInterface =yueContextUtils.getBeanForType(visitIntercept.clazz());

//        String clientType = userAgent.getOperatingSystem().getDeviceType().toString();

        YueAnalysis yueAnalysis = new YueAnalysis();

        //ip
        String ip = httpRequestIpUtil.getIpAddress(httpServletRequest);
        yueAnalysis.setIp(ip);

        //设备浏览器
        UserAgent userAgent = UserAgent.parseUserAgentString(httpServletRequest.getHeader("user-agent"));
        String browser = userAgent.getBrowser().name();
        yueAnalysis.setBrowser(analysisUtils.getBrowser(browser));
        String os = userAgent.getOperatingSystem().name();
        yueAnalysis.setEquipment(analysisUtils.getEquipment(os));

        //栏目数据
        if (StringUtils.isNotBlank(visitIntercept.key())){
            yueAnalysis.setSectionName(visitIntercept.key());
        }
        else if (visitIntercept.args()>=0){
            Object[] tags= point.getArgs();
            if (ObjectUtils.isEmpty(tags)||tags.length< visitIntercept.args()){
                logger.warn("数据分析系统===>您指定了args为{}，传入参数数量为{}",visitIntercept.args(),tags.length);
            }
            else {
                yueAnalysis.setSectionName(yueAnalysisInterface.getName(tags[visitIntercept.args()-1]));
            }
        }
        else {
            yueAnalysis.setSectionName(yueAnalysisInterface.getName());
        }
        yueAnalysisRepository.save(yueAnalysis);
    }

}
