package cn.xisoil.auth.dao.permission;

import cn.xisoil.auth.data.permission.Permission;
import cn.xisoil.curd.dao.YueRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

public interface PermissionRepository extends YueRepository<Permission,String> {

    List<Permission> findAllByParentIdIsNull();

    List<Permission> findAllByIdIn(List<String> ids);

    boolean existsAllByName(String title);


    Optional<Permission> findTopByName(String title);


    @Query("select permission.id from Permission permission where permission.parentId is null")
    List<String>findAllId();

}
