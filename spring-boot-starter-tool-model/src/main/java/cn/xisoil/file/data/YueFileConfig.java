package cn.xisoil.file.data;


import cn.xisoil.curd.model.enums.ObjectColumnType;
import cn.xisoil.curd.model.interfaces.CurdModel;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.*;

@Data
@Entity(name = "YueFileConfig")
@Table(name = "yue_file_config")
@EntityListeners(AuditingEntityListener.class)
@CurdModelObject(value = "文件配置",add = false,edit = false)
public class YueFileConfig {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    @Enumerated(value = EnumType.STRING)
    @CurdModel(value = "存储方式",type = ObjectColumnType.LIST,url = "/file/config/type")
    private UPLOADTYPE uploadtype=UPLOADTYPE.LOCAL;

    @CurdModel(value = "文件节点",group = "阿里云存储")
    private String ossEndpoint;

    @CurdModel(value = "KEY ID",group = "阿里云存储")
    private String ossAk;

    @CurdModel(value = "密钥",group = "阿里云存储")
    private String ossSk;

    @CurdModel(value = "通道名称",group = "阿里云存储")
    private String ossBucketName;


    @CurdModel(value = "文件节点",group = "华为云存储")
    private String obsEndpoint;

    @CurdModel(value = "KEY ID",group = "华为云存储")
    private String obsAk;

    @CurdModel(value = "密钥",group = "华为云存储")
    private String obsSk;

    @CurdModel(value = "通道名称",group = "华为云存储")
    private String obsBucketName;


    @CurdModel(value = "文件节点",group = "MINIO云存储")
    private String minioEndpoint;

    @CurdModel(value = "KEY ID",group = "MINIO云存储")
    private String minioAk;

    @CurdModel(value = "密钥",group = "MINIO云存储")
    private String minioSk;

    @CurdModel(value = "通道名称",group = "MINIO云存储")
    private String minioBucketName;

}
