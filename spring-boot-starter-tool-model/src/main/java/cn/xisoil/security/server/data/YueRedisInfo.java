package cn.xisoil.security.server.data;

import lombok.Data;


@Data
public class YueRedisInfo {

    private String key;

    private String value;

    private Long expireTime;

}
