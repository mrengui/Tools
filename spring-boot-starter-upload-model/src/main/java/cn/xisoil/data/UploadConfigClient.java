package cn.xisoil.data;

import cn.xisoil.exception.FileUploadException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.obs.services.ObsConfiguration;
import io.minio.*;
import io.minio.http.Method;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * @Description 第三方文件上传API
 * @Author Vien
 * @CreateTime 2023-03-2023/3/9 16:22:27
 **/
public class UploadConfigClient {

    private final String endPoint;
    private final String bucketName;
    private final String ak;
    private final String sk;


    public OssClient oss() {
        return new OssClient().init();
    }


    public ObsClient obs() {
        return new ObsClient().init();
    }


    public MinioClient minio() {
        return new MinioClient().init();
    }

    private UploadConfigClient(String endPoint, String bucketName, String ak, String sk) {
        this.endPoint = endPoint;
        this.bucketName = bucketName;
        this.ak = ak;
        this.sk = sk;
    }

    public static UploadConfigClient of(String endPoint, String bucketName, String ak, String sk) {
        return new UploadConfigClient(endPoint, bucketName, ak, sk);
    }

    public class OssClient {

        private OSS oss;


        private OssClient init() {
            oss = new OSSClientBuilder().build(endPoint, ak, sk);
            return this;
        }

        public String upload(FileInfo fileInfo) {
            if (StringUtils.isBlank(fileInfo.name)) {
                throw new FileUploadException("文件名异常");
            }
            try {
                PutObjectRequest putObjectRequest;
                if (fileInfo.file != null) {
                    putObjectRequest = new PutObjectRequest(bucketName, fileInfo.name, fileInfo.file);
                } else if (fileInfo.inputStream != null) {
                    putObjectRequest = new PutObjectRequest(bucketName, fileInfo.name, fileInfo.inputStream);
                } else if (StringUtils.isNotBlank(fileInfo.content)) {
                    putObjectRequest = new PutObjectRequest(bucketName, fileInfo.name, new ByteArrayInputStream(fileInfo.content.getBytes()));

                } else if (fileInfo.bytes != null) {
                    putObjectRequest = new PutObjectRequest(bucketName, fileInfo.name, new ByteArrayInputStream(fileInfo.bytes));
                } else if (fileInfo.byteArrayInputStream != null) {
                    putObjectRequest = new PutObjectRequest(bucketName, fileInfo.name, fileInfo.byteArrayInputStream);
                } else {
                    throw new FileUploadException("文件格式异常");
                }
                putObjectRequest.setProcess("true");
                PutObjectResult result = oss.putObject(putObjectRequest);
                return result.getResponse().getUri();

            } catch (Exception e) {
                throw new FileUploadException(e.getMessage());
            }


        }

        public Boolean delete(String fileName) {
            return oss.deleteObject(bucketName, fileName).getResponse().isSuccessful();
        }
    }

    public class ObsClient {
        private static com.obs.services.ObsClient obs;

        private ObsClient init() {

            ObsConfiguration config = new ObsConfiguration();
            config.setSocketTimeout(30000);
            config.setConnectionTimeout(10000);
            config.setEndPoint(endPoint);
            obs = new com.obs.services.ObsClient(ak, sk, config);
            return this;
        }

        public String upload(FileInfo fileInfo) {
            if (StringUtils.isBlank(fileInfo.name)) {
                throw new FileUploadException("文件名异常");
            }
            try {
                com.obs.services.model.PutObjectResult result;
                if (fileInfo.file != null) {
                    result = obs.putObject(bucketName, fileInfo.name, fileInfo.file);
                } else if (fileInfo.inputStream != null) {
                    result = obs.putObject(bucketName, fileInfo.name, fileInfo.inputStream);
                } else if (StringUtils.isNotBlank(fileInfo.content)) {
                    result = obs.putObject(bucketName, fileInfo.name, new ByteArrayInputStream(fileInfo.content.getBytes()));
                } else if (fileInfo.bytes != null) {
                    result = obs.putObject(bucketName, fileInfo.name, new ByteArrayInputStream(fileInfo.bytes));
                } else if (fileInfo.byteArrayInputStream != null) {
                    result = obs.putObject(bucketName, fileInfo.name, fileInfo.byteArrayInputStream);
                } else {
                    throw new FileUploadException("文件格式异常");
                }
                return result.getObjectUrl();
            } catch (Exception e) {
                throw new FileUploadException(e.getMessage());
            }


        }

        public Boolean delete(String fileName) {
            return obs.deleteObject(bucketName, fileName).isDeleteMarker();
        }


    }

    public class MinioClient {

        private static io.minio.MinioClient minio;

        private MinioClient init() {
            try {
                minio = new io.minio.MinioClient
                        .Builder()
                        .endpoint(endPoint)
                        .credentials(ak, sk)
                        .build();
                return this;
            } catch (Exception e) {
                throw new FileUploadException("MinIo 初始化失败");
            }
        }


        public String upload(FileInfo fileInfo) {
            if (StringUtils.isBlank(fileInfo.name)) {
                throw new FileUploadException("文件名异常");
            }
            try {
                InputStream inputStream;
                if (fileInfo.file != null) {
                    inputStream=new FileInputStream(fileInfo.file);

                } else if (fileInfo.inputStream != null) {
                    inputStream=fileInfo.inputStream;

                } else if (StringUtils.isNotBlank(fileInfo.content)) {
                    inputStream=new ByteArrayInputStream(fileInfo.content.getBytes());

                } else if (fileInfo.bytes != null) {
                     inputStream=new ByteArrayInputStream(fileInfo.bytes);

                } else if (fileInfo.byteArrayInputStream != null) {
                    inputStream=fileInfo.byteArrayInputStream;
                } else {
                    throw new FileUploadException("文件格式异常");
                }
                ObjectWriteResponse result=minio.putObject(
                        PutObjectArgs
                                .builder()
                                .bucket(bucketName)
                                .object(fileInfo.name)
                                .stream(inputStream,inputStream.available(), -1)
                                .build());
                return endPoint+"/"+bucketName+"/"+ fileInfo.name;
            } catch (Exception e) {
                throw new FileUploadException(e.getMessage());
            }


        }

        public Boolean delete(String fileName) {
            try {
                minio.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(fileName).build());
                return true;
            } catch (Exception e) {
                return false;
            }
        }


    }


}
