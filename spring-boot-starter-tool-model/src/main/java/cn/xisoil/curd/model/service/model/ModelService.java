package cn.xisoil.curd.model.service.model;

import cn.xisoil.curd.model.data.ModelColumn;
import cn.xisoil.curd.model.data.ModelClass;

import java.util.List;

public interface ModelService {

    List<ModelColumn>getModelColumns(Class<?> clazz);

    ModelClass getModelClass(Class<?> clazz);

    List<String>getSearchColumn(Class<?> clazz);

    List<ModelColumn>getListSearchColumn(Class<?> clazz);


    String getName(Class<?> clazz);

}
