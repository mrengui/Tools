package cn.xisoil.curd.model.controller;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.curd.dao.YueRepository;
import cn.xisoil.curd.model.data.ModelClass;
import cn.xisoil.curd.model.data.ModelColumn;
import cn.xisoil.curd.model.enums.ObjectType;
import cn.xisoil.curd.model.service.single.SingleDaoServiceImpl;
import cn.xisoil.curd.model.service.model.ModelService;
import cn.xisoil.datacheck.PermissionCheckAutomation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import jakarta.validation.Valid;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class SingleModelController<T,R extends YueRepository<T,String>> extends SingleDaoServiceImpl<T,R> implements PermissionCheckAutomation<T> {

    @Autowired
    private ModelService modelService;

    @GetMapping("/columns")
    public YueResult<ModelClass> columns() {
        ModelClass yueModelClass= modelService.getModelClass(super.getGenericClass());
        yueModelClass.setType(ObjectType.SINGLE);
        List<ModelColumn>columns= modelService.getModelColumns(super.getGenericClass());
        Map<String,List<ModelColumn>> groups=columns.stream().collect(Collectors.groupingBy(ModelColumn::getGroup));
        yueModelClass.setColumns(columns);
        yueModelClass.setGroups(groups);
        return YueResult.<ModelClass>builder().data(yueModelClass).success().message("获取成功").build();
    }

    @GetMapping("")
    public YueResult<T>get(){
        return super.get();
    }

    @PutMapping("")
    public YueResult<String>edit(@Valid  @RequestBody T r, BindingResult result){
        if (result.hasErrors()){
            for (ObjectError error : result.getAllErrors()) {
                return YueResult.<String>builder().fail().code(500).message(error.getDefaultMessage()).build();
            }
        }
        return super.edit(r);
    }

}
