package cn.xisoil.datacheck;

import cn.xisoil.auth.dao.permission.PermissionRepository;
import cn.xisoil.auth.dao.role.YueRoleRepository;
import cn.xisoil.auth.dao.user.UserRepository;
import cn.xisoil.auth.data.role.YueRole;
import cn.xisoil.auth.data.user.YueLoginUser;
import cn.xisoil.properties.AutoMationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DataCheckAutomation implements ApplicationRunner {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    private AutoMationProperties autoMationProperties;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PermissionCheckAutomationSystem permissionCheckAutomationSystem;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (autoMationProperties.getCheck()){
            logger.info("数据检查==>数据检查开始");
            permissionCheckAutomationSystem.checkPermission();
            String roleId= permissionCheckAutomationSystem.checkRole();

            YueLoginUser yueLoginUser=userRepository.findOneByAccount(autoMationProperties.getUser().getUsername())
                    .orElse(new YueLoginUser(autoMationProperties.getUser().getUsername()));
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String encode = passwordEncoder.encode(autoMationProperties.getUser().getPassword()); //进行动态加盐加密
            yueLoginUser.setPassword(encode);
            yueLoginUser.setRoleId(roleId);
            userRepository.save(yueLoginUser);
            logger.info("初始化用户==>{}",yueLoginUser.getAccount());
        }
    }
}
