package cn.xisoil.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

/**
 * @Description 泛型工具类
 * @Author Vien
 * @CreateTime 2023-02-2023/2/11 15:00:05
 **/
public class  GenericObject<T>{

    private  final Logger logger = LoggerFactory.getLogger(this.getClass());

    private Class<T> clazz=null;

    public GenericObject() {
        Type type = this.getClass().getGenericSuperclass();
        try{
            this.clazz = (Class<T>) ((ParameterizedType) type).getActualTypeArguments()[0];
        }
        catch (Exception  e){
            logger.warn("泛型类型获取失败==>{}未指定类型",this.getClass().getName());
        }

    }

    public Class<T> getGenericClass() {
        return clazz;
    }

    public T object(){
        T newObj;
        try {
            newObj = clazz.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return newObj;
    }

    public<A extends Annotation> A getAnnotation(Class<A> aClass){
        return clazz.getAnnotation(aClass);
    }

    public List<Annotation> getAnnotations(){
        return Arrays.stream(clazz.getAnnotations()).toList();
    }


}
