package cn.xisoil.socket;

import com.corundumstudio.socketio.SocketConfig;
import com.corundumstudio.socketio.SocketIOServer;
import jakarta.annotation.Resource;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


/**
 * @ClassNameSocketIOConfig
 * @Description TODO
 * @Date 2022/1/2114:19
 * @Version 1.0
 **/
@Configuration
@Data
@ConfigurationProperties(prefix = "yue.socketio", ignoreUnknownFields = true,ignoreInvalidFields = true)
public class SocketIOConfig implements InitializingBean {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private SocketIOHandler socketIOHandler;

    private String host="localhost";

    private Integer port=9092;

    private int bossCount=1;

    private int workCount=100;

    private boolean allowCustomRequests=true;

    private int upgradeTimeout=1000000;

    private int pingTimeout=6000000;

    private int pingInterval=25000;

    @Override
    public void afterPropertiesSet() throws Exception {
        SocketConfig socketConfig = new SocketConfig();
        socketConfig.setReuseAddress(true);
        socketConfig.setTcpNoDelay(true);
        socketConfig.setSoLinger(0);

        com.corundumstudio.socketio.Configuration configuration = new com.corundumstudio.socketio.Configuration();
        configuration.setSocketConfig(socketConfig);
        // host在本地测试可以设置为localhost或者本机IP，在Linux服务器跑可换成服务器IP
        configuration.setHostname(host);
        configuration.setPort(port);
        // socket连接数大小（如只监听一个端口boss线程组为1即可）
        configuration.setBossThreads(bossCount);
        configuration.setWorkerThreads(workCount);
        configuration.setAllowCustomRequests(allowCustomRequests);

        // 协议升级超时时间（毫秒），默认10秒。HTTP握手升级为ws协议超时时间
        configuration.setUpgradeTimeout(upgradeTimeout);
        // Ping消息超时时间（毫秒），默认60秒，这个时间间隔内没有接收到心跳消息就会发送超时事件
        configuration.setPingTimeout(pingTimeout);
        // Ping消息间隔（毫秒），默认25秒。客户端向服务器发送一条心跳消息间隔
        configuration.setPingInterval(pingInterval);

        SocketIOServer socketIOServer = new SocketIOServer(configuration);
        //添加事件监听器
        socketIOServer.addListeners(socketIOHandler);
        //启动SocketIOServer
        socketIOServer.start();
        logger.info("进程信息==>socketIO进程已启动");
        logger.info("socketIO端口==>{}",port);
    }
}
