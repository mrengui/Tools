package cn.xisoil.log.data;


import cn.xisoil.curd.model.enums.ObjectColumnType;
import cn.xisoil.curd.model.interfaces.CurdModel;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.*;
import java.util.Date;

@Data
@Entity(name = "LoginLog")
@Table(name = "loginLog")
@EntityListeners(AuditingEntityListener.class)
@CurdModelObject(value = "登录日志",edit = false,add = false)
public class YueLoginLogData {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    @CurdModel(value = "登录账号",search = true)
    private String account;

    @CurdModel(value = "登录时间",type = ObjectColumnType.TIME,comparable = true)
    @CreatedDate
    private Date createTime;

    @CurdModel(value = "状态码")
    private Integer code;

    @CurdModel(value = "是否成功",type = ObjectColumnType.BOOLEAN)
    private Boolean isSuccess;

    @CurdModel(value = "登录IP")
    private String ip;

    @CurdModel(value = "返回信息",type = ObjectColumnType.TEXTAREA)
    private String message;


}
