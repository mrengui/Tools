package cn.xisoil.auth.service.login;


import cn.xisoil.auth.data.user.YueLoginUser;
import cn.xisoil.auth.dto.login.EditUserRequest;
import cn.xisoil.auth.dto.login.LoginDto;
import cn.xisoil.auth.dto.login.LoginRequest;
import cn.xisoil.common.result.YueResult;
import org.springframework.web.bind.annotation.RequestBody;

import jakarta.validation.Valid;

public interface LoginService {

    YueResult<LoginDto>  userLogin(LoginRequest request);

    YueResult<YueLoginUser> getUserInfo();

    YueResult<String> edit(EditUserRequest user);

    YueResult<String> register(LoginRequest user);

}
