package cn.xisoil.webmaster.dto;

import lombok.Data;

import jakarta.validation.constraints.NotBlank;

@Data
public class YueSeoUrlRequest {

    @NotBlank(message = "地址不能为空")
    private String urls;

}
