package cn.xisoil.analysis.dao;

import cn.xisoil.analysis.data.YueAnalysis;
import cn.xisoil.analysis.data.YueSectionAnalysis;
import cn.xisoil.curd.dao.YueRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface YueAnalysisRepository extends YueRepository<YueAnalysis,String> {

    @Query(value = "select analysis from YueAnalysis analysis where concat(function('date',analysis.createTime))  = :date")
    List<YueAnalysis>findAllByToday(String date);

}
