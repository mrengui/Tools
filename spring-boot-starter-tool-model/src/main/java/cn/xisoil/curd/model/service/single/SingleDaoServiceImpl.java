package cn.xisoil.curd.model.service.single;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.curd.dao.YueRepository;
import cn.xisoil.curd.model.service.model.ModelService;
import cn.xisoil.utils.GenericObject;
import cn.xisoil.utils.YueContextUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class SingleDaoServiceImpl<T,R extends YueRepository<T,String>> extends GenericObject<T> {

    @Autowired
    private R repository;

    @Autowired
    private YueContextUtils yueContextUtils;

    @Autowired
    private ModelService modelService;

    public YueResult<String> edit(T request) {
        T t=repository.findTopByIdNotNull().orElse(object());
        BeanUtils.copyProperties(request,t);
        repository.save(t);
        return YueResult.<String>builder().success().message("修改成功").build();
    }

    public YueResult<List<T>> list(){
        List<T>list=repository.findAll();
        return YueResult.<List<T>>builder().success().message("获取成功").data(list).build();
    }

    public YueResult<T> get() {
        T t=repository.findTopByIdNotNull().orElse(object());
        return YueResult.<T>builder().success().data(t).message("获取成功").build();
    }





}
