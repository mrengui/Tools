package cn.xisoil.file.controller;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.curd.model.controller.SingleModelController;
import cn.xisoil.file.dao.FileConfigRepository;
import cn.xisoil.file.data.UPLOADTYPE;
import cn.xisoil.file.data.YueFileConfig;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/manage/file/config")
public class FileConfigController extends SingleModelController<YueFileConfig, FileConfigRepository>{


    @GetMapping("/type")
    public YueResult<List<Map<String,String>>>type(){
        List<Map<String,String>>list=new ArrayList<>();
        list.add(Map.of("id", UPLOADTYPE.LOCAL.getType(),"title","本地存储"));
        list.add(Map.of("id", UPLOADTYPE.ALYOSS.getType(),"title","阿里云OSS"));
        list.add(Map.of("id", UPLOADTYPE.HWOBS.getType(),"title","华为OBS"));
        list.add(Map.of("id", UPLOADTYPE.MINIO.getType(),"title","Minio云存储"));
        return YueResult.<List<Map<String, String>>>builder().success().data(list).build();
    }


    @Override
    public String getUrl() {
        return "/file/config";
    }

    @Override
    public String getName() {
        return "上传配置";
    }

    @Override
    public String getParent() {
        return "文件管理";
    }
}
