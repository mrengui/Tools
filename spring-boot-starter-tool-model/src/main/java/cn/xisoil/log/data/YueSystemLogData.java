package cn.xisoil.log.data;


import cn.xisoil.curd.model.enums.ObjectColumnType;
import cn.xisoil.curd.model.interfaces.CurdModel;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.*;
import java.util.Date;

@Data
@Entity(name = "YueSystemLogData")
@Table(name = "systemLogData")
@EntityListeners(AuditingEntityListener.class)
@CurdModelObject(value = "系统日志",edit = false,add = false)
public class YueSystemLogData {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    @CurdModel(value = "操作账号",search = true)
    private String account;

    @CurdModel(value = "操作内容",search = true)
    private String content;

    @CurdModel(value = "登录时间",type = ObjectColumnType.TIME,comparable = true)
    @CreatedDate
    private Date createTime;

    @CurdModel(value = "操作IP",search = true)
    private String ip;


}
