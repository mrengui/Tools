package cn.xisoil.curd.model.service.list;

import cn.xisoil.utils.GenericObject;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.common.to.ListStringRequest;
import cn.xisoil.common.exception.NormalException;
import cn.xisoil.common.to.SearchPageRequest;
import cn.xisoil.curd.model.data.ModelColumn;
import cn.xisoil.curd.model.service.model.ModelService;
import cn.xisoil.utils.YueContextUtils;
import cn.xisoil.curd.dao.YueRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import jakarta.persistence.criteria.Predicate;
import jakarta.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

public class CurdDaoServiceImpl<T,R extends YueRepository<T,String>> extends GenericObject<T> {

    @Autowired
    private R repository;


    @Autowired
    private YueContextUtils yueContextUtils;

    @Autowired
    private ModelService modelService;



    @Transactional
    public YueResult<String> delete(ListStringRequest request) {
        repository.deleteAllByIdIn(request.getIds());
        return YueResult.<String>builder().message("删除成功").success().build();
    }

    public YueResult<String> edit(T request) {
        T t=repository.findById(JSONObject.parseObject(JSON.toJSONString(request)).getString("id")).orElseThrow(()->new NormalException("数据不存在"));
        BeanUtils.copyProperties(request,t);
        repository.save(t);
        return YueResult.<String>builder().success().message("修改成功").build();
    }

    public YueResult<String> add(T request) {
        T t=object();
        BeanUtils.copyProperties(request,t);
        repository.save(t);
        return YueResult.<String>builder().success().message("添加成功").build();
    }


    public YueResult<Page<T>> page(SearchPageRequest searchPageRequest){
        Pageable pageable = PageRequest.of(searchPageRequest.getPageNum(), searchPageRequest.getPageSize());
        Specification<T> sectionSpecification = ((root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            List<String>searchColumns= modelService.getSearchColumn(this.getGenericClass());

            //字符类型搜索
            if (StringUtils.isNotBlank(searchPageRequest.getKeyword()) && !searchColumns.isEmpty()) {
                List<Predicate> searchPredicates = new ArrayList<>();
                searchColumns.forEach(s -> {
                    searchPredicates.add(
                            criteriaBuilder.or(
                                    criteriaBuilder.like(root.get(s), "%" + searchPageRequest.getKeyword() + "%")
                            )
                    );
                });
                predicates.add(criteriaBuilder.or(searchPredicates.toArray(new Predicate[0])));
            }


            //下拉列表类型搜索
            List<ModelColumn>searchListColumns= modelService.getListSearchColumn(this.getGenericClass());
            searchListColumns.forEach(searchListColumn->{
                    if (StringUtils.isNotBlank(searchPageRequest.getString(searchListColumn.getColumn())))
                    {
                        predicates.add(
                                criteriaBuilder.equal(
                                        root.get(searchListColumn.getColumn()).as(String.class),
                                        searchPageRequest.get(searchListColumn.getColumn())));
                    }
            });

            //排序
            if (searchPageRequest.getOrderBy() != null && StringUtils.isNotBlank(searchPageRequest.getOrderBy().getColumn())) {
                criteriaQuery.orderBy(
                        searchPageRequest.getOrderBy().getSortType().equals(Sort.Direction.DESC) ?
                                criteriaBuilder.desc(root.get(searchPageRequest.getOrderBy().getColumn())) : criteriaBuilder.asc(root.get(searchPageRequest.getOrderBy().getColumn()))
                );
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        });
        Page<T> page = repository.findAll(sectionSpecification, pageable);
        return YueResult.<Page<T>>builder().message("获取成功").success().data(page).build();
    }

    public YueResult<List<T>> list(){
        List<T>list=repository.findAll();
        return YueResult.<List<T>>builder().success().message("获取成功").data(list).build();
    }

    public YueResult<List<T>> search(String keyword,String id){
        Specification<T> sectionSpecification = ((root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            List<String>searchColumns= modelService.getSearchColumn(this.getGenericClass());
            //字符类型搜索
            if (StringUtils.isNotBlank(keyword) && !searchColumns.isEmpty()) {
                List<Predicate> searchPredicates = new ArrayList<>();
                searchColumns.forEach(s -> {
                    searchPredicates.add(
                            criteriaBuilder.or(
                                    criteriaBuilder.like(root.get(s), "%" + keyword + "%")
                            )
                    );
                });
                predicates.add(criteriaBuilder.or(searchPredicates.toArray(new Predicate[0])));
            }
            if (StringUtils.isNotBlank(id)){
                predicates.add(criteriaBuilder.equal(root.get("id"),id));
            }
            return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
        });
        List<T> list = repository.findAll(sectionSpecification);
        return YueResult.<List<T>>builder().message("获取成功").success().data(list).build();
    }

    public YueResult<T> get(String id) {
        T t=repository.findById(id).orElseThrow(()->new NormalException("数据不存在"));
        return YueResult.<T>builder().success().data(t).message("获取成功").build();
    }





}
