package cn.xisoil.common.to;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 批量操作
 */
@Data
public class ListStringRequest {

    private String id;

    private List<String> ids;

    public List<String> getIds() {
        ids=ids==null?new ArrayList<>():ids;
        if (id!=null){
            ids.add(id);
        }
        return ids;
    }
}
