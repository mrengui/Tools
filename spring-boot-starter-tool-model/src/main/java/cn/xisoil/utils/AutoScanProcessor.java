package cn.xisoil.utils;

import com.google.auto.service.AutoService;
import com.squareup.javapoet.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@AutoService(Processor.class)
public class AutoScanProcessor extends AbstractProcessor {


    private Elements elementTool;
    private Messager messager;
    private Filer filer;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        elementTool = processingEnv.getElementUtils();
        messager = processingEnv.getMessager();
        filer = processingEnv.getFiler();
        super.init(processingEnv);
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> set = new HashSet<>();
        set.add(EnableModel.class.getCanonicalName());
        return set;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        if (!annotations.isEmpty()) {
            //获取Bind注解类型的元素，这里是类类型TypeElement
            Set<? extends Element> bindElement = roundEnv.getElementsAnnotatedWith(EnableModel.class);
            try {
                generateCode(bindElement);
            } catch (IOException | ClassNotFoundException e) {
                messager.printMessage(Diagnostic.Kind.ERROR, e.toString());
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }


    /**
     * @param elements
     */
    private void generateCode(Set<? extends Element> elements) throws IOException, ClassNotFoundException {

        for (Element element : elements) {

            SpringBootApplication springBootApplication =element.getAnnotation(SpringBootApplication.class);

            String[] scanBasePackages=springBootApplication.scanBasePackages();

            StringBuffer packages=new StringBuffer("{\"cn.xisoil.*\",\"");

            for (int i = 0; i < scanBasePackages.length; i++) {
                packages.append(scanBasePackages[i]).append("\"");
                if (i!=scanBasePackages.length-1){
                    packages.append(",\"");
                }
            }
            packages.append("}");

            AnnotationSpec enableJpaRepositories = AnnotationSpec
                    .builder(EnableJpaRepositories.class)
                    .addMember("value", "$L", packages).build();

            AnnotationSpec entityScan = AnnotationSpec
                    .builder(EntityScan.class)
                    .addMember("value", "$L", packages).build();

            AnnotationSpec configuration = AnnotationSpec
                    .builder(Configuration.class).build();

            AnnotationSpec componentScan = AnnotationSpec
                    .builder(ComponentScan.class)
                    .addMember("value", "$L", "{\"cn.xisoil.*\"}")
                    .build();
            //生成的类
            TypeSpec type = TypeSpec
                    .classBuilder("ModelConfig")
                    .addModifiers(Modifier.PUBLIC)
                    .addAnnotation(enableJpaRepositories)
                    .addAnnotation(entityScan)
                    .addAnnotation(componentScan)
                    .addAnnotation(configuration)
                    .build();
            //创建javaFile文件对象
            JavaFile.builder("cn.xisoil.utils", type)
                    .build().writeTo(filer);
        }
    }


}
