package cn.xisoil.system.model.data;

import cn.xisoil.curd.model.interfaces.CurdModel;
import lombok.Data;

@Data
public class YueCpuInfo {

    @CurdModel("CPU核心数")
    private Integer cpuCount;

    @CurdModel("系统使用率")
    private String utilization;

    @CurdModel("用户使用率")
    private String userUtilization;

    @CurdModel("空闲率")
    private String idleness;
}
