package cn.xisoil.task.utils;

import cn.xisoil.common.exception.NormalException;
import cn.xisoil.utils.YueContextUtils;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

@Component
@EnableScheduling
public class TaskTools  {

    @Autowired
    private YueContextUtils yueContextUtils;
    @Autowired
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;

    @Autowired
    @Lazy
    private Map<String,TaskService>taskServices=new HashMap<>();

    private Map<String,TaskServiceConstructor> taskServiceMap=new HashMap<>();
    ScheduledExecutorService executorService = Executors.newScheduledThreadPool(100);

    @PostConstruct
    private void init() {
        taskServices.forEach((k,v)->{
            setSchedule(v);
        });
    }

    public void setSchedule(TaskService taskService) {
        ScheduledFuture<?> schedule = threadPoolTaskScheduler.schedule(taskService, new CronTrigger(taskService.getCron()));
        taskServiceMap.put(taskService.getTaskName(),new TaskServiceConstructor(taskService,schedule));
    }

    public void close(String taskId){
        TaskServiceConstructor taskServiceConstructor=  taskServiceMap.get(taskId);
        taskServiceConstructor.close();
    }

    public void refreshCron(String taskId,String cron){
        close(taskId);
        TaskService taskService=taskServiceMap.get(taskId).getTaskService();
        taskService.setCron(cron);
        ScheduledFuture<?> schedule = threadPoolTaskScheduler.schedule(taskService, new CronTrigger(cron));
        taskServiceMap.put(taskService.getTaskName(),new TaskServiceConstructor(taskService,schedule));
    }

    public void run(String taskId){
        taskServiceMap.get(taskId).run();
    }

    public void start(String taskId){
        TaskServiceConstructor taskServiceConstructor=taskServiceMap.get(taskId);
        if (!taskServiceConstructor.getCancelled()){
            throw new NormalException("请勿重复启动定时任务");
        }
        ScheduledFuture<?> schedule = threadPoolTaskScheduler.schedule(taskServiceConstructor.getTaskService(), new CronTrigger(taskServiceConstructor.getTaskService().getCron()));
        taskServiceMap.put(taskId,new TaskServiceConstructor(taskServiceConstructor.getTaskService(),schedule));
    }

    public void runAll(){
        taskServiceMap.values().forEach(TaskServiceConstructor::run);
    }

    public List<TaskServiceConstructor>tasks(){
        return taskServiceMap.values().stream().toList();
    }

}
