package cn.xisoil.auth.dto.login;

import lombok.Data;

import jakarta.validation.constraints.NotBlank;

/**
 * 用户登录信息
 */
@Data
public class LoginRequest {

    @NotBlank(message = "账号不能为空")
    private String account;

    @NotBlank(message = "密码不能为空")
    private String password;
}
