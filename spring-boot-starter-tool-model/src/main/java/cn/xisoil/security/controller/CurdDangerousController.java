package cn.xisoil.security.controller;

import cn.xisoil.curd.model.controller.ModelCurdControllerMapping;
import cn.xisoil.datacheck.PermissionCheckAutomation;
import cn.xisoil.security.dao.YueDangerousRepository;
import cn.xisoil.security.data.YueDangerous;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/manage/dangerous")
@RestController
public class CurdDangerousController extends ModelCurdControllerMapping<YueDangerous, YueDangerousRepository> {

    @Override
    public String getUrl() {
        return "/dangerous";
    }

    @Override
    public String getName() {
        return "异常管理";
    }

    @Override
    public String getParent() {
        return "安全中心";
    }
}
