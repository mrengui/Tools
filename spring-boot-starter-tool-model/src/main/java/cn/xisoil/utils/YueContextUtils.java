package cn.xisoil.utils;


import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 上下文操作bean类
 */
@Component
public class YueContextUtils implements BeanFactoryAware, ApplicationContextAware {

    private static DefaultListableBeanFactory listableBeanFactory;
    protected ApplicationContext applicationContext;

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        listableBeanFactory = (DefaultListableBeanFactory) beanFactory;
        listableBeanFactory.setAllowBeanDefinitionOverriding(true);
    }

    /**
     * 创建单例的bean
     */
    public void registerSingleton(String beanName, Object object) {
        listableBeanFactory.registerSingleton(beanName, object);
    }

    /**
     * 销毁单例的bean
     */
    public void destroySingleton(String beanName) {
        listableBeanFactory.destroySingleton(beanName);
    }



    /**
     * 获取bean
     * @param beanName
     * @return
     */
    public Object getBean(String beanName) {
        if (containsBean(beanName)) {
            return listableBeanFactory.getBean(beanName);
        } else {
            return null;
        }
    }


    /**
     * 获取bean 名称列表
     * @param clazz
     * @return
     */
    public  String[] getBeanNamesForType(Class<?> clazz) {
            return listableBeanFactory.getBeanNamesForType(clazz);
    }

    /**
     * 获取bean列表
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> List<T> getBeansForType(Class<T> clazz) {
        String[] beanNames= listableBeanFactory.getBeanNamesForType(clazz);
        List<T>tList=new LinkedList<>();
        for (String beanName : beanNames) {
            tList.add(listableBeanFactory.getBean(beanName, clazz));
        }
            return tList;
    }

    /**
     * 获取bean列表
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> T getBeanForType(Class<T> clazz) {
       return listableBeanFactory.getBeanProvider(clazz).getIfAvailable();
    }

    public<T extends Annotation> Map<String,Object>getBeansWithAnnotation(Class<T> tClass){
        return applicationContext.getBeansWithAnnotation(tClass);
    }

    /**
     * 判断上下文是否有这个bean
     */
    public boolean containsBean(String beanName) {
        return listableBeanFactory.containsBean(beanName);
    }



    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}

