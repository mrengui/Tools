package cn.xisoil.analysis.aop;

import cn.xisoil.analysis.dao.*;
import cn.xisoil.analysis.data.*;
import cn.xisoil.system.model.enums.EQUIPMENT;
import cn.xisoil.task.utils.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("数据分析")
public class AnalysisTaskUtils extends TaskService {

    @Autowired
    private YueAnalysisRepository yueAnalysisRepository;
    @Autowired
    private YueBrowserAnalysisRepository yueBrowserAnalysisRepository;
    @Autowired
    private YueSectionAnalysisRepository yueSectionAnalysisRepository;
    @Autowired
    private YueEquipmentAnalysisRepository yueEquipmentAnalysisRepository;
    @Autowired
    private YueDateAnalysisRepository yueDateAnalysisRepository;

    public AnalysisTaskUtils() {
        super("0 0 1 * * ?");
    }

    @Override
    public String getTaskName() {
        return "数据分析";
    }


    @Override
    public void run() {

        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        String date=simpleDateFormat.format(getYesterday());
        List<YueAnalysis>yueAnalyses=yueAnalysisRepository.findAllByToday(date);

        //设备
        Map<AnalysisEquipment,List<YueAnalysis>>analysisEquipments=yueAnalyses.stream().collect(Collectors.groupingBy(YueAnalysis::getEquipment));
        analysisEquipments.forEach(((analysisEquipment, analyses) -> {
            YueEquipmentAnalysis yueEquipmentAnalysis=yueEquipmentAnalysisRepository.findTopByDateAndEquipment(date,analysisEquipment)
                    .orElse(new YueEquipmentAnalysis());
            yueEquipmentAnalysis.setEquipment(analysisEquipment);
            yueEquipmentAnalysis.setDate(date);
            yueEquipmentAnalysis.setPvCount(yueEquipmentAnalysis.getPvCount()+analyses.size());
            yueEquipmentAnalysis.setUvCount(yueEquipmentAnalysis.getUvCount()+analyses.stream().collect(Collectors.groupingBy(YueAnalysis::getIp)).size());
            yueEquipmentAnalysisRepository.save(yueEquipmentAnalysis);
        }));

        //浏览器
        Map<AnalysisBrowser,List<YueAnalysis>>analysisBrowsers=yueAnalyses.stream().collect(Collectors.groupingBy(YueAnalysis::getBrowser));
        analysisBrowsers.forEach(((analysisBrowser, analyses) -> {
            YueBrowserAnalysis yueBrowserAnalysis=yueBrowserAnalysisRepository.findTopByDateAndBrowser(date,analysisBrowser)
                    .orElse(new YueBrowserAnalysis());
            yueBrowserAnalysis.setBrowser(analysisBrowser);
            yueBrowserAnalysis.setDate(date);
            yueBrowserAnalysis.setPvCount(yueBrowserAnalysis.getPvCount()+analyses.size());
            yueBrowserAnalysis.setUvCount(yueBrowserAnalysis.getUvCount()+analyses.stream().collect(Collectors.groupingBy(YueAnalysis::getIp)).size());
            yueBrowserAnalysisRepository.save(yueBrowserAnalysis);
        }));

        //栏目
        Map<String,List<YueAnalysis>>analysisSections=yueAnalyses.stream().collect(Collectors.groupingBy(YueAnalysis::getSectionName));
        analysisSections.forEach(((analysisSection, analyses) -> {
            YueSectionAnalysis yueSectionAnalysis=yueSectionAnalysisRepository.findTopByDateAndSectionName(date,analysisSection)
                    .orElse(new YueSectionAnalysis());
            yueSectionAnalysis.setSectionName(analysisSection);
            yueSectionAnalysis.setDate(date);
            yueSectionAnalysis.setPvCount(yueSectionAnalysis.getPvCount()+analyses.size());
            yueSectionAnalysis.setUvCount(yueSectionAnalysis.getUvCount()+analyses.stream().collect(Collectors.groupingBy(YueAnalysis::getIp)).size());
            yueSectionAnalysisRepository.save(yueSectionAnalysis);
        }));

        //时间PV/UV
        YueDateAnalysis yueDateAnalysis=yueDateAnalysisRepository.findTopByDate(date).orElse(new YueDateAnalysis());
        yueDateAnalysis.setDate(date);
        yueDateAnalysis.setPvCount(yueAnalyses.size());
        yueDateAnalysis.setUvCount(yueAnalyses.stream().collect(Collectors.groupingBy(YueAnalysis::getIp)).size());
        yueDateAnalysisRepository.save(yueDateAnalysis);

        yueAnalysisRepository.deleteAll(yueAnalyses);

    }

    private Date getYesterday(){
        Calendar cal   =   Calendar.getInstance();
        cal.add(Calendar.DATE,   -1);
        return  cal.getTime();
    }

}
