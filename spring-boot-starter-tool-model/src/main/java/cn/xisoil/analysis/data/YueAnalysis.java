package cn.xisoil.analysis.data;

import cn.xisoil.curd.model.interfaces.CurdModelObject;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;

@Data
@Entity(name = "YueAnalysis")
@Table(name = "yue_analysis")
@EntityListeners(AuditingEntityListener.class)
@CurdModelObject(value = "访问日志")
public class YueAnalysis {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    private String sectionName;

    @Enumerated(value = EnumType.STRING)
    private AnalysisBrowser browser;

    @Enumerated(value = EnumType.STRING)
    private AnalysisEquipment equipment;

    @CreatedDate
    private Date createTime;

    private String ip;

}
