package cn.xisoil.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "yue.auto", ignoreUnknownFields = true, ignoreInvalidFields = true)
public class AutoMationProperties {

    private Boolean check = false;

    private SystemUser user = new SystemUser();

    @Data
    public class SystemUser {

        private String username = "admin";

        private String password = "123456";


    }


}
