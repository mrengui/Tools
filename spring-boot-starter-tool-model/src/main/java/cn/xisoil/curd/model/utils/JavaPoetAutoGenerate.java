package cn.xisoil.curd.model.utils;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.common.to.ListStringRequest;
import cn.xisoil.common.to.SearchPageRequest;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import cn.xisoil.log.data.LogEnum;
import cn.xisoil.log.interfaces.Log;
import jakarta.validation.Valid;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.javapoet.*;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.*;

/**
 * @Description JavaPoet 方法
 * @Author Vien
 * @CreateTime 2023-02-2023/2/24 00:02:06
 **/


public class JavaPoetAutoGenerate {

    private CurdModelObject curdModelObject;

    public JavaPoetAutoGenerate(CurdModelObject curdModelObject){
        this.curdModelObject=curdModelObject;
    }
    



    @SafeVarargs
    public final ParameterSpec parameterSpec(Type type, String value, Class<? extends Annotation>... annotations) {
        List<AnnotationSpec>annotationSpecs=new ArrayList<>();
        for (Class<? extends Annotation> annotation : annotations) {
            annotationSpecs.add(annotationSpec(annotation));
        }
        return ParameterSpec
                .builder(type, value)
                .addAnnotations(new ArrayList<>(annotationSpecs))
                .build();
    }

    @SafeVarargs
    public final ParameterSpec parameterSpec(TypeName type, String value, Class<? extends Annotation>... annotations) {
        List<AnnotationSpec>annotationSpecs=new ArrayList<>();
        for (Class<? extends Annotation> annotation : annotations) {
            annotationSpecs.add(annotationSpec(annotation));
        }
        return ParameterSpec
                .builder(type, value)
                .addAnnotations(new ArrayList<>(annotationSpecs))
                .build();
    }



    public  ParameterSpec parameterSpec(Type type, String value,AnnotationSpec annotations) {
        return ParameterSpec
                .builder(type, value)
                .addAnnotation(annotations)
                .build();
    }


    public   AnnotationSpec annotationSpec(Class<? extends Annotation> annotation){
        return AnnotationSpec
                .builder(annotation)
                .build();
    }

    public   AnnotationSpec annotationSpec(Class<? extends Annotation> a, String name){
        return  AnnotationSpec
                .builder(a)
                .addMember("value", "$S", name).build();
    }



    public  AnnotationSpec postMappingAnnotationSpec(String value) {
        return AnnotationSpec
                .builder(PostMapping.class)
                .addMember("value", "$S", value)
                .build();

    }
    public  AnnotationSpec putMappingAnnotationSpec(String value) {
        return AnnotationSpec
                .builder(PutMapping.class)
                .addMember("value", "$S", value)
                .build();

    }

    public  AnnotationSpec getMappingAnnotationSpec(String value) {
        return AnnotationSpec
                .builder(GetMapping.class)
                .addMember("value", "$S", value)
                .build();

    }


    public  MethodSpec delMethSpec() {

        return MethodSpec.methodBuilder("delete")
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(postMappingAnnotationSpec("/delete"))
                .addAnnotation(annotationSpec(Log.class, LogEnum.DEL.toString(curdModelObject.value())))
                .addParameter(parameterSpec(ListStringRequest.class,"stringRequest", Valid.class, RequestBody.class))
                .addParameter(parameterSpec(BindingResult.class,"result"))
                .addStatement("return super.delete($L,$L)","stringRequest","result")
                .returns(YueResult.class)
                .build();

    }

    public  MethodSpec pageMethSpec() {

        return MethodSpec.methodBuilder("page")
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(postMappingAnnotationSpec("/page"))
                .addAnnotation(annotationSpec(Log.class, LogEnum.PAGE.toString(curdModelObject.value())))
                .addParameter(parameterSpec(SearchPageRequest.class,"searchPageRequest", Valid.class, RequestBody.class))
                .addParameter(parameterSpec(BindingResult.class,"result"))
                .addStatement("return super.page($L)","searchPageRequest")
                .returns(YueResult.class)
                .build();

    }

    public  MethodSpec editMethSpec(TypeName type) {
        return MethodSpec.methodBuilder("edit")
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(putMappingAnnotationSpec(""))
                .addAnnotation(annotationSpec(Log.class, LogEnum.EDIT.toString(curdModelObject.value())))
                .addParameter(parameterSpec(type,"request", Valid.class, RequestBody.class))
                .addParameter(parameterSpec(BindingResult.class,"result"))
                .addStatement("return super.edit($L)","request")
                .returns(YueResult.class)
                .build();
    }

    public  MethodSpec getMethSpec() {
        return MethodSpec.methodBuilder("get")
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(annotationSpec(Log.class, LogEnum.GET.toString(curdModelObject.value())))
                .addAnnotation(getMappingAnnotationSpec(""))
                .addStatement("return super.get()")
                .returns(YueResult.class)
                .build();
    }


    public  MethodSpec getMethSpec(Type type) {
        return MethodSpec.methodBuilder("get")
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(getMappingAnnotationSpec("/{id}"))
                .addAnnotation(annotationSpec(Log.class, LogEnum.GET.toString(curdModelObject.value())))
                .addParameter(parameterSpec(type,"id", PathVariable.class))
                .addStatement("return super.get($L)","id")
                .returns(YueResult.class)
                .build();
    }



    public  MethodSpec addMethSpec(TypeName type) {
        return MethodSpec.methodBuilder("add")
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(postMappingAnnotationSpec(""))
                .addAnnotation(annotationSpec(Log.class, LogEnum.ADD.toString(curdModelObject.value())))
                .addParameter(parameterSpec(type,"request", Valid.class, RequestBody.class))
                .addParameter(parameterSpec(BindingResult.class,"result"))
                .addStatement("return super.add($L)","request")
                .returns(YueResult.class)
                .build();
    }

    public   AnnotationSpec requestMappingAnnotationSpec(String route){
       return  AnnotationSpec
                .builder(RequestMapping.class)
                .addMember("value", "$S", route).build();
    }

    public   AnnotationSpec restControllerAnnotationSpec(String name){
       return  AnnotationSpec
                .builder(RestController.class)
                .addMember("value", "$S", name).build();
    }

}
