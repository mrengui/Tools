package cn.xisoil.file.service.strategy.impl;


import cn.xisoil.common.exception.NormalException;
import cn.xisoil.common.result.ResultFile;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.data.FileInfo;
import cn.xisoil.data.UploadConfigClient;
import cn.xisoil.file.dao.FileConfigRepository;
import cn.xisoil.file.data.UPLOADTYPE;
import cn.xisoil.file.data.YueFile;
import cn.xisoil.file.data.YueFileConfig;
import cn.xisoil.file.service.manage.YueFileManageService;
import cn.xisoil.file.service.strategy.YueFileStrategy;
import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service(value = "HWOBS")
public class ObsFileStrategy implements YueFileStrategy {

    @Autowired
    private YueFileManageService yueFileManageService;
    @Autowired
    private FileConfigRepository fileConfigRepository;


    @Override
    public ResultFile<String> upload(HttpServletRequest request) {
        YueFileConfig config= fileConfigRepository.findTopByIdNotNull().orElse(new YueFileConfig());

        try {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile multipartFile=multipartRequest.getFile("file");
            String fileName= multipartFile.getOriginalFilename();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String dateNowStr = sdf.format(new Date());
            String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
            String objectName=dateNowStr+"-"+(int)(Math.random() * 1000000)+"."+fileExt;//随机文件名
            InputStream inputStream =multipartFile.getInputStream();
            String url= UploadConfigClient
                    .of(config.getObsEndpoint(), config.getObsBucketName(), config.getObsAk(), config.getObsSk())
                    .obs().upload(FileInfo.builder().name(objectName).inputStream(inputStream).build());
            YueFile yueFile=new YueFile();
            yueFile.setFilename(fileName);
            yueFile.setObjectName(objectName);
            yueFile.setUploadtype(UPLOADTYPE.HWOBS);
            yueFile.setUrl(url);
            yueFileManageService.save(yueFile);
            return ResultFile.<String>builder()
                    .data(url)
                    .param(request.getHeader("key"))
                    .success().build();
        } catch (OSSException oe) {
        } catch (ClientException ce) {
            System.out.println("Error Message:" + ce.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new NormalException("未知错误");
    }

    @Override
    public YueResult<String> delete(YueFile file) {
        YueFileConfig config= fileConfigRepository.findTopByIdNotNull().orElse(new YueFileConfig());
        // 填写文件完整路径。文件完整路径中不能包含Bucket名称。
        String objectName = file.getObjectName();

        // 创建OSSClient实例。
        UploadConfigClient
                .of(config.getObsEndpoint(), config.getObsBucketName(), config.getObsAk(), config.getObsSk())
                .obs().delete(objectName);

        return YueResult.<String>builder().success().message("删除成功").build();
    }
}
