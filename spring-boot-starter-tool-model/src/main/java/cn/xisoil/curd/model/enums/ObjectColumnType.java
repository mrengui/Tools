package cn.xisoil.curd.model.enums;

public enum ObjectColumnType {
    STRING,
    NUMBER,
    TEXTAREA,
    URL,
    @Deprecated(forRemoval = true,since="2.0.1-RELEASE")
    RICHTEXT,
    EDITOR,
    LIGHTWEIGHTEDITOR,
    LIST,
    DATE,
    TIME,
    FILE,
    IMAGE,
    BOOLEAN,
    CHECKBOX,
    RADIO,
    IMAGELIST,
    LISTMULTISELECT,
    VIDEO,
    COLOR,
    SEARCHLIST,
    TABLE
}
