package cn.xisoil.security.data;


import cn.xisoil.curd.model.enums.ObjectColumnType;
import cn.xisoil.curd.model.interfaces.CurdModel;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.*;
import java.io.Serializable;

@Data
@Entity(name = "YueSecurityBasic")
@Table(name = "yue_security_basic")
@EntityListeners(AuditingEntityListener.class)
@CurdModelObject(value = "安全配置")
public class YueSecurityBasic implements Serializable {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    @CurdModel(value = "开启防御",type = ObjectColumnType.BOOLEAN)
    private Boolean isSecurity;

    @CurdModel(value = "防盗链",type = ObjectColumnType.BOOLEAN)
    private Boolean isHotlink;

    @CurdModel(value = "拦截列表",type = ObjectColumnType.TEXTAREA)
    private String rollList;

    public String getRollList() {
        return rollList==null?"":rollList;
    }

    @CurdModel(value = "邮件提醒",type = ObjectColumnType.BOOLEAN)
    private Boolean emailRemind=false;

    @CurdModel(value = "通知邮箱",type = ObjectColumnType.STRING)
    private String email;

}
