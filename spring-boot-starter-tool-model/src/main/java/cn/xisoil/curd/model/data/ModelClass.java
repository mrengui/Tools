package cn.xisoil.curd.model.data;

import cn.xisoil.curd.model.enums.ObjectType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ModelClass {

    private Boolean delete=true;

    private Boolean edit=true;

    private Boolean add=true;

    private String name="";

    private ObjectType type ;

    private List<ModelColumn>columns=new LinkedList<>();

    private Map<String,List<ModelColumn>>groups=new ConcurrentHashMap<>();

}
