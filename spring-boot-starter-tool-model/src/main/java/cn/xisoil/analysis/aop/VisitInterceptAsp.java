package cn.xisoil.analysis.aop;

import cn.xisoil.analysis.interfaces.VisitIntercept;
import cn.xisoil.analysis.sercvice.YueAnalysisUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;

@Component
@Aspect
public class VisitInterceptAsp {

    @Autowired
    private YueAnalysisUtils yueAnalysisUtils;

    @Before(value = "@annotation(visitIntercept)")
    public void systemLogin(JoinPoint point, VisitIntercept visitIntercept){
        RequestContextHolder.setRequestAttributes(RequestContextHolder.getRequestAttributes(), true);
        yueAnalysisUtils.analysis(point,visitIntercept);
    }

}
