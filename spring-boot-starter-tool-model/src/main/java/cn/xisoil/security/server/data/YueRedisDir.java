package cn.xisoil.security.server.data;

import lombok.Data;


@Data
public class YueRedisDir {

    private String dirName;

    private String remark;

}
