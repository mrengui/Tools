package cn.xisoil.common.enums;

public enum HTTPCODE {

    SUCCESS(200,"请求成功"),
    BADREQUEST(400,"请求格式错误"),
    NOTFOUND(404,"资源丢失"),
    ERROR(500,"服务器错误"),
    UAAUTHORIZED(401,"客户端验证失败"),
    FORBIDDEN(403,"客户端没有权限访问");


    private final Integer code;
    private final String message;

    HTTPCODE(Integer code,String message){
        this.code=code;
        this.message=message;
    }
    public Integer getCode() {
        return code;
    }
    public String getMessage() {
        return message;
    }


    @Override
    public String toString() {
        return message;
    }
}
