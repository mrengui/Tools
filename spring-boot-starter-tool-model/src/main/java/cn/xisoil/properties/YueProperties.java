package cn.xisoil.properties;

import cn.hutool.core.date.DateUtil;
import cn.xisoil.system.model.enums.EQUIPMENT;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Component
@ConfigurationProperties(prefix = "yue", ignoreUnknownFields = true,ignoreInvalidFields = true)
public class YueProperties {

    @Value("${server.port}")
    private String port;

    private final Date startTime=new Date();

    private EQUIPMENT equipment=EQUIPMENT.linux;

    public void setEquipment(EQUIPMENT equipment) {
        this.equipment = equipment;
    }

    public EQUIPMENT getEquipment() {
        return equipment;
    }

    public String getPort() {
        return port;
    }

    public String getRunTime(){
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime after = LocalDateTime.ofInstant(startTime.toInstant(), ZoneId.systemDefault());
        return DateUtil.formatBetween(ChronoUnit.MILLIS.between(after,now));
    }
    public Date getStartTime(){
        return startTime;
    }
}
