//package cn.xisoil;
//
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
//import org.springframework.context.ConfigurableApplicationContext;
//import org.springframework.scheduling.annotation.EnableAsync;
//
//@SpringBootApplication(scanBasePackages={"cn.xisoil.*"}, exclude= DataSourceAutoConfiguration.class)
//@EnableAsync
//public class ThyWebApplication {
//
//    public static void main(String[] args) {
//        ConfigurableApplicationContext context= SpringApplication.run(ThyWebApplication.class, args);
//    }
//}
