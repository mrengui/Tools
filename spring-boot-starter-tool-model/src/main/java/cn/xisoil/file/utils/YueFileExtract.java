package cn.xisoil.file.utils;

import cn.xisoil.file.data.YueFileType;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class YueFileExtract {

    private final static Map<String, YueFileType> FILE_TYPE_MAP = new HashMap<String, YueFileType>();

    static {
        FILE_TYPE_MAP.put("bmp",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("jpg",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("png",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("tif",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("jpeg",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("xbm",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("pjp",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("ico",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("tiff",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("gif",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("webp",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("jfif",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("pjpeg",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("avif",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("svgz",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("svg",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("jxr",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("iff",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("ilbm",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("ppm",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("pcx",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("xcf",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("xpm",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("psd",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("sai",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("psp",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("ufo",YueFileType.IMAGE);
        FILE_TYPE_MAP.put("mng",YueFileType.IMAGE);

        FILE_TYPE_MAP.put("doc",YueFileType.DOC);
        FILE_TYPE_MAP.put("docx",YueFileType.DOC);
        FILE_TYPE_MAP.put("xls",YueFileType.DOC);
        FILE_TYPE_MAP.put("xlt",YueFileType.DOC);
        FILE_TYPE_MAP.put("xml",YueFileType.DOC);
        FILE_TYPE_MAP.put("rtf",YueFileType.DOC);
        FILE_TYPE_MAP.put("dot",YueFileType.DOC);
        FILE_TYPE_MAP.put("pps",YueFileType.DOC);
        FILE_TYPE_MAP.put("pot",YueFileType.DOC);
        FILE_TYPE_MAP.put("ppt",YueFileType.DOC);
        FILE_TYPE_MAP.put("msg",YueFileType.DOC);
        FILE_TYPE_MAP.put("eml",YueFileType.DOC);
        FILE_TYPE_MAP.put("mht",YueFileType.DOC);
        FILE_TYPE_MAP.put("pst",YueFileType.DOC);
        FILE_TYPE_MAP.put("ost",YueFileType.DOC);
        FILE_TYPE_MAP.put("txt",YueFileType.DOC);
        FILE_TYPE_MAP.put("pdf",YueFileType.DOC);
        FILE_TYPE_MAP.put("pptx",YueFileType.DOC);
        FILE_TYPE_MAP.put("xlsm",YueFileType.DOC);
        FILE_TYPE_MAP.put("xlsx",YueFileType.DOC);
        FILE_TYPE_MAP.put("html",YueFileType.DOC);
        FILE_TYPE_MAP.put("xhtml",YueFileType.DOC);

        FILE_TYPE_MAP.put("rar",YueFileType.RAR);
        FILE_TYPE_MAP.put("zip",YueFileType.RAR);
        FILE_TYPE_MAP.put("7z",YueFileType.RAR);
        FILE_TYPE_MAP.put("tar",YueFileType.RAR);
        FILE_TYPE_MAP.put("bz2",YueFileType.RAR);
        FILE_TYPE_MAP.put("tgz",YueFileType.RAR);
        FILE_TYPE_MAP.put("z",YueFileType.RAR);
        FILE_TYPE_MAP.put("gz",YueFileType.RAR);
        FILE_TYPE_MAP.put("wim",YueFileType.RAR);
        FILE_TYPE_MAP.put("gzip",YueFileType.RAR);


        FILE_TYPE_MAP.put("exe",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("dll",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("com",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("bat",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("cpl",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("acm",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("apk",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("drv",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("scr",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("sys",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("sh",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("c",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("py",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("java",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("go",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("php",YueFileType.PROGRAM);
        FILE_TYPE_MAP.put("lnk",YueFileType.PROGRAM);


        FILE_TYPE_MAP.put("mp4",YueFileType.VIDEO);
        FILE_TYPE_MAP.put("rmvb",YueFileType.VIDEO);
        FILE_TYPE_MAP.put("asf",YueFileType.VIDEO);
        FILE_TYPE_MAP.put("divx",YueFileType.VIDEO);
        FILE_TYPE_MAP.put("mpg",YueFileType.VIDEO);
        FILE_TYPE_MAP.put("mpeg",YueFileType.VIDEO);
        FILE_TYPE_MAP.put("wmv",YueFileType.VIDEO);
        FILE_TYPE_MAP.put("mkv",YueFileType.VIDEO);
        FILE_TYPE_MAP.put("vob",YueFileType.VIDEO);
        FILE_TYPE_MAP.put("3gp",YueFileType.VIDEO);
        FILE_TYPE_MAP.put("avi",YueFileType.VIDEO);
        FILE_TYPE_MAP.put("mov",YueFileType.VIDEO);
        FILE_TYPE_MAP.put("flv",YueFileType.VIDEO);
        FILE_TYPE_MAP.put("swf",YueFileType.VIDEO);
        FILE_TYPE_MAP.put("qsv",YueFileType.VIDEO);
        FILE_TYPE_MAP.put("kux",YueFileType.VIDEO);


        FILE_TYPE_MAP.put("mp3",YueFileType.AUDIO);
        FILE_TYPE_MAP.put("mp2",YueFileType.AUDIO);
        FILE_TYPE_MAP.put("mp1",YueFileType.AUDIO);
        FILE_TYPE_MAP.put("wav",YueFileType.AUDIO);
        FILE_TYPE_MAP.put("aif",YueFileType.AUDIO);
        FILE_TYPE_MAP.put("aiff",YueFileType.AUDIO);
        FILE_TYPE_MAP.put("au",YueFileType.AUDIO);
        FILE_TYPE_MAP.put("ra",YueFileType.AUDIO);
        FILE_TYPE_MAP.put("rm",YueFileType.AUDIO);
        FILE_TYPE_MAP.put("ram",YueFileType.AUDIO);
        FILE_TYPE_MAP.put("mid",YueFileType.AUDIO);
        FILE_TYPE_MAP.put("rmi",YueFileType.AUDIO);


    }

    public static YueFileType getType(String suffix){
        YueFileType yueFileType=FILE_TYPE_MAP.get(suffix.toLowerCase(Locale.ROOT));
        return yueFileType==null?YueFileType.OTHER:yueFileType;
    }




}
