package cn.xisoil.analysis.dao;

import cn.xisoil.analysis.data.AnalysisBrowser;
import cn.xisoil.analysis.data.YueBrowserAnalysis;
import cn.xisoil.analysis.data.YueSectionAnalysis;
import cn.xisoil.curd.dao.YueRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface YueBrowserAnalysisRepository extends YueRepository<YueBrowserAnalysis,String> {

    Optional<YueBrowserAnalysis>findTopByDateAndBrowser(String time, AnalysisBrowser browser);



    @Query(value = "select new map(browser.browser as name,sum(browser.uvCount) as value)  from YueBrowserAnalysis browser group by browser.browser order by sum(browser.uvCount) desc ")
    List<Map<String,Object>> findTop5SumByBrowser();



}
