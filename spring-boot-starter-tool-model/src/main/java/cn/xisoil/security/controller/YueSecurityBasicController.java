package cn.xisoil.security.controller;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.curd.model.controller.SingleModelController;
import cn.xisoil.security.dao.YueSecurityBasicRepository;
import cn.xisoil.security.data.YueSecurityBasic;
import cn.xisoil.security.service.YueSecurityBasicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/manage/security/basic")
public class YueSecurityBasicController extends SingleModelController<YueSecurityBasic, YueSecurityBasicRepository>    {

    @Override
    public String getUrl() {
        return "/security/basic";
    }

    @Override
    public String getName() {
        return "安全配置";
    }

    @Override
    public String getParent() {
        return "安全中心";
    }

    @Autowired
    private YueSecurityBasicService yueSecurityBasicService;

    @GetMapping("")
    @Primary
    public YueResult<YueSecurityBasic>get(){
        return yueSecurityBasicService.get();
    }

    @PutMapping("")
    @Primary
    public YueResult<String>edit(@Valid  @RequestBody YueSecurityBasic yueSecurityBasic, BindingResult result){
        return yueSecurityBasicService.edit(yueSecurityBasic);
    }


}
