package cn.xisoil.notice.service;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.notice.data.YueNotice;

import java.util.List;

public interface YueNoticeService {

    YueResult<List<YueNotice>>getNotice();

}
