package cn.xisoil.common.result;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import cn.xisoil.common.enums.HTTPCODE;
import lombok.Data;

/**
 * @author vien
 * @date 2021/11/8 9:27
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class YueResult<T> {

    private final String result;

    private final Integer code;

    private final String message;

    private final T data;

    private YueResult(Builder<T> builder) {
        this.result = builder.result;
        this.code = builder.code;
        this.message = builder.message;
        this.data = builder.data;
    }

    public static<T> Builder<T> builder() {
        return new Builder<>();
    }

    public static class Builder<T> {
        private String result;

        private Integer code;

        private String message;

        private T data;

        public Builder<T> code(Integer code) {
            this.code = code;
            return this;
        }

        public Builder<T>  message(String message) {
            this.message = message;
            return this;
        }

        public Builder<T>  result(String result) {
            this.result = result;
            return this;
        }

        public Builder<T>  result(HTTPCODE result) {
            this.result = result.getMessage();
            this.code = result.getCode();
            return this;
        }

        public Builder<T>  success() {
            this.result = "SUCCESS";
            this.code = 200;
            return this;
        }

        public Builder<T>  fail() {
            this.result = "FAILURE";
            this.code = 500;
            return this;
        }

        public Builder<T>  data(T data) {
            this.data = data;
            return this;
        }

        public YueResult<T> build() {
            return new YueResult<>(this);
        }
    }

    @JsonIgnore
    public Boolean isFailed(){
        return this.code==500;
    }
}
