package cn.xisoil.common.exception;

import cn.xisoil.common.enums.HTTPCODE;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import jakarta.servlet.http.HttpServletRequest;

public class ResponseException extends RuntimeException{

    public ResponseException(){
        super();
    }

    public static Integer code=500;

    public Integer getCode() {
        return code;
    }

    public ResponseException(HTTPCODE message){
        code=message.getCode();
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        httpServletRequest.setAttribute("jakarta.servlet.error.status_code",message.getCode());
        throw new ResponseException(message.toString());
    }

    public ResponseException(Integer codes,String message){
        code=codes;
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        httpServletRequest.setAttribute("jakarta.servlet.error.status_code",code);
        throw new ResponseException(message);
    }

    public ResponseException(String message){
        super(message);
    }



}
