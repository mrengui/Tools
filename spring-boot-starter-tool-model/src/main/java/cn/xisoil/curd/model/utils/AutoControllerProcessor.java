package cn.xisoil.curd.model.utils;


import cn.xisoil.curd.dao.YueRepository;
import cn.xisoil.curd.model.controller.ModelCurdController;
import cn.xisoil.curd.model.controller.SingleModelController;
import com.google.auto.service.AutoService;
import org.springframework.javapoet.*;
import cn.xisoil.curd.model.enums.ObjectType;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import cn.xisoil.curd.model.interfaces.CurdTrusteeShipObject;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.File;
import java.io.IOException;
import java.util.*;

@AutoService(Processor.class)
public class AutoControllerProcessor extends AbstractProcessor {

    private Elements elementTool;
    private Messager messager;
    private Filer filer;
    public static String CONTROLLER="Controller";
    public static String REPOSITORY="Repository";
    public static String TARGET="target/classes/";
    public static String SRC="src/main/java/";
    public static String BASEMAPPING="/manage";

    private final String fileDir= Objects.requireNonNull(this.getClass().getResource("/")).getPath().replace(TARGET,SRC);

    private final String persistentDir=fileDir+properties.persistent.replace(".","/");
    private final String controllerDir=fileDir+properties.controller.replace(".","/");

    private static final ConfigProperties properties=new ConfigProperties();


    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        elementTool = processingEnv.getElementUtils();
        messager = processingEnv.getMessager();
        filer = processingEnv.getFiler();
        super.init(processingEnv);
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> set = new HashSet<>();
        set.add(CurdModelObject.class.getCanonicalName());
        return set;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        if (!annotations.isEmpty()) {
            //获取Bind注解类型的元素，这里是类类型TypeElement
            Set<? extends Element> bindElement = roundEnv.getElementsAnnotatedWith(CurdModelObject.class);
            try {
                generateCode(bindElement);
            } catch (IOException | ClassNotFoundException e) {
                messager.printMessage(Diagnostic.Kind.ERROR, e.toString());
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }


    /**
     * @param elements
     */
    private void generateCode(Set<? extends Element> elements) throws IOException, ClassNotFoundException {

        for (Element element : elements) {

            CurdModelObject curdModelObject=element.getAnnotation(CurdModelObject.class);
            CurdTrusteeShipObject curdTrusteeShipObject =curdModelObject.trusteeship();
            JavaPoetAutoGenerate javaPoetAutoGenerate=new JavaPoetAutoGenerate(curdModelObject);

            //由于是在类上注解，那么获取TypeElement
            TypeElement typeElement = (TypeElement) element;
            //获取全限定类名
            String QualifiedName = typeElement.getQualifiedName().toString();
            //获取包路径
            PackageElement packageElement = elementTool.getPackageOf(typeElement);
            String packageName = packageElement.getQualifiedName().toString();
            //获取用于生成的类名
            String className = getClassName(typeElement, packageName);
            String lowerClassName=className.toLowerCase();

            //代码生成位置
            File file=new File(fileDir);

            if (curdTrusteeShipObject.auto()) {

                //持久层包名
                String persistent = String.format("%s.%s",properties.persistent,lowerClassName);

                //持久层类名
                String persistentClass =String.format("%s%s",className,REPOSITORY);

                //控制层包名
                String controller = String.format("%s.%s",properties.controller,lowerClassName);

                //控制层类名
                String controllerClass = String.format("%s%s",className,CONTROLLER);

                //控制层mapping
                String controllerManageMapping =String.format("%s%s",BASEMAPPING,StringUtils.isNotBlank(curdTrusteeShipObject.router())?
                        curdTrusteeShipObject.router():"/"+lowerClassName) ;

                if (!new File(persistentDir+"/"+lowerClassName+"/"+persistentClass+".java").exists()){
                    //生成的类
                    TypeSpec persistentType = TypeSpec
                            .interfaceBuilder(persistentClass)
                            .addModifiers(Modifier.PUBLIC)
                            .addSuperinterface(ParameterizedTypeName.get(
                                    ClassName.get(YueRepository.class),
                                    ClassName.get(packageName, className),
                                    ClassName.get("java.lang", "String")))
                            .build();
                    JavaFile.builder(persistent, persistentType)
                            .build().writeTo(file);
                }
                if (!new File(controllerDir+"/"+lowerClassName+"/"+controllerClass+".java").exists()){
                    //单页or列表页
                    ObjectType objectType =curdTrusteeShipObject.type();
                    Class<?> ModelCurdControllerMapping = objectType.equals(ObjectType.LIST)? ModelCurdController.class: SingleModelController.class;
                    TypeSpec.Builder typeBuilder = TypeSpec
                            .classBuilder(controllerClass )
                            .addModifiers(Modifier.PUBLIC)
                            .superclass(ParameterizedTypeName.get(
                                    ClassName.get(ModelCurdControllerMapping),
                                    ClassName.get(packageName, className),
                                    ClassName.get(persistent,persistentClass)))
                            .addAnnotation(javaPoetAutoGenerate.requestMappingAnnotationSpec(controllerManageMapping))
                            .addAnnotation(javaPoetAutoGenerate.restControllerAnnotationSpec(controllerClass));
                    if (ModelCurdControllerMapping.equals(ModelCurdController.class)){
                        //生成的类
                        typeBuilder.addMethod(javaPoetAutoGenerate.delMethSpec())
                                .addMethod(javaPoetAutoGenerate.addMethSpec(ClassName.get(packageName, className)))
                                .addMethod(javaPoetAutoGenerate.pageMethSpec())
                                .addMethod(javaPoetAutoGenerate.editMethSpec(ClassName.get(packageName, className)))
                                .addMethod(javaPoetAutoGenerate.getMethSpec(String.class));

                    }
                    else {
                        //生成的类
                        typeBuilder.addMethod(javaPoetAutoGenerate.getMethSpec())
                                .addMethod(javaPoetAutoGenerate.editMethSpec(ClassName.get(packageName, className)));
                    }
                    //创建javaFile文件对象
                    JavaFile.builder(controller, typeBuilder.build())
                            .build().writeTo(file);
                }

            }

        }
    }

    private static String getClassName(TypeElement type, String packageName) {
        int packageLen = packageName.length() + 1;
        return type.getQualifiedName().toString().substring(packageLen)
                .replace('.', '$');
    }



}
