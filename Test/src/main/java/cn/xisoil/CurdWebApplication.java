package cn.xisoil;

import cn.xisoil.utils.EnableModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages={"cn.xisoil.*"})
@EnableJpaAuditing
@EnableAsync
@EnableTransactionManagement
@EnableModel
public class CurdWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(CurdWebApplication.class, args);
    }
}
