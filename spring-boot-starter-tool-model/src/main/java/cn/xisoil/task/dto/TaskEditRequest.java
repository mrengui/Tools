package cn.xisoil.task.dto;

import lombok.Data;

@Data
public class TaskEditRequest {

    private String id;

    private String cron;

}
