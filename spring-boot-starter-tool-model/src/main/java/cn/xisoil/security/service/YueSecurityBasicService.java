package cn.xisoil.security.service;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.security.data.YueSecurityBasic;

public interface YueSecurityBasicService {

    YueResult<YueSecurityBasic> get();

    YueResult<String>edit(YueSecurityBasic edit);

}
