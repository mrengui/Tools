package cn.xisoil.auth.config.token;


import com.alibaba.fastjson.JSON;
import cn.xisoil.common.exception.ResponseException;
import io.jsonwebtoken.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * JWT过滤器
 */
@Component
public class JwtInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String method = request.getMethod();
        if ("OPTIONS".equals(method)){
            return true;
        }


        //获取请求头部信息
        String token = request.getHeader("token");
        if (token!=null) {
            try {
                JwtParser jwtParser = Jwts.parser();
                jwtParser.setSigningKey("java666");
                //只做token验证，若不通过，则抛异常
                Jws<Claims> claimsJws = jwtParser.parseClaimsJws(token);
                return true;
            }catch (ExpiredJwtException e){
//                doResponse(response,"Token已过期，请重新登陆！");
                throw new ResponseException(401,"Token已过期，请重新登陆！");
            } catch (Exception e){
                throw new ResponseException(401,"Token不合法，请自重！");
            }
        }
        throw new ResponseException(401,"Token已过期，请重新登陆！");


    }

    public void doResponse(HttpServletResponse response,String info) throws IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");

        PrintWriter writer = response.getWriter();
        ResponseEntity<String> responseEntity=new ResponseEntity<>(info, HttpStatus.UNAUTHORIZED);
        String json = JSON.toJSONString(responseEntity);
        writer.write(json);
        writer.flush();
        writer.close();
    }
}
