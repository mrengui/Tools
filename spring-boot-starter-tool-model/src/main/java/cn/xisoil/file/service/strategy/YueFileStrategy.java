package cn.xisoil.file.service.strategy;

import cn.xisoil.common.result.ResultFile;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.common.to.ListStringRequest;
import cn.xisoil.file.data.YueFile;

import jakarta.servlet.http.HttpServletRequest;


public interface YueFileStrategy {

    ResultFile<String> upload(HttpServletRequest request);

    YueResult<String>delete(YueFile file);


}
