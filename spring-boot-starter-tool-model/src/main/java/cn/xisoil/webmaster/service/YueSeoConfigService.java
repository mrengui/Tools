package cn.xisoil.webmaster.service;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.webmaster.data.YueSeoConfig;
import org.springframework.web.bind.annotation.RequestBody;

public interface YueSeoConfigService {

    YueResult<YueSeoConfig> getSeoConfig();

    YueResult<String>setSeoConfig(YueSeoConfig yueSeoConfig);


    YueResult<String>pushBaidu(String siteUrl);

}
