package cn.xisoil.webmaster.dao;

import cn.xisoil.curd.dao.YueRepository;
import cn.xisoil.webmaster.data.YueSeoConfig;

import java.util.Optional;

public interface YueSeoConfigRepository extends YueRepository<YueSeoConfig,String> {

    Optional<YueSeoConfig>findTopByIdIsNotNull();

}
