package cn.xisoil.analysis.dao;

import cn.xisoil.analysis.data.AnalysisEquipment;
import cn.xisoil.analysis.data.YueEquipmentAnalysis;
import cn.xisoil.analysis.data.YueSectionAnalysis;
import cn.xisoil.curd.dao.YueRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface YueEquipmentAnalysisRepository extends YueRepository<YueEquipmentAnalysis,String> {

    Optional<YueEquipmentAnalysis>findTopByDateAndEquipment(String time, AnalysisEquipment equipment);

    @Query(value = "select new map(equipment.equipment as name,sum(equipment.uvCount) as value)  from YueEquipmentAnalysis equipment group by equipment.equipment order by sum(equipment.uvCount) desc")
    List<Map<String,Object>> findTop5SumByEquipment();

}
