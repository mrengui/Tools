package cn.xisoil.analysis.dao;

import cn.xisoil.analysis.data.YueAnalysis;
import cn.xisoil.analysis.data.YueDateAnalysis;
import cn.xisoil.curd.dao.YueRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface YueDateAnalysisRepository extends YueRepository<YueDateAnalysis,String> {

    Optional<YueDateAnalysis>findTopByDate(String time);


    List<YueDateAnalysis> findAllByDateIn(List<String> date);


}
