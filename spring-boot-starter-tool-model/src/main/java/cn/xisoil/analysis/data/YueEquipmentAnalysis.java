package cn.xisoil.analysis.data;

import cn.xisoil.curd.model.interfaces.CurdModelObject;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.*;

@Data
@Entity(name = "YueEquipmentAnalysis")
@Table(name = "yue_equipment_analysis")
@EntityListeners(AuditingEntityListener.class)
@CurdModelObject(value = "设备访问日志")
public class YueEquipmentAnalysis {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    private String date;

    @Enumerated(value = EnumType.STRING)
    private AnalysisEquipment equipment;

    private Integer pvCount=0;

    private Integer uvCount=0;


}
