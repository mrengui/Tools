package cn.xisoil.log.controller;

import cn.xisoil.curd.model.controller.ModelCurdControllerMapping;
import cn.xisoil.datacheck.PermissionCheckAutomation;
import cn.xisoil.log.dao.LoginLogRepository;
import cn.xisoil.log.dao.YueSystemLogDataRepository;
import cn.xisoil.log.data.YueLoginLogData;
import cn.xisoil.log.data.YueSystemLogData;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/manage/system/system/log")
public class CurdSystemLogController extends ModelCurdControllerMapping<YueSystemLogData, YueSystemLogDataRepository>{

    @Override
    public String getUrl() {
        return "/system/system/log";
    }

    @Override
    public String getName() {
        return "系统日志";
    }

    @Override
    public String getParent() {
        return "系统管理";
    }
}
