package cn.xisoil.notice.controller;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.curd.model.controller.ModelCurdControllerMapping;
import cn.xisoil.datacheck.PermissionCheckAutomation;
import cn.xisoil.notice.dao.YueNoticeRepository;
import cn.xisoil.notice.data.YueNotice;
import cn.xisoil.notice.service.YueNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RequestMapping("/manage/notice")
@RestController
public class CurdNoticeController extends ModelCurdControllerMapping<YueNotice, YueNoticeRepository>{

    @Autowired
    private YueNoticeService yueNoticeService;

    @GetMapping("/user")
    public YueResult<List<YueNotice>> getList(){
        return yueNoticeService.getNotice();
    }


    @Override
    public String getUrl() {
        return "/notice";
    }

    @Override
    public String getName() {
        return "用户通知";
    }

    @Override
    public String getParent() {
        return "通知管理";
    }
}
