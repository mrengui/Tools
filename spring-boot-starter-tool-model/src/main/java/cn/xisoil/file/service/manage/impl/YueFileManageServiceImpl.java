package cn.xisoil.file.service.manage.impl;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.common.to.ListStringRequest;
import cn.xisoil.file.dao.FileRepository;
import cn.xisoil.file.data.YueFile;
import cn.xisoil.file.service.manage.YueFileManageService;
import cn.xisoil.file.service.strategy.YueFileStrategy;
import cn.xisoil.file.utils.YueFileExtract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class YueFileManageServiceImpl implements YueFileManageService {

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    @Lazy
    private Map<String, YueFileStrategy> yueFileConfigMap=new HashMap<>();


    @Override
    public void save(YueFile yueFile) {
        yueFile.setType(YueFileExtract
                .getType(yueFile.getFilename()
                        .substring(yueFile
                                .getFilename()
                                .lastIndexOf(".")+1
                        )
                )
        );
        fileRepository.save(yueFile);
    }

    @Override
    @Transactional
    public YueResult<String> delete(ListStringRequest request) {

        List<YueFile> files= fileRepository.findUrlByIdIn(request.getIds());
        fileRepository.deleteAllByIdIn(request.getIds());
        files.forEach(file->{
            yueFileConfigMap.get(file.getUploadtype().getType()).delete(file);
        });

        return YueResult.<String>builder().success().message("删除成功").build();
    }
}
