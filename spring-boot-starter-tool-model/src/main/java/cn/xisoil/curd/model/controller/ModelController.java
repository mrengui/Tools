package cn.xisoil.curd.model.controller;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.curd.model.data.ModelClass;
import cn.xisoil.curd.model.data.ModelColumn;
import cn.xisoil.curd.model.enums.ObjectType;
import cn.xisoil.curd.model.service.model.ModelService;
import cn.xisoil.datacheck.PermissionCheckAutomation;
import cn.xisoil.utils.GenericObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public abstract class ModelController<T> extends GenericObject<T> implements PermissionCheckAutomation<T> {

    @Autowired
    private ModelService modelService;

    @GetMapping("/columns")
    public YueResult<ModelClass> columns() {
        ModelClass yueModelClass= modelService.getModelClass(super.getGenericClass());
        yueModelClass.setType(ObjectType.LIST);
        List<ModelColumn>columns= modelService.getModelColumns(super.getGenericClass());
        Map<String,List<ModelColumn>>groups=columns.stream().collect(Collectors.groupingBy(ModelColumn::getGroup));
        yueModelClass.setColumns(columns);
        yueModelClass.setGroups(groups);
        return YueResult.<ModelClass>builder().data(yueModelClass).success().message("获取成功").build();
    }


}
