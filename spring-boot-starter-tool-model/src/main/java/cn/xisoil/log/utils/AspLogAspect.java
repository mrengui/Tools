package cn.xisoil.log.utils;

import cn.xisoil.auth.data.user.YueLoginUser;
import cn.xisoil.auth.dto.login.LoginDto;
import cn.xisoil.auth.dto.login.LoginRequest;
import cn.xisoil.auth.config.token.JwtUtils;
import cn.xisoil.common.exception.ResponseException;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.curd.model.service.model.ModelService;
import cn.xisoil.log.data.YueLoginLogData;
import cn.xisoil.log.data.YueSystemLogData;
import cn.xisoil.log.interfaces.Log;
import cn.xisoil.log.service.LoginLogService;
import cn.xisoil.log.service.YueSystemLogDataService;
import cn.xisoil.system.model.dao.YueBasicDataRepository;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import jakarta.servlet.http.HttpServletRequest;

@Component
@Aspect
public class AspLogAspect {

    @Autowired
    private HttpRequestIpUtils httpRequestIpUtils;
    @Autowired
    private LoginLogService loginLogService;
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private ModelService modelService;
    @Autowired
    private YueSystemLogDataService yueSystemLogDataService;
    @Autowired
    private YueBasicDataRepository yueBasicDataRepository;

    @Pointcut("@annotation(cn.xisoil.log.interfaces.YueLoginLog)")
    public void YueLoginLogCut() {
    }

    @Pointcut("@annotation(cn.xisoil.log.interfaces.Log)")
    public void YueSystemLogCut() {
    }

    @AfterReturning(value = "YueLoginLogCut()",returning = "result")
    public void loginLog(YueResult<LoginDto> result){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        YueLoginLogData yueLoginLogData=new YueLoginLogData();
        yueLoginLogData.setIp(httpRequestIpUtils.getIpAddress(request));
        YueLoginUser yueLoginUser=result.getData().getUser();
        yueLoginLogData.setAccount(yueLoginUser.getAccount());
        yueLoginLogData.setIp(httpRequestIpUtils.getIpAddress(request));
        yueLoginLogData.setMessage(result.getMessage());
        yueLoginLogData.setIsSuccess(true);
        yueLoginLogData.setCode(result.getCode());
        loginLogService.save(yueLoginLogData);
    }

    @AfterThrowing(value = "YueLoginLogCut()",throwing = "e")
    public void loginLogThrow(JoinPoint point, ResponseException e){
        Object[] objects=point.getArgs();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        YueLoginLogData yueLoginLogData=new YueLoginLogData();
        LoginRequest loginRequest=(LoginRequest)objects[0];
        yueLoginLogData.setIp(httpRequestIpUtils.getIpAddress(request));
        yueLoginLogData.setAccount(loginRequest.getAccount());
        yueLoginLogData.setMessage(e.getMessage());
        yueLoginLogData.setIsSuccess(false);
        yueLoginLogData.setCode(e.getCode());
        loginLogService.save(yueLoginLogData);
    }

    @Before(value = "@annotation(log)")
    public void systemLogin(JoinPoint point, Log log){
        if (yueBasicDataRepository.existsByIsLogIsTrue()){
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            YueSystemLogData yueSystemLogData=new YueSystemLogData();
            YueLoginUser yueLoginUser=jwtUtils.getLoginUser();
            yueSystemLogData.setAccount(yueLoginUser.getAccount());
            yueSystemLogData.setIp(httpRequestIpUtils.getIpAddress(request));
            yueSystemLogData.setContent(log.value());
            yueSystemLogDataService.save(yueSystemLogData);
        }
    }

}
