package cn.xisoil.config;
import cn.xisoil.auth.config.token.JwtInterceptor;
import cn.xisoil.security.aop.IpInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class MyInterceptorConfig implements WebMvcConfigurer {

    @Autowired
    private JwtInterceptor jwtInterceptor;
    @Autowired
    private IpInterceptor ipInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        List<String> pathPatterns=new ArrayList<>();
        pathPatterns.add("/manage/**");

        List<String> excludePathPatterns=new ArrayList<>();
        excludePathPatterns.add("/manage/auth/**");
        excludePathPatterns.add("/manage/system/basic");
        excludePathPatterns.add("/manage/utils/upload");

        excludePathPatterns.add("/manage/pay/notifyUrl");//微信通知不拦截
        excludePathPatterns.add("/manage/analysis/**");

        //登录拦截
        registry.addInterceptor(jwtInterceptor) //添加拦截器
                .addPathPatterns(pathPatterns) //添加拦截url
                .excludePathPatterns(excludePathPatterns); //添加不拦截url

        //黑名单拦截
        registry.addInterceptor(ipInterceptor)
                .addPathPatterns("/**");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedHeaders("*")
                .allowedMethods("*")
                .allowedOrigins("*");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/upload/**")
                .addResourceLocations("file:resources/upload/");
    }


}
