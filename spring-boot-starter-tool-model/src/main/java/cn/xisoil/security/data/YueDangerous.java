package cn.xisoil.security.data;


import cn.xisoil.curd.model.enums.ObjectColumnType;
import cn.xisoil.curd.model.interfaces.CurdModel;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.*;
import java.util.Date;

@Data
@Entity(name = "YueDangerous")
@Table(name = "Yue_dangerous")
@EntityListeners(AuditingEntityListener.class)
@CurdModelObject(value = "安全预警",edit = false,add = false)
public class YueDangerous {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    @CurdModel(value = "操作IP",type = ObjectColumnType.STRING)
    private String ip;


    @CurdModel(value = "异常时间",type = ObjectColumnType.TIME)
    @CreatedDate
    private Date createTime;

    @CurdModel(value = "描述",type = ObjectColumnType.STRING)
    private String remark;

}
