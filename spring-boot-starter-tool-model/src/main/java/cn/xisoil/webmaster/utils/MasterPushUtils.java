package cn.xisoil.webmaster.utils;

import cn.xisoil.task.utils.TaskService;
import cn.xisoil.webmaster.dao.YueSeoConfigRepository;
import cn.xisoil.webmaster.data.YueSeoConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("站长推送")
public class MasterPushUtils extends TaskService {


    @Autowired
    private YueSeoConfigRepository yueSeoConfigRepository;

    public MasterPushUtils() {
        super("0 0 0 * * ? ");
    }


    @Override
    public void setCron(String cron) {
        YueSeoConfig yueSeoConfig=yueSeoConfigRepository.findTopByIdIsNotNull().orElse(new YueSeoConfig());
        yueSeoConfig.setCron(cron);
        yueSeoConfigRepository.save(yueSeoConfig);
        super.setCron(cron);
    }

    @Override
    public String getTaskName() {
        return "站长推送";
    }

    @Override
    public void run() {
        super.run();
    }
}
