package cn.xisoil.auth.service.permission.impl;

import cn.xisoil.auth.dao.permission.PermissionRepository;
import cn.xisoil.auth.data.permission.Permission;
import cn.xisoil.auth.service.permission.YuePermissionService;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.common.to.SearchPageRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import jakarta.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
public class YuePermissionServiceImpl implements YuePermissionService {

    @Autowired
    private PermissionRepository permissionRepository;

    @Override
    public YueResult<List<Permission>> list() {
        List<Permission>permissions=permissionRepository.findAllByParentIdIsNull();
        return YueResult.<List<Permission>>builder().success().data(permissions).build();
    }

    @Override
    public YueResult<Page<Permission>> page(SearchPageRequest searchPageRequest) {
        Pageable pageable = PageRequest.of(searchPageRequest.getPageNum(), searchPageRequest.getPageSize());
        Specification<Permission> sectionSpecification = ((root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (StringUtils.isNotBlank(searchPageRequest.getKeyword())){
                predicates.add(criteriaBuilder.like(root.get("name"),"%"+searchPageRequest.getKeyword()+"%"));
            }
            if (StringUtils.isNotBlank(searchPageRequest.getString("parentId"))){
                predicates.add(criteriaBuilder.equal(root.get("parentId"),searchPageRequest.get("parentId")));
            }
             if (StringUtils.isBlank(searchPageRequest.getKeyword()) && StringUtils.isBlank(searchPageRequest.getString("parentId"))){
                predicates.add(root.get("parentId").isNull());
            }
            //排序
            if (searchPageRequest.getOrderBy() != null) {
                criteriaQuery.orderBy(
                        searchPageRequest.getOrderBy().getSortType().equals(Sort.Direction.DESC) ?
                                criteriaBuilder.desc(root.get(searchPageRequest.getOrderBy().getColumn())) : criteriaBuilder.asc(root.get(searchPageRequest.getOrderBy().getColumn()))
                );
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        });
        Page<Permission> page = permissionRepository.findAll(sectionSpecification, pageable);
        return YueResult.<Page<Permission>>builder().data(page).success().build();
    }
}
