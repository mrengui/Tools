package cn.xisoil.system.model.controller;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.curd.model.controller.SingleModelController;
import cn.xisoil.datacheck.PermissionCheckAutomation;
import cn.xisoil.system.model.dao.YueBasicDataRepository;
import cn.xisoil.system.model.data.YueBasicData;
import cn.xisoil.system.model.service.YueBasicDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/manage/system/basic")
public class YueBasicDataController extends SingleModelController<YueBasicData, YueBasicDataRepository>   {

    @Override
    public String getUrl() {
        return "/system/basic";
    }

    @Override
    public String getName() {
        return "系统配置";
    }

    @Override
    public String getParent() {
        return "系统管理";
    }

    @Autowired
    private YueBasicDataService yueBasicDataService;

    @GetMapping()
    @Primary
    public YueResult<YueBasicData>get(){
        return yueBasicDataService.get();
    }

    @PutMapping()
    @Primary
    public YueResult<String>edit(@Valid  @RequestBody YueBasicData yueBasicData, BindingResult result){
        if (result.hasErrors()){
            for (ObjectError error : result.getAllErrors()) {
                return YueResult.<String>builder().fail().code(500).message(error.getDefaultMessage()).build();
            }
        }
        return yueBasicDataService.edit(yueBasicData);
    }


}
