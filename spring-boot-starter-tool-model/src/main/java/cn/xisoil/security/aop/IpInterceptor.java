package cn.xisoil.security.aop;

import cn.xisoil.common.enums.HTTPCODE;
import cn.xisoil.common.exception.RefusedException;
import cn.xisoil.log.utils.HttpRequestIpUtils;
import cn.xisoil.security.dao.YueDangerousRepository;
import cn.xisoil.security.data.YueDangerous;
import cn.xisoil.security.data.YueSecurityBasic;
import cn.xisoil.security.interfaces.IpIntercept;
import cn.xisoil.utils.EmailSendUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.Email;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static cn.xisoil.utils.RedisFields.SECURITY_ACCESS;
import static cn.xisoil.utils.RedisFields.SECURITY_BASIC;

@Component
@Aspect
public class IpInterceptor implements HandlerInterceptor {

    @Autowired
    private HttpRequestIpUtils httpRequestIpUtils;
    @Autowired
    private EmailSendUtils emailSendUtils;

    @Autowired
    private RedisTemplate<String, Serializable> redisCacheTemplate;

    @Autowired
    private YueDangerousRepository yueDangerousRepository;

    @Before(value = "@annotation(ipIntercept)")
    public void ipCut(JoinPoint point, IpIntercept ipIntercept){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String ip=httpRequestIpUtils.getIpAddress(request);
        Serializable serializable=redisCacheTemplate.opsForValue().get(SECURITY_ACCESS+":"+ip);
        if (serializable==null){
            redisCacheTemplate.opsForValue().set(SECURITY_ACCESS+":"+ip,1, 60,TimeUnit.SECONDS);
        }
        else {
            int count=((int)serializable)+1;
            if (count>20){
                saveLog(ip,"时间内访问次数达到阈值：20");
                try{
                    Serializable serializable1=redisCacheTemplate.opsForValue().get(SECURITY_BASIC);
                    if (serializable1!=null){
                        YueSecurityBasic yueSecurityBasic=(YueSecurityBasic)serializable1;
                        if (yueSecurityBasic.getEmailRemind()){
                            emailSendUtils.sendDangerous(ip, yueSecurityBasic.getEmail());
                        }
                    }
                }catch (Exception ignored){}
            }
            else {
                redisCacheTemplate.opsForValue().set(SECURITY_ACCESS+":"+ip,count,redisCacheTemplate.getExpire(SECURITY_ACCESS+":"+ip,TimeUnit.SECONDS),TimeUnit.SECONDS);
            }
        }

    }



    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) throws Exception {
        Serializable serializable=redisCacheTemplate.opsForValue().get(SECURITY_BASIC);
        if (serializable==null){//未配置直接放行
            return true;
        }
        else {
            String ip = httpRequestIpUtils.getIpAddress(request);
            YueSecurityBasic yueSecurityBasic = (YueSecurityBasic) serializable;
            if (!yueSecurityBasic.getIsSecurity()) {//拦截选项关闭直接放行
                return true;
            }


            //防盗链
            if (yueSecurityBasic.getIsHotlink()) {
                String referer = request.getHeader("referer");
                String host = request.getServerName();
                if ("POST".equals(request.getMethod()) || "GET".equals(request.getMethod())) {
                    if (referer == null) {
                        //为空直接放行
                        return true;
                    }
                    URL url = null;
                    try {
                        url = new URL(referer);
                    } catch (MalformedURLException e) {
                        saveLog(ip,"非正常访问，地址异常");
                        // URL解析异常，也置为404
                        response.setStatus(HTTPCODE.NOTFOUND.getCode());
                    }
                    // 首先判断请求域名和referer域名是否相同
                    if (!host.equals(url.getHost())) {
                        saveLog(ip,"防盗链拦截，盗链网站："+referer);
                        // URL解析异常，也置为404
                        response.setStatus(HTTPCODE.NOTFOUND.getCode());
                        throw new RefusedException("涉嫌非法盗链");
                    }
                }
            }



            //IP拦截
            List<String> ips = Arrays.stream(yueSecurityBasic.getRollList().split(",|，|\n")).toList();
            if (ips.contains(ip)) {
                throw new RefusedException("IP拉黑");
            }

        }
        //true放行
        //false不放行
        return true;
    }

    /**
     * 方法内部处理完成，页面渲染之前
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
    }

    /**
     * 页面渲染之后
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
                                Object handler, Exception ex) throws Exception {

    }

    private void saveLog(String ip,String message){
        YueDangerous yueDangerous=new YueDangerous();
        yueDangerous.setIp(ip);
        yueDangerous.setRemark(message);
        yueDangerousRepository.save(yueDangerous);
    }


}
