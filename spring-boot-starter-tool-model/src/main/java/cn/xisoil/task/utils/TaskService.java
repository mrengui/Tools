package cn.xisoil.task.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

public abstract class TaskService implements Runnable {

    public TaskService(String cron){
        this.cron=cron;
    }

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public String cron;

    @Autowired
    @Lazy
    private TaskTools taskTools;

    public void setCron(String cron) {
        this.cron = cron;
    }

    public String getTaskName() {
        return Thread.currentThread().getName();
    }

    public final  String getCron(){
        return cron;
    }

    @Override
    public void run() {
        logger.warn("计划任务==>你还未设置任何事件在此任务");
    }


}
