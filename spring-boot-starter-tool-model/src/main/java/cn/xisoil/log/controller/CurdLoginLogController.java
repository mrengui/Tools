package cn.xisoil.log.controller;

import cn.xisoil.curd.model.controller.ModelCurdControllerMapping;
import cn.xisoil.datacheck.PermissionCheckAutomation;
import cn.xisoil.log.dao.LoginLogRepository;
import cn.xisoil.log.data.YueLoginLogData;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/manage/system/login/log")
public class CurdLoginLogController extends ModelCurdControllerMapping<YueLoginLogData, LoginLogRepository>{

    @Override
    public String getUrl() {
        return "/system/login/log";
    }

    @Override
    public String getName() {
        return "登录日志";
    }

    @Override
    public String getParent() {
        return "系统管理";
    }
}
