package cn.xisoil.auth.dto.login;

import lombok.Data;

import java.io.Serializable;


@Data
public class UserDto implements Serializable {

    private String id;

    private String number;

    private String nickname;

    private String roleId;

    private String roleName;

    private Boolean authority;

    private String urgencyContent;


}
