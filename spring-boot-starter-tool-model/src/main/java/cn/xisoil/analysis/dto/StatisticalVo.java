package cn.xisoil.analysis.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class StatisticalVo {

    private List<String>times=new ArrayList<>();

    private List<Integer>pvCount=new ArrayList<>();

    private List<Integer>uvCount=new ArrayList<>();

}
