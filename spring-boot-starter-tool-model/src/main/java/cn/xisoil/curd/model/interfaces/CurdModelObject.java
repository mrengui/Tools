package cn.xisoil.curd.model.interfaces;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CurdModelObject {

    //类托管，全自动生成curd
    CurdTrusteeShipObject trusteeship() default @CurdTrusteeShipObject(auto = false);

    //权限托管，全自动生成权限
    PermissionTrusteeShipObject permission() default @PermissionTrusteeShipObject(key = "");

    String value();

    boolean delete() default true;

    boolean edit() default true;

    boolean add() default true;


}
