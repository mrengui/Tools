package cn.xisoil.utils;

import cn.xisoil.security.server.interfaces.RedisDirField;
import cn.xisoil.security.server.interfaces.RedisDirObject;

@RedisDirObject
public class RedisFields {

    @RedisDirField("安全访问记录")
    public static final String SECURITY_ACCESS="SECURITY:ACCESS";

    @RedisDirField("安全配置")
    public static final String SECURITY_BASIC="SECURITY:BASIC";

    @RedisDirField(value = "访问记录")
    public static final String VISIT_IP = "VISIT:IP:";


}
