package cn.xisoil.common.to;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.springframework.data.domain.Sort;
import org.thymeleaf.util.MapUtils;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 分页操作
 */
@Data
public class SearchPageRequest  extends JSONObject implements Serializable{

    @NotNull(message = "分页参数不能为空")
    private Integer pageSize;

    @NotNull(message = "分页参数不能为空")
    private Integer pageNum;

    private String keyword;

    public OrderBy getOrderBy() {
        return orderBy;
    }

    private OrderBy orderBy;

    @Data
    public class OrderBy{

        private String column;

        private Sort.Direction sortType= Sort.Direction.DESC;

    }

    public Object put(String key, Object value) {
        try{
            switch (key) {
                case "pageSize":
                    this.pageSize = Integer.parseInt(value.toString());
                    break;
                case "pageNum":
                    this.pageNum = Integer.parseInt(value.toString());
                    break;
                case "keyword":
                    this.keyword = value.toString();
                    break;
                case "orderBy":
                    if (value!=null){
                        Map<String, Object> map = (Map<String, Object>) value;
                        this.orderBy = new OrderBy();
                        this.orderBy.setColumn(map.get("column").toString());
                        this.orderBy.setSortType(Objects.equals(map.get("sortType"), "ASC") ? Sort.Direction.ASC : Sort.Direction.DESC);
                    }
                    break;
                default:
                    return super.put(key, value);
            }
            return value;
        }
        catch (Exception e){
            return value;
        }

    }


    public Integer getPageNum() {
        return pageNum-1;
    }

}
