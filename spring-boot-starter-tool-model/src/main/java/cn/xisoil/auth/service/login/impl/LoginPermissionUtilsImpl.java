package cn.xisoil.auth.service.login.impl;

import cn.xisoil.auth.dao.permission.PermissionRepository;
import cn.xisoil.auth.dao.role.YueRoleRepository;
import cn.xisoil.auth.data.permission.Permission;
import cn.xisoil.auth.data.role.YueRole;
import cn.xisoil.auth.service.login.LoginPermissionUtils;
import cn.xisoil.common.exception.ResponseException;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class LoginPermissionUtilsImpl implements LoginPermissionUtils {


    @Autowired
    private PermissionRepository permissionRepository;
    @Autowired
    private YueRoleRepository yueRoleRepository;
    @Autowired
    @PersistenceContext()
    EntityManager entityManager;

    @Override
    public List<Permission> structureByRoleId(String roleId) {
        YueRole yueRole=yueRoleRepository.findById(roleId).orElseThrow(()->new ResponseException(403,"账户已被取消授权！"));
        Session session = entityManager.unwrap(Session.class);

//        session.enableFilter("permissionChildren").setParameterList("ids",yueRole.getPermissionIds());
        Specification<Permission>specification=((root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate>predicates=new ArrayList<>();
            predicates.add(criteriaBuilder.or(
                    criteriaBuilder.and(
                            root.get("id").in(yueRole.getPermissionIds()),
                            root.get("parentId").isNull()
                            ),
                    criteriaBuilder.and(
                            root.get("id").in(yueRole.getPermissionIds()),
                            criteriaBuilder.not(root.get("parentId").in(yueRole.getPermissionIds())),
                            root.get("parentId").isNotNull()
                    )
            ));
            criteriaQuery.orderBy( criteriaBuilder.asc(root.get("comparable")));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        });
        List<Permission>permissions=permissionRepository.findAll(specification);
//        session.disableFilter("permissionChildren");
        return permissions;
    }

}
