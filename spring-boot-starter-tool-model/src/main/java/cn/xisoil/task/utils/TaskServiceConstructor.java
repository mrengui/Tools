package cn.xisoil.task.utils;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@Data
public class TaskServiceConstructor {

    private TaskService taskService;

    @Getter(AccessLevel.NONE)
    private ScheduledFuture<?> schedule;

    public TaskServiceConstructor() {

    }

    public TaskServiceConstructor(TaskService taskService, ScheduledFuture<?> schedule) {
        this.taskService = taskService;
        this.schedule = schedule;
    }

    public void run() {
        taskService.run();
    }


    void close() {
       schedule.cancel(true);
    }

    public Boolean getDone() {
        return schedule.isDone();
    }

    public String getNextTime() {
        long secondCount = schedule.getDelay(TimeUnit.SECONDS);
        long day = secondCount / (24 * 60 * 60 );
        long hour = (secondCount / (60 * 60) - day * 24);
        long min = ((secondCount / (60)) - day * 24 * 60 - hour * 60);
        long second = secondCount-min*60 - hour*60*60 -day*24*60*60;
        return day + "天" + hour + "时" + min + "分钟" + second + "秒";
    }

    public Boolean getCancelled() {
        return schedule.isCancelled();
    }
}
