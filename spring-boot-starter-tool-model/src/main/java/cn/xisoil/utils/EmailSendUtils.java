package cn.xisoil.utils;

import cn.xisoil.common.exception.NormalException;
import cn.xisoil.system.model.dao.YueBasicDataRepository;
import cn.xisoil.system.model.data.YueBasicData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.spring6.SpringTemplateEngine;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.thymeleaf.web.IWebExchange;
import org.thymeleaf.web.servlet.JakartaServletWebApplication;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Component
public class EmailSendUtils {

    @Autowired
    private YueBasicDataRepository yueBasicDataRepository;
    @Autowired
    private SpringTemplateEngine templateEngine;

    public void sendDangerous(String ip,String noticeEmail){
        YueBasicData yueBasicData=yueBasicDataRepository.findTopByNameIsNotNull().orElse(new YueBasicData());
        JavaMailSenderImpl senderImpl = new JavaMailSenderImpl();
        senderImpl.setHost(yueBasicData.getEmailSmtp());
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.auth", "true");
        senderImpl.setJavaMailProperties(properties);
        senderImpl.setUsername(yueBasicData.getEmailAccount());
        senderImpl.setPassword(yueBasicData.getEmailPassword());
        MimeMessage mimeMessage = senderImpl.createMimeMessage();
        try {
            //需要借助Helper类
            MimeMessageHelper helper=new MimeMessageHelper(mimeMessage ,true, "utf-8");
            Map<String,Object> map=new HashMap<>();
            map.put("ip",ip);
            map.put("time",new Date());
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
            IWebExchange webExchange = JakartaServletWebApplication.buildApplication(request.getServletContext())
                    .buildExchange(request, response);
            String context=templateEngine.process("dist/error/dangerousError", new WebContext(webExchange,
                    request.getLocale(),map));
                helper.setFrom(new InternetAddress(yueBasicData.getEmailAccount(), yueBasicData.getName(), "UTF-8"));
            helper.setTo(noticeEmail);
            helper.setBcc(new InternetAddress(yueBasicData.getEmailAccount(), yueBasicData.getName(), "UTF-8"));
            helper.setSubject("危险预警");
            helper.setSentDate(new Date());//发送时间
            helper.setText(context,true);
            //第一个参数要发送的内容，第二个参数是不是Html格式。
            senderImpl.send(mimeMessage);
        } catch (Exception e) {
            throw new NormalException(e.getMessage());
        }
    }

}
