package cn.xisoil.file.service.manage;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.common.to.ListStringRequest;
import cn.xisoil.file.data.YueFile;

public interface YueFileManageService {

    void  save(YueFile yueFile);

    YueResult<String>delete(ListStringRequest request);

}
