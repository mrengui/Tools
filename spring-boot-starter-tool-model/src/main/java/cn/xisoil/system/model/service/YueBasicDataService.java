package cn.xisoil.system.model.service;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.system.model.data.YueBasicData;

public interface YueBasicDataService {

    YueResult<YueBasicData>get();

    YueResult<String>edit(YueBasicData edit);

}
