package cn.xisoil.curd.model.utils;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * @Description 获取配置信息，编译器使用，编译器无法获取上下文，采用此方法获取配置
 * @Author Vien
 * @CreateTime 2023-02-2023/2/25 22:38:20
 **/
public class ConfigProperties {

    public Properties properties= new Properties();

    public String persistent;
    public String controller;



    private static List<String> configDir=Arrays.asList(
            "/application.properties",
            "/application.yml",
            "/application.yaml",
            "/bootstrap.properties",
            "/bootstrap.yml",
            "/bootstrap.yaml"
    );

    public ConfigProperties(){
        configDir.forEach(dir->{
            InputStream in = this.getClass().getResourceAsStream(dir);
            if (in!=null){
                try{
                    properties.load(in);
                    if (StringUtils.isNotBlank(properties.getProperty("active"))){
                        properties.load(this.getClass().getResourceAsStream(dir.replace(".","-"+properties.getProperty("active")+".")));
                    }
                    persistent= properties.getProperty("persistent","cn.xisoil.dao");
                    controller= properties.getProperty("controller","cn.xisoil.controller");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }
        });


    }


}
