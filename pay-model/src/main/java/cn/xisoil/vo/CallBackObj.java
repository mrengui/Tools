package cn.xisoil.vo;

import lombok.Data;

@Data
public class CallBackObj {

    private String summary;

    private String event_type;

    private String create_time;

    private CallBackResource resource;

    private String resource_type;

    private String id;


}
