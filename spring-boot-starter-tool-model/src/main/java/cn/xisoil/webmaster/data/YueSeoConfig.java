package cn.xisoil.webmaster.data;


import cn.xisoil.curd.model.enums.ObjectColumnType;
import cn.xisoil.curd.model.interfaces.CurdModel;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.*;

@Data
@Entity(name = "YueSeoConfig")
@Table(name = "yue_seo_config")
@EntityListeners(AuditingEntityListener.class)
@CurdModelObject(value = "站长配置")
public class YueSeoConfig {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    private String domain;

    @CurdModel(value = "百度密钥",editor = false,comparable = true)
    private String baiduKey;

    @CurdModel(value = "自动提交")
    private Boolean autoCommit;

    @CurdModel(value = "cron表达式",type = ObjectColumnType.STRING)
    private String cron;

    @CurdModel(value = "360账号",type = ObjectColumnType.STRING)
    private String account360;

    @CurdModel(value = "360密码",type = ObjectColumnType.STRING)
    private String password360;

    private String accountSogou;

    private String passwordSogou;

}
