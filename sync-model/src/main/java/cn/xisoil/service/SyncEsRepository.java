package cn.xisoil.service;

import cn.xisoil.curd.dao.YueRepository;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;

@NoRepositoryBean
public interface SyncEsRepository<T1,ID extends Serializable> extends YueRepository<T1, ID> {

     T1  syncSave(T1 t);

     void syncDelete(T1 t);

     void syncDeleteById(ID id);

     void syncDeleteAll(List<T1> ts);

     void syncDeleteByIdIn(List<ID> id);

     SearchHits<T1> esAll();

     T1 esById(ID id);

}
