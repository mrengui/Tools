package cn.xisoil.exception;

/**
 * @Description TODO
 * @Author Vien
 * @CreateTime 2023-03-2023/3/13 19:20:43
 **/
public class FileUploadException extends RuntimeException{

    public FileUploadException() {
    }

    public FileUploadException(String message) {
        super(message);
    }

    public FileUploadException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileUploadException(Throwable cause) {
        super(cause);
    }

    public FileUploadException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
