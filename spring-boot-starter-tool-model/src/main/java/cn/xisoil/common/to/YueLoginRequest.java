package cn.xisoil.common.to;

import lombok.Data;

@Data
public class YueLoginRequest {

    private String account;

    private String password;

    private String verificationCode;

}
