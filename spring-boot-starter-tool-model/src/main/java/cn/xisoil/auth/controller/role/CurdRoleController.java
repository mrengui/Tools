package cn.xisoil.auth.controller.role;

import cn.xisoil.auth.data.role.YueRole;
import cn.xisoil.auth.dao.role.YueRoleRepository;
import cn.xisoil.curd.model.controller.ModelCurdControllerMapping;
import cn.xisoil.datacheck.PermissionCheckAutomation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/manage/role")
public class CurdRoleController extends ModelCurdControllerMapping<YueRole, YueRoleRepository>  {
    @Override
    public String getUrl() {
        return "/role";
    }

    @Override
    public String getName() {
        return "角色管理";
    }

    @Override
    public String getParent() {
        return "用户管理";
    }
}
