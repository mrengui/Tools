package cn.xisoil.analysis.utils;


import cn.xisoil.analysis.data.AnalysisBrowser;
import cn.xisoil.analysis.data.AnalysisEquipment;
import org.springframework.stereotype.Component;

@Component
public class AnalysisUtils {

    public AnalysisEquipment getEquipment(String equipmentName){
        if (equipmentName.contains("WINDOW"))
        {
            return AnalysisEquipment.WINDOWS;
        }
        if (equipmentName.contains("ANDROID")){
            return AnalysisEquipment.ANDROID;
        }
        if (equipmentName.contains("MAC")){
            return AnalysisEquipment.MAC;
        }
        return AnalysisEquipment.OTHER;
    }

    public AnalysisBrowser getBrowser(String browserName){
        browserName=browserName.toLowerCase();
        if (browserName.contains("msie")){
            return AnalysisBrowser.IE;
        }
        if (browserName.contains("opera")){
            return AnalysisBrowser.OPERA;
        }
        if (browserName.contains("firefox")){
            return AnalysisBrowser.FIREFOX;
        }
        if (browserName.contains("safari")){
            return AnalysisBrowser.SAFARI;
        }
        if (browserName.contains("edge")){
            return AnalysisBrowser.EDGE;
        }
        if (browserName.contains("chrome")){
            return AnalysisBrowser.CHROME;
        }
        return AnalysisBrowser.OTHER;
    }

}
