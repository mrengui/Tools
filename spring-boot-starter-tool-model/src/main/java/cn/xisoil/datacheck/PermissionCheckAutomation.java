package cn.xisoil.datacheck;

import cn.hutool.core.lang.generator.ObjectGenerator;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public interface PermissionCheckAutomation<T> {


    default String getUrl() {
        String path = this.getClass().getAnnotation(RequestMapping.class).value()[0];
        return path.replaceAll("/manage", "");
    }


    default boolean auto(){
        Type type = this.getClass().getGenericSuperclass();
        Class<T> clazz = (Class<T>) ((ParameterizedType) type).getActualTypeArguments()[0];
        CurdModelObject curdModelObject=clazz.getAnnotation(CurdModelObject.class);
        return curdModelObject.permission().auto();
    }

    default String getName() {
        Type type = this.getClass().getGenericSuperclass();
        Class<T> clazz = (Class<T>) ((ParameterizedType) type).getActualTypeArguments()[0];
        CurdModelObject curdModelObject=clazz.getAnnotation(CurdModelObject.class);
        return curdModelObject.value();
    }

    default String getParent(){
        Type type = this.getClass().getGenericSuperclass();
        Class<T> clazz = (Class<T>) ((ParameterizedType) type).getActualTypeArguments()[0];
        CurdModelObject curdModelObject=clazz.getAnnotation(CurdModelObject.class);
        return curdModelObject.permission().parent();
    }

}
