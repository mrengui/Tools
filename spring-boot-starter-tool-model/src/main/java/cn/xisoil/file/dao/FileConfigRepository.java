package cn.xisoil.file.dao;

import cn.xisoil.curd.dao.YueRepository;
import cn.xisoil.file.data.YueFileConfig;

public interface FileConfigRepository extends YueRepository<YueFileConfig,String> {
}
