package cn.xisoil.file.service.upload.impl;

import cn.xisoil.common.result.ResultFile;
import cn.xisoil.data.UploadConfigClient;
import cn.xisoil.file.dao.FileConfigRepository;
import cn.xisoil.file.data.YueFileConfig;
import cn.xisoil.file.service.upload.UploadFileService;
import cn.xisoil.file.service.strategy.YueFileStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class UploadFileServiceImpl implements UploadFileService {

    @Autowired
    private FileConfigRepository fileConfigRepository;

    @Autowired
    private Map<String, YueFileStrategy> yueFileConfigMap=new HashMap<>();

    @Override
    public ResultFile<String> upload(HttpServletRequest request) {
        YueFileConfig yueFileConfig= fileConfigRepository.findTopByIdNotNull().orElse(new YueFileConfig());
       return yueFileConfigMap.get(yueFileConfig.getUploadtype().getType()).upload(request);

    }



}
