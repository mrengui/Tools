package cn.xisoil;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages={"cn.xisoil.*"})
@EnableJpaAuditing
@EnableAsync
@EnableTransactionManagement
public class SyncApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context= SpringApplication.run(SyncApplication.class, args);
    }

}
