package cn.xisoil.security.server.interfaces;

import cn.xisoil.utils.YueContextUtils;
import cn.xisoil.security.server.data.YueRedisDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;
import java.lang.reflect.Field;
import java.util.*;

@Component
public class RedisDirKeyUtils{

    @Autowired
    YueContextUtils yueContextUtils;


    private final List<YueRedisDir>redisDirs=new ArrayList<>();

    @PostConstruct
    public void init(){
        Map<String, Object> handlerMap=yueContextUtils.getBeansWithAnnotation(RedisDirObject.class);
        handlerMap.values().forEach(a->{
            Field[] fields = a.getClass().getDeclaredFields();
            for (int j = 0; j < fields.length; j++) {
                Field field = fields[j];
                if (field.isAnnotationPresent(RedisDirField.class)) {
                    try {
                        //获取SignalsRuleExportDTO字段上的ExcelProperty注解实例
                        RedisDirField redisDirField = field.getAnnotation(RedisDirField.class);
                        YueRedisDir yueRedisDir=new YueRedisDir();
                        String dirName=field.get("value").toString();
                        if (dirName.endsWith(":")){
                            dirName=dirName.substring(0,dirName.length()-1);
                        }
                        yueRedisDir.setDirName(dirName);
                        yueRedisDir.setRemark(redisDirField.value());
                        redisDirs.add(yueRedisDir);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        });
    }

    public List<YueRedisDir> getRedisDirs() {
        return redisDirs;
    }

}
