package cn.xisoil.auth.service.login;

import cn.xisoil.auth.data.permission.Permission;

import java.util.List;

public interface LoginPermissionUtils {

    List<Permission>structureByRoleId(String roleId);

}
