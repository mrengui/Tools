package cn.xisoil.auth.config.token;

import com.alibaba.fastjson.JSON;
import cn.xisoil.auth.data.user.YueLoginUser;
import cn.xisoil.common.enums.HTTPCODE;
import cn.xisoil.common.exception.ResponseException;
import io.jsonwebtoken.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

@Component
public class JwtUtils {

    private static final String signingKey="java666";

    public YueLoginUser getLoginUser(){
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String token=httpServletRequest.getHeader("token");
        if (token!=null){
            JwtParser jwtParser= Jwts.parser(); //获取jwt解析器
            jwtParser.setSigningKey(signingKey);
            try{
                //如果token正确(密码，有效期)则正常运行，否则抛出异常
                Jws<Claims> claimsJws = jwtParser.parseClaimsJws(token);
                Claims body = claimsJws.getBody();//获取body
                String subject = body.getSubject();//获取body中subject中的值
                String key = body.get("key", String.class);//获取Claims中map的值
                YueLoginUser user = JSON.parseObject(key, YueLoginUser.class);//反序列化user
                return user;
            }catch (Exception e){
                throw new ResponseException(HTTPCODE.UAAUTHORIZED);
            }
        }else {
            throw new ResponseException(HTTPCODE.UAAUTHORIZED);
        }
    }


    public String jwt(YueLoginUser user){
        String userJson = JSON.toJSONString(user);//序列化user
        JwtBuilder jwtBuilder = Jwts.builder(); //获得JWT构造器
        Map<String,Object> map=new Hashtable<>();
        map.put("key",userJson);
        String token = jwtBuilder.setSubject(user.getNickname()) //设置用户数据
                .setIssuedAt(new Date()) //设置jwt生成时间
                .setId(user.getId()) //设置id为token id
                .setClaims(map) //通过map传值
                .setExpiration(new Date(System.currentTimeMillis() + 7200000)) //设置token有效期
                .signWith(SignatureAlgorithm.HS256, signingKey) //设置token加密方式和密码
                .compact(); //生成token字符串
        return token;
    }

}
