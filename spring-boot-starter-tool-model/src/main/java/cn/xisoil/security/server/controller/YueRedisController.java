package cn.xisoil.security.server.controller;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.datacheck.PermissionCheckAutomation;
import cn.xisoil.security.server.data.YueRedisInfo;
import cn.xisoil.security.server.interfaces.RedisDirKeyUtils;
import cn.xisoil.security.server.data.YueRedisDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
@RequestMapping("/manage/server")
public class YueRedisController implements PermissionCheckAutomation {

    @Autowired
    private RedisTemplate<String, String> redisCacheTemplate;
    @Autowired
    private RedisDirKeyUtils redisDirKeyUtils;

    @GetMapping("/redis/dir")
    public YueResult<List<YueRedisDir>> redis(){
        return YueResult.<List<YueRedisDir>>builder().data(redisDirKeyUtils.getRedisDirs()).build();
    }

    @GetMapping("/redis/dir/{dir}")
    public YueResult<List<Map<String, String>>> key(@PathVariable String dir){
        Set<String> keys = redisCacheTemplate.keys(dir+"*");
        List<Map<String, String>> mapList=new ArrayList<>();
        keys.forEach(key->{
            mapList.add(Map.of("value",key));
        });
        return YueResult.<List<Map<String, String>>>builder().data(mapList).build();
    }

    @GetMapping("/redis/value")
    public YueResult<YueRedisInfo> value(@RequestParam String key){
        String value = redisCacheTemplate.opsForValue().get(key);
        YueRedisInfo yueRedisInfo=new YueRedisInfo();
        yueRedisInfo.setValue(value);
        yueRedisInfo.setKey(key);
        yueRedisInfo.setExpireTime(redisCacheTemplate.getExpire(key));
        return YueResult.<YueRedisInfo>builder().data(yueRedisInfo).build();
    }

    @DeleteMapping("/redis/dir/{dir}")
    public YueResult<Long> delDir(@PathVariable String dir){
        Set<String> keys = redisCacheTemplate.keys(dir+":*");
        Long result = redisCacheTemplate.delete(keys);
        return YueResult.<Long>builder().message("删除成功").data(result).build();
    }


    @DeleteMapping("/redis/value")
    public YueResult<Long> delValue(@RequestParam String key){
       redisCacheTemplate.delete(key);
        return YueResult.<Long>builder().message("删除成功").build();
    }


    @Override
    public String getUrl() {
        return "/server/redis";
    }

    @Override
    public String getName() {
        return "缓存服务";
    }

    @Override
    public String getParent() {
        return "服务管理";
    }

}
