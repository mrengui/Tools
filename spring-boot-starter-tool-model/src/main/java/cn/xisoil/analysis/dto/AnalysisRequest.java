package cn.xisoil.analysis.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;


@Data
public class AnalysisRequest {

    private Integer count;

    private List<Map<String, Object>> list;

}
