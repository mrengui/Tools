package cn.xisoil.log.service.impl;

import cn.xisoil.log.dao.LoginLogRepository;
import cn.xisoil.log.data.YueLoginLogData;
import cn.xisoil.log.service.LoginLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginLogServiceImpl implements LoginLogService {

    @Autowired
    private LoginLogRepository loginLogRepository;

    @Override
    public void save(YueLoginLogData yueLoginLog) {
        loginLogRepository.save(yueLoginLog);
    }
}
