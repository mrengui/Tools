package cn.xisoil.auth.service.user;

import cn.xisoil.auth.data.user.YueLoginUser;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.common.to.SearchPageRequest;
import org.springframework.data.domain.Page;

public interface UserService {

    YueResult<Page<YueLoginUser>>page(SearchPageRequest searchPageRequest);

    YueResult<String>edit(YueLoginUser request);

    YueResult<String>add(YueLoginUser request);

}
