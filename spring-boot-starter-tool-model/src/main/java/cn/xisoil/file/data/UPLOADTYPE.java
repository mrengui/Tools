package cn.xisoil.file.data;

public enum UPLOADTYPE {


    ALYOSS("ALYOSS"), LOCAL("LOCAL"),HWOBS("HWOBS"),MINIO("MINIO");

    private String type;

    private UPLOADTYPE(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
