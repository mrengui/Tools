package cn.xisoil.auth.controller.user;

import cn.xisoil.auth.dao.user.UserRepository;
import cn.xisoil.auth.data.user.YueLoginUser;
import cn.xisoil.auth.service.user.UserService;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.curd.model.controller.ModelCurdControllerMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/manage/user")
public class CurdUserController extends ModelCurdControllerMapping<YueLoginUser, UserRepository>  {

    @Autowired
    private UserService userService;

    @PutMapping("")
    @Primary
    public YueResult<String> edit(@Valid @RequestBody YueLoginUser request, BindingResult result){
        if (result.hasErrors()){
            for (ObjectError error : result.getAllErrors()) {
                return YueResult.<String>builder().fail().code(500).message(error.getDefaultMessage()).build();
            }
        }
        return userService.edit(request);
    }

    @PostMapping("")
    @Primary
    protected  YueResult<String>add(@Valid @RequestBody YueLoginUser request,BindingResult result){
        if (result.hasErrors()){
            for (ObjectError error : result.getAllErrors()) {
                return YueResult.<String>builder().fail().code(500).message(error.getDefaultMessage()).build();
            }
        }
        return userService.add(request);
    }


    @Override
    public String getUrl() {
        return "/user";
    }

    @Override
    public String getName() {
        return "用户列表";
    }

    @Override
    public String getParent() {
        return "用户管理";
    }
}
