package cn.xisoil.auth.dao.role;

import cn.xisoil.auth.data.role.YueRole;
import cn.xisoil.curd.dao.YueRepository;

public interface YueRoleRepository extends YueRepository<YueRole,String> {
}
