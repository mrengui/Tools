package cn.xisoil.auth.utils;

import cn.xisoil.auth.interfaces.AutoPermissionIntercept;
import cn.xisoil.common.enums.HTTPCODE;
import cn.xisoil.common.exception.ResponseException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * @Description 权限拦截
 * @Author Vien
 * @CreateTime 2023-02-2023/2/11 12:53:03
 **/
@Component
@Aspect
public class AuthPermissionInterceptAsp {

    @Before(value = "@annotation(autoPermissionIntercept)")
    public void asp(JoinPoint point, AutoPermissionIntercept autoPermissionIntercept){

        throw new ResponseException(HTTPCODE.UAAUTHORIZED);
//        throw new ResponseException(100,"aaaa");

    }


}