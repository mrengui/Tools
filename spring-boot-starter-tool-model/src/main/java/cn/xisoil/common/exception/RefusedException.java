package cn.xisoil.common.exception;

public class RefusedException extends RuntimeException{

    public RefusedException(){
        super();
    }

    public static Integer code=500;

    public Integer getCode() {
        return code;
    }


    public RefusedException(String message){
        super(message);
    }



}
