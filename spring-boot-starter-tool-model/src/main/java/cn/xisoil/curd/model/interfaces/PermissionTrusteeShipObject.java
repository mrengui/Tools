package cn.xisoil.curd.model.interfaces;

import java.lang.annotation.Documented;

@Documented
public @interface PermissionTrusteeShipObject {


    String key();

    boolean auto() default true;

    //持久层类目
    String parent() default "";


}
