package cn.xisoil.system.model.data;

import lombok.Data;

@Data
public class YueMemoryInfo {

    private String memorySize;//内存大小

    private String memoryFree;//空闲内存

    private String memoryUse;//使用内存

    private String memoryUsage;//内存使用率

    private String jvmMemorySize;//jvm内存大小

    private String jvmMemoryFree;//jvm空闲内存

    private String jvmMemoryUse;//jvm使用内存

    private String jvmMemoryUsage;//jvm内存使用率


}
