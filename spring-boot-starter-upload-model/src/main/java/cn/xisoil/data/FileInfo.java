package cn.xisoil.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

/**
 * @Description TODO
 * @Author Vien
 * @CreateTime 2023-03-2023/3/9 17:12:31
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FileInfo{

    public String name;

    public File file;

    public InputStream inputStream;

    public String content;

    public byte[] bytes;

    public ByteArrayInputStream byteArrayInputStream;

}
