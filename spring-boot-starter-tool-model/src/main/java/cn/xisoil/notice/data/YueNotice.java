package cn.xisoil.notice.data;


import cn.xisoil.common.to.StringConverters;
import cn.xisoil.curd.model.enums.ObjectColumnType;
import cn.xisoil.curd.model.interfaces.CurdModel;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity(name = "YueNotice")
@Table(name = "Yue_notice")
@EntityListeners(AuditingEntityListener.class)
@CurdModelObject(value = "通知管理")
public class YueNotice {


    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    @CurdModel(value = "通知对象",type = ObjectColumnType.CHECKBOX,url = "/role/list")
    @Convert(converter = StringConverters.class)
    private List<String> roleIds;

    @CurdModel(value = "添加时间",editor = false,comparable = true)
    @CreatedDate
    private Date createTime;

    @CurdModel(value = "通知标题")
    private String title;

    @CurdModel(value = "通知内容",type = ObjectColumnType.RICHTEXT)
    private String content;

}
