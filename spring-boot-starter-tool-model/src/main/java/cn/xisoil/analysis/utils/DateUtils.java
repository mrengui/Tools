package cn.xisoil.analysis.utils;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class DateUtils {



    public List<String> getDateList(){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        LocalDate endDate = LocalDate.now().plusDays(1);
        LocalDate endDate = LocalDate.now();
        LocalDate startDate = endDate.minusDays(7);
        List<String>times=new ArrayList<>();
        List<LocalDate> listOfDates = startDate.datesUntil(endDate).toList();
        listOfDates.forEach(item->{
            times.add(dateTimeFormatter.format(item));
        });
        return times;
    }

    public List<String>getDateList(String startTime,String endTime) throws ParseException {

        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        LocalDate endDate =timeToLocal(simpleDateFormat.parse(endTime)).plusDays(1);
        LocalDate startDate = timeToLocal(simpleDateFormat.parse(startTime));

        List<String>times=new ArrayList<>();
        List<LocalDate> listOfDates = startDate.datesUntil(endDate).toList();
        listOfDates.forEach(item->{
            times.add(dateTimeFormatter.format(item));
        });
        return times;
    }

    public LocalDate timeToLocal(Date date){
        Instant instant =date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        return localDateTime.toLocalDate();
    }


    public static void main(String[] args) throws ParseException {
        final BigDecimal[] shouyi={new BigDecimal(0)};
        final BigDecimal[] price = {new BigDecimal(258000)};
        List<String>dates=new DateUtils().getDateList("2022-11-17","2022-12-30");
        dates.forEach(date->{
            price[0] = price[0].multiply(new BigDecimal(1.04));
            BigDecimal nowSy=price[0].multiply(new BigDecimal(0.005));
            shouyi[0]=shouyi[0].add(nowSy);
            System.out.println("时间："+date+" 总金额："+String.format("%.2f", price[0])+ "当日收益："+String.format("%.2f", nowSy)+" 总收益："+String.format("%.2f",shouyi[0]));
        });
    }

}
