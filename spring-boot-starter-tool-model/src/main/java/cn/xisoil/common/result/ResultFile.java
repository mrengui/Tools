package cn.xisoil.common.result;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultFile<T> {

    private final String result;

    private final Integer code;

    private final String message;

    private final String param;

    private final T data;

    private ResultFile(Builder<T> builder) {
        this.result = builder.result;
        this.code = builder.code;
        this.message = builder.message;
        this.data = builder.data;
        this.param = builder.param;
    }

    public static<T> Builder<T> builder() {
        return new Builder<>();
    }

    public static class Builder<T> {
        private String result;

        private Integer code;

        private String message;

        private String param;

        private T data;

        public Builder<T> code(Integer code) {
            this.code = code;
            return this;
        }

        public Builder<T>  message(String message) {
            this.message = message;
            return this;
        }

        public Builder<T>  result(String result) {
            this.result = result;
            return this;
        }

        public Builder<T>  param(String param) {
            this.param = param;
            return this;
        }

        public Builder<T>  success() {
            this.result = "SUCCESS";
            this.code = 200;
            return this;
        }

        public Builder<T>  fail() {
            this.result = "FAILURE";
            this.code = 500;
            return this;
        }

        public Builder<T>  data(T data) {
            this.data = data;
            return this;
        }

        public ResultFile<T> build() {
            return new ResultFile<>(this);
        }
    }

    @JsonIgnore
    public Boolean isFailed(){
        return this.code==500;
    }
}
