package cn.xisoil.curd.model.interfaces;

import cn.xisoil.curd.model.enums.ObjectType;

import java.lang.annotation.Documented;

@Documented
public @interface CurdTrusteeShipObject {

    boolean auto() default true;

    //控制层mapping
    String router() default "";

    //类型
    ObjectType type() default  ObjectType.LIST;

}
