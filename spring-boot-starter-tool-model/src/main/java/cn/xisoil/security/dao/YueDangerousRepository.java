package cn.xisoil.security.dao;

import cn.xisoil.curd.dao.YueRepository;
import cn.xisoil.security.data.YueDangerous;

public interface YueDangerousRepository extends YueRepository<YueDangerous,String> {
}
