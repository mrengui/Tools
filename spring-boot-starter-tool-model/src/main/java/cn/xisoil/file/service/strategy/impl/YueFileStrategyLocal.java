package cn.xisoil.file.service.strategy.impl;


import cn.xisoil.common.exception.NormalException;
import cn.xisoil.common.result.ResultFile;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.file.dao.FileRepository;
import cn.xisoil.file.data.UPLOADTYPE;
import cn.xisoil.file.data.YueFile;
import cn.xisoil.file.service.strategy.YueFileStrategy;
import cn.xisoil.file.service.manage.YueFileManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import jakarta.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service(value = "LOCAL")
public class YueFileStrategyLocal implements YueFileStrategy {


    @Autowired
    private YueFileManageService yueFileManageService;
    @Autowired
    private FileRepository fileRepository;

    @Override
    public ResultFile<String> upload(HttpServletRequest request) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateNowStr = sdf.format(new Date());
        String fileDir="resources"+ File.separator+"upload"+File.separator+dateNowStr;
        File dir=new File(fileDir);
        if (!dir.exists()){
            dir.mkdirs();
        }
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile=multipartRequest.getFile("file");
        String fileName= multipartFile.getOriginalFilename();
        String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
        String filename=dateNowStr+String.valueOf((int)(Math.random() * 1000000))+"."+fileExt;//随机文件名
        String url=fileDir+File.separator+filename;
        File file = new File(url);
        try{
            multipartFile.transferTo(file.getAbsoluteFile());
        }
        catch (Exception e){
            throw new NormalException("上传失败，未知错误");
        }
        YueFile yueFile=new YueFile();
        yueFile.setFilename(fileName);
        yueFile.setUploadtype(UPLOADTYPE.LOCAL);
        yueFile.setUrl(File.separator+"upload"+File.separator+dateNowStr+File.separator+filename);
        yueFileManageService.save(yueFile);
        return ResultFile.<String>builder()
                .data(File.separator+"upload"+File.separator+dateNowStr+File.separator+filename)
                .param(request.getHeader("key"))
                .success().build();
    }

    @Override
    @Transactional
    public YueResult<String> delete(YueFile file) {
        File dir=new File("resources/"+file.getUrl());
        if (dir.exists()){
            dir.delete();
        }
        return YueResult.<String>builder().success().message("删除成功").build();
    }

}
