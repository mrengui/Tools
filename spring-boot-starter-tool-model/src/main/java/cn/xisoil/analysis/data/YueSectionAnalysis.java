package cn.xisoil.analysis.data;

import cn.xisoil.curd.model.interfaces.CurdModelObject;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.*;

@Data
@Entity(name = "YueSectionAnalysis")
@Table(name = "yue_section_analysis")
@EntityListeners(AuditingEntityListener.class)
@CurdModelObject(value = "栏目访问日志")
public class YueSectionAnalysis {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    private String date;

    private String sectionName;

    private Integer pvCount=0;

    private Integer uvCount=0;

}
