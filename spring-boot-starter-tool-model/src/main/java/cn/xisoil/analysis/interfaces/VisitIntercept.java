package cn.xisoil.analysis.interfaces;

import cn.xisoil.analysis.sercvice.YueAnalysisInterface;
import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface VisitIntercept {

    String key() default "";

    int args() default -1;

    Class<? extends YueAnalysisInterface> clazz() default YueAnalysisInterface.class;

}
