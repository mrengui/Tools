package cn.xisoil.auth.dto.login;

import cn.xisoil.auth.data.permission.Permission;
import cn.xisoil.auth.data.user.YueLoginUser;
import lombok.Data;

import java.util.List;


@Data
public class LoginDto {

    private String token;

    private YueLoginUser user;

    private List<Permission> permissions;


}
