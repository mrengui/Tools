package cn.xisoil.analysis.dao;

import cn.xisoil.analysis.data.YueSectionAnalysis;
import cn.xisoil.curd.dao.YueRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface YueSectionAnalysisRepository extends YueRepository<YueSectionAnalysis,String> {

    Optional<YueSectionAnalysis>findTopByDateAndSectionName(String time,String sectionName);

    @Query(value = "select new map(sectionAnalysis.sectionName as sectionName,sum(sectionAnalysis.uvCount) as uvCount) " +
            " from YueSectionAnalysis sectionAnalysis group by sectionAnalysis.sectionName order by sum(sectionAnalysis.uvCount) desc")
    List<Map<String,Object>>findTop5SumBySectionName();


}
