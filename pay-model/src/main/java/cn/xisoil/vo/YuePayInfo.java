package cn.xisoil.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class YuePayInfo {

    private String description;

    private String notify_url;

    private String orderNum;

    private Integer total;

}
