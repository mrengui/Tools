package cn.xisoil.manage.controller;


import cn.xisoil.common.result.YueResult;
import cn.xisoil.manage.utils.YueIcoExtract;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/manage/ico")
public class IconController {

    @GetMapping("/list")
    public YueResult<List<Map<String,String>>>getIcos(){
        return YueResult.<List<Map<String, String>>>builder().data(YueIcoExtract.icos).success().build();
    }

}
