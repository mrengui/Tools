package cn.xisoil.security.dao;

import cn.xisoil.curd.dao.YueRepository;
import cn.xisoil.security.data.YueSecurityBasic;

import java.util.Optional;

public interface YueSecurityBasicRepository extends YueRepository<YueSecurityBasic,String> {

    Optional<YueSecurityBasic>findTopByIdIsNotNull();

}
