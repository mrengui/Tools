package cn.xisoil.notice.service.impl;

import cn.xisoil.auth.data.user.YueLoginUser;
import cn.xisoil.auth.config.token.JwtUtils;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.notice.dao.YueNoticeRepository;
import cn.xisoil.notice.data.YueNotice;
import cn.xisoil.notice.service.YueNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class YueNoticeServiceImpl implements YueNoticeService {

    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private YueNoticeRepository yueNoticeRepository;

    @Override
    public YueResult<List<YueNotice>> getNotice() {
        YueLoginUser yueLoginUser=jwtUtils.getLoginUser();
        List<YueNotice>list=yueNoticeRepository.findAllByRoleIdsIsIn(yueLoginUser.getRoleId());
        return YueResult.<List<YueNotice>>builder().data(list).build();
    }
}
