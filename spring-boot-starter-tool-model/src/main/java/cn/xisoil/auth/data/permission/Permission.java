package cn.xisoil.auth.data.permission;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cn.xisoil.curd.model.enums.ObjectColumnType;
import cn.xisoil.curd.model.interfaces.CurdModel;
import cn.xisoil.curd.model.interfaces.CurdModelObject;
import lombok.Data;
import org.hibernate.annotations.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.*;
import jakarta.persistence.Entity;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity(name = "Permission")
@Table(name = "permission")
@EntityListeners(AuditingEntityListener.class)
@CurdModelObject(value = "权限管理")
@FilterDef(name = "permissionChildren", parameters = {@ParamDef(name="ids",type=String.class)})
public class Permission {

    public Permission(){

    }
    public Permission(String title,String path,String ico){
        this.title=title;
        this.name=title;
        this.path=path;
        this.ico=ico;
    }

    public Permission(String title,String path){
        this.title=title;
        this.name=title;
        this.path=path;
    }

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    @CurdModel(value = "所属父级",show = false,search = true,url = "/permission/list",type = ObjectColumnType.LIST)
    @Column(columnDefinition = "varchar(32)")
    private String parentId;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "parentId",columnDefinition = "varchar(32)")
    @JsonIgnoreProperties("children")
    @Fetch(FetchMode.SUBSELECT)
    @Filter(name = "permissionChildren", condition = " id in (:ids)   ")
    @OrderBy("comparable asc ")
    private Set<Permission> children=new HashSet<>();

    @CurdModel(value = "权限名称")
    private String name;

    private Date createTime;

    @Column(name = "`key`")
    private String key;

    @CurdModel(value = "排序",comparable = true,type = ObjectColumnType.NUMBER)
    private Integer comparable;

    private String title;

    public String getTitle() {
        return name;
    }

    @CurdModel(value = "权限地址")
    @Column(unique = true)
    private String path;

    @CurdModel(value = "权限图标",url = "/ico/list",type = ObjectColumnType.LIST)
    private String ico;

}
