package cn.xisoil.log.dao;

import cn.xisoil.curd.dao.YueRepository;
import cn.xisoil.log.data.YueSystemLogData;

public interface YueSystemLogDataRepository extends YueRepository<YueSystemLogData,String> {
}
