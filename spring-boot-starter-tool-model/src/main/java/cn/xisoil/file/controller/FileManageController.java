package cn.xisoil.file.controller;

import cn.xisoil.common.result.YueResult;
import cn.xisoil.common.to.ListStringRequest;
import cn.xisoil.curd.model.controller.ModelCurdControllerMapping;
import cn.xisoil.file.dao.FileRepository;
import cn.xisoil.file.data.YueFile;
import cn.xisoil.file.service.manage.YueFileManageService;
import cn.xisoil.log.interfaces.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RequestMapping("/manage/file")
@RestController
public class FileManageController extends ModelCurdControllerMapping<YueFile, FileRepository>  {

    @Autowired
    private YueFileManageService yueFileManageService;

    @GetMapping("/type")
    @Log("获取文件分类")
    public YueResult<List<Map<String,String>>>type(){
        List<Map<String,String>>list=new ArrayList<>();
        list.add(Map.of("id","IMAGE","title","图片"));
        list.add(Map.of("id","DOC","title","文档"));
        list.add(Map.of("id","RAR","title","压缩包"));
        list.add(Map.of("id","VIDEO","title","视频"));
        list.add(Map.of("id","AUDIO","title","音频"));
        list.add(Map.of("id","PROGRAM","title","执行文件"));
        list.add(Map.of("id","OTHER","title","其他文件"));
        return YueResult.<List<Map<String, String>>>builder().data(list).success().build();
    }


    @PostMapping("/delete")
    @Primary
    @Log("删除文件")
    protected YueResult<String> delete(@Valid @RequestBody ListStringRequest stringRequest, BindingResult result){
        if (result.hasErrors()){
            for (ObjectError error : result.getAllErrors()) {
                return YueResult.<String>builder().fail().code(500).message(error.getDefaultMessage()).build();
            }
        }
        return  yueFileManageService.delete(stringRequest);
    }

    @Override
    public String getUrl() {
        return "/file";
    }

    @Override
    public String getName() {
        return "媒体服务";
    }

    @Override
    public String getParent() {
        return "文件管理";
    }
}
