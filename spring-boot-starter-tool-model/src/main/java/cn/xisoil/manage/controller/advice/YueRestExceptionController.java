package cn.xisoil.manage.controller.advice;

import cn.xisoil.common.exception.AuthException;
import cn.xisoil.common.exception.RefusedException;
import cn.xisoil.common.exception.NormalException;
import cn.xisoil.common.exception.ResponseException;
import cn.xisoil.common.result.ResultMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import jakarta.servlet.http.HttpServletRequest;

@RestControllerAdvice
@Slf4j
public class YueRestExceptionController {
    private final ResultMap resultMap;

    @Autowired
    public YueRestExceptionController(ResultMap resultMap) {
        this.resultMap = resultMap;
    }


    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResultMap jsonParseError(HttpServletRequest request, Throwable ex) {
        return resultMap.fail().code(getStatus(request).value()).message(ex.getMessage());
    }

    @ExceptionHandler(NormalException.class)
    public ResultMap NormalError(HttpServletRequest request, Throwable ex) {
        return resultMap.fail().code(getStatus(request).value()).message(ex.getMessage());
    }

    @ExceptionHandler(AuthException.class)
    public ResultMap AuthError(HttpServletRequest request, Throwable ex) {
        return resultMap.fail().code(403).message(ex.getMessage());
    }


    @ExceptionHandler(ResponseException.class)
    public ResultMap ResponseError(HttpServletRequest request, Throwable ex) {
        return resultMap.fail().code(getStatus(request).value()).message(ex.getMessage());
    }

    private HttpStatus getStatus(HttpServletRequest request) {
        Integer statusCode = (Integer) request.getAttribute("jakarta.servlet.error.status_code");
        if (statusCode == null) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.valueOf(statusCode);
    }


}
