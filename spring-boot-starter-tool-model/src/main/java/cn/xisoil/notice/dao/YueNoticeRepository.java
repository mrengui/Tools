package cn.xisoil.notice.dao;

import cn.xisoil.curd.dao.YueRepository;
import cn.xisoil.notice.data.YueNotice;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface YueNoticeRepository extends YueRepository<YueNotice,String> {


    @Query(value = "select notice from YueNotice notice where find_in_set(?1,notice.roleIds)>0")
    List<YueNotice>findAllByRoleIdsIsIn(String roleIds);


}
