package cn.xisoil.manage.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class YueIcoExtract {

    public  static  List<Map<String,String>>icos=new ArrayList<>();

    static {
        icos.add(Map.of("id","location-o","title","定位"));
        icos.add(Map.of("id","star-o","title","五角星"));
        icos.add(Map.of("id","phone-o","title","电话"));
        icos.add(Map.of("id","setting-o","title","设置"));
        icos.add(Map.of("id","fire-o","title","火苗"));
        icos.add(Map.of("id","coupon-o","title","优惠券"));
        icos.add(Map.of("id","cart-o","title","购物车"));
        icos.add(Map.of("id","shopping-cart-o","title","购物车"));
        icos.add(Map.of("id","friends-o","title","多用户"));
        icos.add(Map.of("id","service-o","title","服务"));
        icos.add(Map.of("id","send-gift-o","title","礼物盒"));
        icos.add(Map.of("id","bag-o","title","包"));
        icos.add(Map.of("id","shop-o","title","商铺"));
        icos.add(Map.of("id","wap-home-o","title","主页"));
        icos.add(Map.of("id","cluster-o","title","集群"));
        icos.add(Map.of("id","label-o","title","书签"));
        icos.add(Map.of("id","manager-o","title","管理员"));
        icos.add(Map.of("id","balance-o","title","余额"));
        icos.add(Map.of("id","user-o","title","单用户"));
        icos.add(Map.of("id","tv-o","title","电视"));
        icos.add(Map.of("id","filter-o","title","筛选"));
        icos.add(Map.of("id","bar-chart-o","title","条形图"));
        icos.add(Map.of("id","chart-trending-o","title","趋势图"));
        icos.add(Map.of("id","calendar-o","title","日历"));
        icos.add(Map.of("id","apps-o","title","更多"));
        icos.add(Map.of("id","qr","title","二维码"));
        icos.add(Map.of("id","contact","title","关于"));
        icos.add(Map.of("id","pending-payment","title","钱包-待支付"));
        icos.add(Map.of("id","notes-o","title","通知"));
        icos.add(Map.of("id","idcard","title","证件"));
        icos.add(Map.of("id","shield-o","title","盾牌"));
        icos.add(Map.of("id","points","title","积分"));
    }


}
