package cn.xisoil.auth.service.permission;

import cn.xisoil.auth.data.permission.Permission;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.common.to.SearchPageRequest;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestBody;

import jakarta.validation.Valid;
import java.util.List;

public interface YuePermissionService {

    YueResult<List<Permission>> list();

    YueResult<Page<Permission>> page(SearchPageRequest searchPageRequest);
}
