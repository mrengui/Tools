package cn.xisoil.analysis.sercvice.impl;

import cn.xisoil.analysis.dao.YueBrowserAnalysisRepository;
import cn.xisoil.analysis.dao.YueDateAnalysisRepository;
import cn.xisoil.analysis.dao.YueEquipmentAnalysisRepository;
import cn.xisoil.analysis.dao.YueSectionAnalysisRepository;
import cn.xisoil.analysis.data.YueDateAnalysis;
import cn.xisoil.analysis.dto.AnalysisRequest;
import cn.xisoil.analysis.dto.StatisticalVo;
import cn.xisoil.analysis.sercvice.YueAnalysisService;
import cn.xisoil.analysis.utils.DateUtils;
import cn.xisoil.common.result.YueResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class YueAnalysisServiceImpl implements YueAnalysisService {

    @Autowired
    private YueSectionAnalysisRepository yueSectionAnalysisRepository;
    @Autowired
    private YueEquipmentAnalysisRepository yueEquipmentAnalysisRepository;
    @Autowired
    private YueBrowserAnalysisRepository yueBrowserAnalysisRepository;
    @Autowired
    private DateUtils dateUtils;
    @Autowired
    private YueDateAnalysisRepository yueDateAnalysisRepository;

    @Override
    public YueResult<AnalysisRequest> getSectionBar() {
        List<Map<String, Object>> mapList=yueSectionAnalysisRepository.findTop5SumBySectionName();
        AnalysisRequest analysisRequest=new AnalysisRequest();
        analysisRequest.setList(mapList);
        AtomicInteger count= new AtomicInteger();
        mapList.forEach(map -> {
            count.set(count.get() + Integer.parseInt(map.get("uvCount").toString()));
        });
        analysisRequest.setCount(count.get());
        return YueResult.<AnalysisRequest>builder().success().data(analysisRequest).build();
    }

    @Override
    public YueResult<AnalysisRequest> equipment() {
        List<Map<String, Object>> mapList=yueEquipmentAnalysisRepository.findTop5SumByEquipment();
        AnalysisRequest analysisRequest=new AnalysisRequest();
        analysisRequest.setList(mapList);
        AtomicInteger count= new AtomicInteger();
        mapList.forEach(map -> {
            count.set(count.get() + Integer.parseInt(map.get("value").toString()));
        });
        analysisRequest.setCount(count.get());
        return YueResult.<AnalysisRequest>builder().success().data(analysisRequest).build();
    }

    @Override
    public YueResult<AnalysisRequest> getBrowser() {
        List<Map<String, Object>> mapList=yueBrowserAnalysisRepository.findTop5SumByBrowser();
        AnalysisRequest analysisRequest=new AnalysisRequest();
        analysisRequest.setList(mapList);
        AtomicInteger count= new AtomicInteger();
        mapList.forEach(map -> {
            count.set(count.get() + Integer.parseInt(map.get("value").toString()));
        });
        analysisRequest.setCount(count.get());
        return YueResult.<AnalysisRequest>builder().success().data(analysisRequest).build();
    }

    @Override
    public YueResult<StatisticalVo> getPVAndUV() {
        StatisticalVo statisticalVo=new StatisticalVo();
        List<String> times=dateUtils.getDateList();
        statisticalVo.setTimes(times);

        Map<String, YueDateAnalysis>yueDateAnalysisList=yueDateAnalysisRepository.findAllByDateIn(times).stream()
                .collect(Collectors.toMap(YueDateAnalysis::getDate,v->v,(v1,v2)->v2));;

        List<Integer>pvCounts=new ArrayList<>();
        List<Integer>uvCounts=new ArrayList<>();

        times.forEach(time->{
            pvCounts.add(yueDateAnalysisList.getOrDefault(time,new YueDateAnalysis()).getPvCount());
            uvCounts.add(yueDateAnalysisList.getOrDefault(time,new YueDateAnalysis()).getUvCount());
        });

        statisticalVo.setPvCount(pvCounts);
        statisticalVo.setUvCount(uvCounts);

        return YueResult.<StatisticalVo>builder().success().data(statisticalVo).build();
    }
}
