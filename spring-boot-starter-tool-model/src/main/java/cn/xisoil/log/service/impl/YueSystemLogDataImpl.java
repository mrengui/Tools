package cn.xisoil.log.service.impl;

import cn.xisoil.log.dao.YueSystemLogDataRepository;
import cn.xisoil.log.data.YueSystemLogData;
import cn.xisoil.log.service.YueSystemLogDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class YueSystemLogDataImpl implements YueSystemLogDataService {

    @Autowired
    private YueSystemLogDataRepository yueSystemLogDataRepository;

    @Override
    public void save(YueSystemLogData yueSystemLogData) {
        yueSystemLogDataRepository.save(yueSystemLogData);
    }
}
