package cn.xisoil.log.dao;

import cn.xisoil.curd.dao.YueRepository;
import cn.xisoil.log.data.YueLoginLogData;

public interface LoginLogRepository extends YueRepository<YueLoginLogData,String> {
}
