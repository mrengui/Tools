package cn.xisoil.security.service.impl;

import cn.xisoil.common.enums.HTTPCODE;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.security.dao.YueSecurityBasicRepository;
import cn.xisoil.security.data.YueSecurityBasic;
import cn.xisoil.security.service.YueSecurityBasicService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.io.Serializable;

import static cn.xisoil.utils.RedisFields.SECURITY_BASIC;

@Service
public class YueSecurityBasicServiceImpl implements YueSecurityBasicService {

    @Autowired
    private YueSecurityBasicRepository yueSecurityBasicRepository;

    @Autowired
    private RedisTemplate<String, Serializable> redisCacheTemplate;

    @Override
    public YueResult<YueSecurityBasic> get() {
        return YueResult.<YueSecurityBasic>builder().result(HTTPCODE.SUCCESS).data(yueSecurityBasicRepository.findTopByIdIsNotNull().orElse(new YueSecurityBasic())).build();
    }

    @Override
    public YueResult<String> edit(YueSecurityBasic edit) {
        YueSecurityBasic basicData=yueSecurityBasicRepository.findTopByIdIsNotNull().orElse(new YueSecurityBasic());
        BeanUtils.copyProperties(edit,basicData);
        yueSecurityBasicRepository.save(basicData);
        redisCacheTemplate.opsForValue().set(SECURITY_BASIC,basicData);
        return YueResult.<String>builder().result(HTTPCODE.SUCCESS).build();
    }


}
