package cn.xisoil.manage.controller;

import cn.xisoil.analysis.interfaces.VisitIntercept;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("")
public class YueAdminController {

    @GetMapping("/admin")
    public String index(){
        return "dist/index";
    }

}
