package cn.xisoil.manage.controller.advice;

import cn.xisoil.common.exception.AuthException;
import cn.xisoil.common.exception.NormalException;
import cn.xisoil.common.exception.RefusedException;
import cn.xisoil.common.exception.ResponseException;
import cn.xisoil.common.result.ResultMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import jakarta.servlet.http.HttpServletRequest;

@ControllerAdvice
@Slf4j
public class YueExceptionController {

    @ExceptionHandler(RefusedException.class)
    public String IpRefused(HttpServletRequest request, Throwable ex, Model model) {
        model.addAttribute("message",ex.getMessage());
        return "dist/error/ipError.html";//封装异常信息
    }

}
