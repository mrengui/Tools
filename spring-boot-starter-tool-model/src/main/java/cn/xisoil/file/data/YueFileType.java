package cn.xisoil.file.data;

public enum YueFileType {

    IMAGE("IMAGE"), DOC("DOC"), RAR("RAR"),
    PROGRAM("PROGRAM"), VIDEO("VIDEO"), AUDIO("AUDIO"), OTHER("OTHER");

    private final String name;

    public String getName() {
        return name;
    }

    YueFileType(String name) {
        this.name = name;
    }

}
