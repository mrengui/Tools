package cn.xisoil.webmaster.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import cn.xisoil.common.exception.NormalException;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.task.utils.TaskTools;
import cn.xisoil.webmaster.dao.YueSeoConfigRepository;
import cn.xisoil.webmaster.data.YueSeoConfig;
import cn.xisoil.webmaster.service.YueSeoConfigService;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class YueSeoConfigServiceImpl implements YueSeoConfigService {

    @Autowired
    private TaskTools taskTools;
    @Autowired
    private YueSeoConfigRepository yueSeoConfigRepository;

    @Override
    public YueResult<YueSeoConfig> getSeoConfig() {
        YueSeoConfig yueSeoConfig=yueSeoConfigRepository.findTopByIdIsNotNull().orElse(new YueSeoConfig());
        return YueResult.<YueSeoConfig>builder().success().data(yueSeoConfig).build();
    }

    @Override
    public YueResult<String> setSeoConfig(YueSeoConfig request) {
        YueSeoConfig yueSeoConfig=yueSeoConfigRepository.findTopByIdIsNotNull().orElse(new YueSeoConfig());
        BeanUtils.copyProperties(request,yueSeoConfig);
        yueSeoConfigRepository.save(yueSeoConfig);
        if (StringUtils.isNotBlank(request.getCron())){
            taskTools.refreshCron("站长推送",request.getCron());
        }
        return YueResult.<String>builder().success().message("保存成功").build();
    }

    @Override
    public YueResult<String> pushBaidu(String siteUrl) {
        YueSeoConfig yueSeoConfig=yueSeoConfigRepository.findTopByIdIsNotNull().orElse(new YueSeoConfig());
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/plain");
        RequestBody body = RequestBody.create(mediaType, siteUrl);
        Request request = new Request.Builder()
                .url("http://data.zz.baidu.com/urls?site="+yueSeoConfig.getDomain()+"&token="+yueSeoConfig.getBaiduKey())
                .method("POST", body)
                .addHeader("Content-Type", "text/plain")
                .build();
        try{
            Response response = client.newCall(request).execute();
            JSONObject jsonObject= JSON.parseObject(response.body().string());
            if (response.code()==200){
                return YueResult.<String>builder().success().message("成功推送"+jsonObject.getString("success")+"条记录").build();
            }
            else {
                throw new NormalException(jsonObject.getString("message"));
            }
        }catch (Exception  e){
            throw new NormalException(e.getMessage());
        }
    }
}
