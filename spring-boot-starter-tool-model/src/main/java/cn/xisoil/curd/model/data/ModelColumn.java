package cn.xisoil.curd.model.data;

import cn.xisoil.curd.model.enums.ObjectColumnType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ModelColumn {

    //字段名
    private String column;
    //展示名称
    private String name;
    //字段类型
    private ObjectColumnType type;
    //是否可编辑
    private Boolean edit;
    //是否展示
    private Boolean show;
    //是否用户搜索
    private Boolean search;
    //是否可排序
    private Boolean comparable=false;
    //是否必填
    private Boolean required=false;
    //外键关联时，下拉选择框列表来源api（checkbox,radio,list需填写该字段）
    private String url;
    //输入提示
    private String placeholder;
    //分组名称
    private String group;
}
