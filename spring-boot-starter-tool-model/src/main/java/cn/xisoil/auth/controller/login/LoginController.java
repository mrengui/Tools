package cn.xisoil.auth.controller.login;


import cn.xisoil.auth.dao.user.UserRepository;
import cn.xisoil.auth.data.user.YueLoginUser;
import cn.xisoil.auth.dto.login.EditUserRequest;
import cn.xisoil.auth.dto.login.LoginDto;
import cn.xisoil.auth.dto.login.LoginRequest;
import cn.xisoil.auth.service.login.LoginService;
import cn.xisoil.common.exception.ResponseException;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.curd.model.controller.SingleModelController;
import cn.xisoil.log.interfaces.YueLoginLog;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping(value = "/manage/auth")
public class LoginController extends SingleModelController<YueLoginUser, UserRepository> {

    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    @YueLoginLog
    public YueResult<LoginDto> userLogin(@Valid @RequestBody LoginRequest request, BindingResult result){
        if (result.hasErrors()){
            for (ObjectError error : result.getAllErrors()) {
                throw new  ResponseException(500,error.getDefaultMessage());
            }
        }
        return loginService.userLogin(request);
    }

    @GetMapping({"","/userInfo"})
    @Primary
    public YueResult<YueLoginUser> get(){
        return loginService.getUserInfo();
    }


    @PutMapping("")
    @Primary
    public YueResult<String> edit(@Valid @RequestBody YueLoginUser request, BindingResult result){
        EditUserRequest user=new EditUserRequest();
        BeanUtils.copyProperties(request,user);
        return loginService.edit(user);
    }


    @PostMapping("/register")
    public YueResult<String> register(@Valid @RequestBody LoginRequest user, BindingResult result){
        return loginService.register(user);
    }




}
