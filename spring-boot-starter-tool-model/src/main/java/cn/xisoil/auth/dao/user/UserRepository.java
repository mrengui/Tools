package cn.xisoil.auth.dao.user;

import cn.xisoil.auth.data.user.YueLoginUser;
import cn.xisoil.curd.dao.YueRepository;

import java.util.Optional;

public interface UserRepository extends YueRepository<YueLoginUser,String> {

    Optional<YueLoginUser>findOneByAccount(String account);

    Boolean existsAllByAccount(String account);

}
