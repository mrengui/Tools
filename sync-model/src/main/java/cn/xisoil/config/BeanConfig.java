package cn.xisoil.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.erhlc.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@Configuration
@EnableJpaRepositories(basePackages = "cn.xisoil.*",repositoryFactoryBeanClass = CustomRepositoryFactoryBean.class)
public class BeanConfig {

    @Autowired
    private RestHighLevelClient restHighLevelClient;
    public static ElasticsearchRestTemplate elasticsearchRestTemplate;


    @Bean
    public ElasticsearchRestTemplate restTemplate() throws Exception {
        elasticsearchRestTemplate=new ElasticsearchRestTemplate(restHighLevelClient);
        return elasticsearchRestTemplate;
    }

    @Bean
    public ElasticsearchOperations elasticsearchTemplate() {
        return new ElasticsearchRestTemplate(restHighLevelClient);
    }


}
