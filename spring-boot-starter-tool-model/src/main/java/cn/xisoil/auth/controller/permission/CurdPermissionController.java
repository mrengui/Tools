package cn.xisoil.auth.controller.permission;

import cn.xisoil.auth.dao.permission.PermissionRepository;
import cn.xisoil.auth.data.permission.Permission;
import cn.xisoil.auth.service.permission.YuePermissionService;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.common.to.SearchPageRequest;
import cn.xisoil.curd.model.controller.ModelCurdControllerMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/manage/permission")
public class CurdPermissionController extends ModelCurdControllerMapping<Permission, PermissionRepository> {

    @Autowired
    private YuePermissionService yuePermissionService;

    @GetMapping("/list")
    @Primary
    public YueResult<List<Permission>> list() {
        return yuePermissionService.list();
    }

    @PostMapping("/page")
    @Primary
    public YueResult<Page<Permission>> page(@Valid @RequestBody SearchPageRequest searchPageRequest, BindingResult result) {
        if (result.hasErrors()){
            for (ObjectError error : result.getAllErrors()) {
                return YueResult.<Page<Permission>>builder().fail().code(500).message(error.getDefaultMessage()).build();
            }
        }
        return yuePermissionService.page(searchPageRequest);
    }


    @Override
    public String getUrl() {
        return "/permission";
    }

    @Override
    public String getName() {
        return "权限管理";
    }

    @Override
    public String getParent() {
        return "用户管理";
    }
}
