package cn.xisoil.analysis.sercvice;

import cn.xisoil.analysis.dto.AnalysisRequest;
import cn.xisoil.analysis.dto.StatisticalVo;
import cn.xisoil.common.result.YueResult;

import java.util.List;
import java.util.Map;

public interface YueAnalysisService {

    YueResult<AnalysisRequest> getSectionBar();

    YueResult<AnalysisRequest>equipment();

    YueResult<AnalysisRequest>getBrowser();

    YueResult<StatisticalVo>getPVAndUV();

}
