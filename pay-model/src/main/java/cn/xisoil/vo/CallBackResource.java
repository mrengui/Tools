package cn.xisoil.vo;

import lombok.Data;

@Data
public class CallBackResource {

        private String original_type;

        private String algorithm;

        private String ciphertext;

        private String associated_data;

        private String nonce;

}
