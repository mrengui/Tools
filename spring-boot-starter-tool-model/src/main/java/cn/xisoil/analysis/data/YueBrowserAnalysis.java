package cn.xisoil.analysis.data;

import cn.xisoil.curd.model.interfaces.CurdModelObject;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;



@Data
@Entity(name = "YueBrowserAnalysis")
@Table(name = "yue_browser_analysis")
@EntityListeners(AuditingEntityListener.class)
@CurdModelObject(value = "浏览器访问日志")
public class YueBrowserAnalysis {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(32)")
    private String id;

    @Enumerated(value = EnumType.STRING)
    private AnalysisBrowser browser;

    private String date;

    private Integer pvCount=0;

    private Integer uvCount=0;


}
