package cn.xisoil.curd.model.controller;

import cn.xisoil.common.to.ListStringRequest;
import cn.xisoil.common.to.SearchPageRequest;
import cn.xisoil.common.result.YueResult;
import cn.xisoil.curd.model.data.ModelClass;
import cn.xisoil.curd.model.data.ModelColumn;
import cn.xisoil.curd.model.enums.ObjectType;
import cn.xisoil.curd.model.service.list.CurdDaoServiceImpl;
import cn.xisoil.curd.model.service.model.ModelService;
import cn.xisoil.curd.dao.YueRepository;
import cn.xisoil.datacheck.PermissionCheckAutomation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public abstract class ModelCurdControllerMapping<T,R extends YueRepository<T,String>> extends CurdDaoServiceImpl<T,R> implements PermissionCheckAutomation<T> {

    @Autowired
    private ModelService modelService;

    @GetMapping("/columns")
    public YueResult<ModelClass> columns() {
        ModelClass yueModelClass= modelService.getModelClass(super.getGenericClass());
        List<ModelColumn>columns= modelService.getModelColumns(super.getGenericClass());
        Map<String,List<ModelColumn>> groups=columns.stream().collect(Collectors.groupingBy(ModelColumn::getGroup));
        yueModelClass.setColumns(columns);
        yueModelClass.setGroups(groups);
        yueModelClass.setType(ObjectType.LIST);
        return YueResult.<ModelClass>builder().data(yueModelClass).success().message("获取成功").build();
    }

    @PostMapping("/page")
    public YueResult<Page<T>> page(@Valid @RequestBody SearchPageRequest searchPageRequest,BindingResult result) {
        if (result.hasErrors()){
            for (ObjectError error : result.getAllErrors()) {
                return YueResult.<Page<T>>builder().fail().code(500).message(error.getDefaultMessage()).build();
            }
        }
        return super.page(searchPageRequest);
    }

    @GetMapping("/list")
    public YueResult<List<T>> list() {
        return super.list();
    }

    @GetMapping("/search")
    public YueResult<List<T>> search(
            @RequestParam(value = "keyword",required = false,defaultValue = "")String keyword,
            @RequestParam(value = "id",required = false,defaultValue = "")String id

    ) {
        return super.search(keyword,id);
    }

    @PostMapping("/delete")
    protected YueResult<String> delete(@Valid @RequestBody ListStringRequest stringRequest, BindingResult result){
        if (result.hasErrors()){
            for (ObjectError error : result.getAllErrors()) {
                return YueResult.<String>builder().fail().code(500).message(error.getDefaultMessage()).build();
            }
        }
        return  super.delete(stringRequest);
    }

    @PutMapping("")
    public YueResult<String>edit(@Valid @RequestBody T request,BindingResult result){
        if (result.hasErrors()){
            for (ObjectError error : result.getAllErrors()) {
                return YueResult.<String>builder().fail().code(500).message(error.getDefaultMessage()).build();
            }
        }
        return super.edit(request);
    }

    @PostMapping("")
    protected  YueResult<String>add(@Valid @RequestBody T request,BindingResult result){
        if (result.hasErrors()){
            for (ObjectError error : result.getAllErrors()) {
                return YueResult.<String>builder().fail().code(500).message(error.getDefaultMessage()).build();
            }
        }
        return super.add(request);
    }

    @GetMapping("/{id}")
    public YueResult<T>get(@PathVariable String id){
        return super.get(id);
    }

}
