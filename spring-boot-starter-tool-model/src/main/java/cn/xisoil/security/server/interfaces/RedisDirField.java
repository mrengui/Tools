package cn.xisoil.security.server.interfaces;


import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisDirField {

    String value() default "";
}
